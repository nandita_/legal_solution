import React, { useEffect } from 'react';
import { LogBox, StatusBar, StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
// import MainStack from './src/navigation/MainStack';
import RootNavigator from './src/navigation/RootNavigator';
import SplashScreen from 'react-native-splash-screen';
import { useSelector } from 'react-redux';
import { NotificationListener, getToken, requestUserPermission } from './src/utils/NotificationListener';
import NotificationController from './src/utils/NotificationController';


function App() {
  const data = useSelector((state) => state.counter.apiDataFlow);
  useEffect(() => {
    setTimeout(
      function () {
        SplashScreen.hide();
      }.bind(this),
      3000,
    );
  }, []);

  useEffect(() => {
    getToken()
    requestUserPermission()
    NotificationListener()
  }, [])

  console.log("Store Data------->", data);
  LogBox.ignoreAllLogs();

  return (
    <>
      <NavigationContainer>
        <NotificationController />
        <RootNavigator />

      </NavigationContainer>
    </>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
