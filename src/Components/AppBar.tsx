import { StyleSheet, View } from "react-native";
import { Text, TouchableRipple } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";
import BackButton from "./BackButton";
// import { s } from "react-native-wind";
import { Height } from "../dimensions/dimensions";
import { fontConstant } from "../utils/constants";
interface AppBarProps {
    title: string,
    onBackPress?: () => void
}
const AppBar = (props: AppBarProps) => {
    const Nav = useNavigation();
    return (
        <View style={styles.container}>
            <BackButton color="black" size={12} onPress={props.onBackPress} />
            <View style={styles.titleContainer}>
                <Text style={[styles.title]} children={props.title} />
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        width: "95%",
        alignSelf: 'center',
        justifyContent: 'center',
        height: Height * 0.10,
        position: 'relative',
        // backgroundColor: "pink",
        paddingTop: Height * 0.06

    },
    backIcon: {

        paddingHorizontal: 8,
    },
    title: {
        // backgroundColor: "yellow",
        textAlign: "center",
        color: 'black',
        width: '100%',
        fontFamily: fontConstant.bold,
        fontSize: 20,
        textTransform: 'capitalize'
    },
    titleContainer: {
        width: '100%',
        height: Height * 0.10,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        left: 0,
    }
})
export default AppBar;