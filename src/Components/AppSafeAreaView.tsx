import { ReactNode } from "react"
import { Platform, StatusBar, StyleSheet, View } from "react-native"
import AppBar from "./AppBar"
import { ViewStyle } from "react-native"
import React from "react"

interface AppSafeAreaViewProps {
    children: ReactNode,
    title?: string
    withBg?: boolean,
    bgColor?: string,
    noPadding?: boolean
    containerStyle?: ViewStyle,
    translucentFlag?: boolean
    onBackPress?: () => void
}
const AppSafeAreaView = (props: AppSafeAreaViewProps) => {
    // const UserStore = useSelector((state:RootState)=> state.user.data)
    // const themeColor = getThemeColorByUserType(UserStore.currentAccType);
    return (
        <>
            <StatusBar barStyle={'dark-content'} translucent={props.translucentFlag}
                // backgroundColor={props.bgColor?props.bgColor:"white"}
                backgroundColor={'transparent'}
            />
            <View style={[styles.safeViewStyle, { backgroundColor: (props.bgColor) ? props.bgColor : 'white', }]}  >
                {
                    props.title && (<><View style={{ paddingTop: "10%" }} ><AppBar title={props.title} onBackPress={(props.onBackPress) && props.onBackPress} /></View></>)
                    // <AppBar title={props.title} />
                }
                <View style={[{ paddingHorizontal: props.noPadding ? 0 : 16, overflow: 'hidden', flex: 1, ...props.containerStyle },]}>
                    {
                        props.children
                    }
                </View>


            </View>

        </>

    )
}
const styles = StyleSheet.create({
    safeViewStyle: {
        flex: 1,
        backgroundColor: 'white',
        position: 'relative',
        // marginTop: (Platform.OS == 'android') ? StatusBar.currentHeight  : 0,
        paddingBottom: (Platform.OS == 'android') ? 0 : 30
    }
})
export default AppSafeAreaView;