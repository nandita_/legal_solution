import { useNavigation } from '@react-navigation/native';
import { Image, Pressable, StyleSheet } from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import { imageConstants } from '../utils/constants';
// import { BackIcon } from ';

const BackButton = (prop: { color: string, size: number, onPress?: () => void }) => {
    const Nav = useNavigation();
    function pushNewScreen(): void {
        console.log(typeof prop.onPress);
        (prop.onPress != undefined) ? prop.onPress() : Nav.goBack()
    }
    return (
        <TouchableRipple style={styles.btn} onPress={pushNewScreen}>
            <Image source={imageConstants.BackIcon} resizeMode={'contain'} style={{ width: prop.size }} />
        </TouchableRipple>
    )
}
const styles = StyleSheet.create({

    btn: {
        // backgroundColor: "red",
        position: 'absolute',
        height: '400%',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 15,
        padding: 5,
        // width: 40,
    }
})
export default BackButton;