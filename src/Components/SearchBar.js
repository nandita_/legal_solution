import { TextInput, View, Image, StyleSheet, TouchableOpacity, ViewStyle } from "react-native"
import AntDesignIcons from 'react-native-vector-icons/AntDesign'
// import Icon from 'react-native-vector-icons/FontAwesome5';
import { Text } from "react-native-paper"
import { memo, useEffect, useRef, useState } from "react"
import { fontConstant, imageConstants } from "../utils/constants";

// interface SearchBoxType {
//     containerStyle?: ViewStyle,
//     onClick?: () => void,
//     onSubmit?: (search: string, from: 'search' | 'recent') => void
//     label: string,
//     type: 'block' | 'input'

// }
// interface SearchInputType extends SearchBoxType {
//     containerStyle?: ViewStyle,
//     placholder: string,
//     inputRef?:any
//     // setValue?: (value: string) => void,
//     // // type: 'input',
//     // value: string
// }
// type SearchBarProps = SearchInputType;
const SearchBar = (props) => {
    const [search, setSearch] = useState('');
    return (
        <TouchableOpacity onPress={props.onClick} activeOpacity={1} >
            <View style={[styles.container, props.containerStyle]}>
                {
                    (props.type == 'block') ?
                        <Text style={styles.label} children={props.label} />
                        :
                        <TextInput
                            ref={props.inputRef}
                            value={props.value}
                            onFocus={props.onFocus}
                            editable={props.editable}
                            onChangeText={props.onChangeText}
                            style={styles.input}
                            onSubmitEditing={() => {
                                // console.log(search, 'key...'),
                                (props.onSubmit) &&
                                    props.onSubmit(search, 'search')
                            }}
                            placeholder={props.placholder}
                            placeholderTextColor={"#d3d3d3"}
                        />
                }

                <Image
                    source={imageConstants.searchIcon}
                />
            </View>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    container: {
        padding: 8,
        paddingHorizontal: 16,
        width: "100%",
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 8,
        justifyContent: 'space-between',
    },
    label: {
        width: "90%",
        fontSize: 15,

    },
    input: {
        backgroundColor: 'transparent',
        color: "#000000",
        width: '90%',
        padding: 0,
        fontSize: 14,
        fontFamily: fontConstant.medium
    }
})
export default memo(SearchBar); 