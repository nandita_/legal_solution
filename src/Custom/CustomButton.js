import {
    ActivityIndicator,
    Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
  } from 'react-native';
  import React from 'react';
  import {colorConstant, fontConstant, imageConstant} from '../utils/constants';
  import {Width} from '../dimensions/dimensions';
  
  const CustomButton = props => {
    return (
      <TouchableOpacity
        onPress={props.OnButtonPress}
        activeOpacity={1}
        disabled={props.disabled}
        style={[
          {...styles.buttonStyle},
          {
            backgroundColor: props.backgroundColor
              ? props.backgroundColor
              : colorConstant.blue,
              height: props.height ? props.height : 50,
            width: props.width ? props.width : '90%',
            marginTop: props.marginTop ? props.marginTop : 20,
            marginRight: props.marginRight,
            borderRadius: props.borderRadius ? props.borderRadius : 5,
            bottom: props.bottom,
            borderColor: props.borderColor,
            borderWidth: props.borderWidth,
            position: props.position,
            borderColor: props.borderColor,
            borderWidth: props.borderWidth,
            marginVertical: props.marginVertical,
            marginBottom: props.marginBottom,
            paddingHorizontal: props.paddingHorizontal,
          },
        ]}>
        {
        props.isLoading 
        ? 
        (
          <ActivityIndicator size={25} color={colorConstant.white} />
        ) : (

          <>
          {
            props.imageTrue ?
          
            <View style={{
              flexDirection:"row",
              alignItems:"center",
              // backgroundColor:"pink",
              // width:"50%",
              alignSelf:"center",
              // justifyContent:"space-between"
            }}>
              <Image 
              source={props.source}
              resizeMode='contain'
              style={{
                width:20,
                height:20,

              }}
              />
            
          <Text
            style={[
              styles.text,
              {
                color: props.color ? props.color : colorConstant.white,
                fontFamily: props.fontFamily
                  ? props.fontFamily
                  : fontConstant.semiBold,
                  marginLeft:"5%"
              },
            ]}>
            {props.buttonText}
          </Text>
          </View>

          :

          <Text
            style={[
              styles.text,
              {
                color: props.color ? props.color : colorConstant.white,
                fontFamily: props.fontFamily
                  ? props.fontFamily
                  : fontConstant.semiBold,
              },
            ]}>
            {props.buttonText}
          </Text>

          }
          </>
          
        )}
      </TouchableOpacity>
    );
  };
  
  export default CustomButton;
  
  const styles = StyleSheet.create({
    buttonStyle: {
      alignSelf: 'center',
      alignItems: 'center',
      justifyContent: 'center',
      
  
      flexDirection: 'row',
    },
    text: {
      fontSize: 16,
    },
  });
  