import { Image, Platform, StyleSheet, Text, TextInput, View } from 'react-native';
import React, { useState } from 'react';
import { Dropdown } from 'react-native-element-dropdown';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import { colorConstant, fontConstant } from '../utils/constants';

const CustomDropdown = props => {
    const [value, setValue] = useState(null);
    const [isFocus, setIsFocus] = useState(false);

    const data = [
        { label: 'Item 1', value: '1' },
        { label: 'Item 2', value: '2' },
        { label: 'Item 3', value: '3' },
        { label: 'Item 4', value: '4' },
        { label: 'Item 5', value: '5' },
        { label: 'Item 6', value: '6' },
        { label: 'Item 7', value: '7' },
        { label: 'Item 8', value: '8' },
    ];

    return (
        <View
            style={[
                styles.inputView,
                {
                    width: props.width ? props.width : '85%',
                    marginLeft: props.marginLeft,
                    marginRight: props.marginRight,
                    height: props.height,
                    marginTop: props.margin ? props.margin : 20,
                    backgroundColor: props.backgroundColor
                        ? props.backgroundColor
                        : colorConstant.white,
                    borderColor: props.borderColor,
                    borderWidth: props.borderWidth,
                    borderRadius: props.borderRadius ? props.borderRadius : 5,
                    paddingHorizontal: props.padding ? props.padding : moderateScale(15),

                },
            ]}>

            <Dropdown
                style={[
                    styles.inputStyle,
                    {
                        fontSize: props.fontSize ? props.fontSize : 14,
                        color: colorConstant.black,
                        marginTop: props.marginTop,
                        paddingHorizontal: props.paddingHorizontal,
                        height: props.height ? props.height : moderateVerticalScale(46),
                        padding: props.padding ? props.padding : moderateScale(3),
                        textAlignVertical: props.textAlignVertical,
                        paddingTop: props.paddingTop,
                        // backgroundColor:"pink",
                        width: "100%"
                    },
                ]}
                // placeholderStyle={{
                //     placeholderTextColor: props.placeholderTextColor ? props.placeholderTextColor : '#4F5D77CC',
                //     fontSize: props.placeholderfont ? props.placeholderfont : 13,
                //     fontFamily: fontConstant.medium
                // }}
                placeholderStyle={styles.placeholderStyle}
                selectedTextStyle={styles.selectedTextStyle}
                placeholder={props.placeholder ? props.placeholder : 'Enter'}
                // inputSearchStyle={styles.inputSearchStyle}
                iconStyle={styles.iconStyle}
                data={props.data ? props.data : data}
                search={props.search}
                maxHeight={300}
                labelField="label"
                valueField="value"
                searchPlaceholder="Search..."
                value={props.value}
                onFocus={() => setIsFocus(true)}
                onBlur={() => setIsFocus(false)}
                // onChange={item => {
                //     setValue(item.value);
                //     setIsFocus(false);
                // }}
                onChange={props.onChange}

            />


        </View>
    );
};

export default CustomDropdown;

const styles = StyleSheet.create({
    inputView: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginRight: moderateScale(5),

    },
    inputStyle: {
        paddingVertical: 0,
        width: '70%',
        fontFamily: fontConstant.regular,
    },

    dropdown: {
        margin: 16,
        height: 50,
        backgroundColor: 'white',
        borderRadius: 12,
        padding: 12,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,

        elevation: 2,
    },

    selectedTextStyle: {
        fontSize: 16,
    },
    iconStyle: {
        width: 20,
        height: 20,

    },

    placeholderStyle: {
        fontFamily: fontConstant.regular,
        fontSize: 13,
        color: "#4F5D77CC"
    }

});
