import { Image, Platform, StyleSheet, Text, TextInput, View } from 'react-native';
import React from 'react';

import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import { colorConstant, fontConstant } from '../utils/constants';

const CustomInput = props => {
  return (
    <View
      style={[
        styles.inputView,
        {
          width: props.width ? props.width : '85%',
          marginLeft: props.marginLeft,
          marginRight: props.marginRight,
          height: props.height,
          marginTop: props.margin ? props.margin : 20,
          backgroundColor: props.backgroundColor
            ? props.backgroundColor
            : colorConstant.white,
          borderColor: props.borderColor,
          borderWidth: props.borderWidth,
          borderRadius: props.borderRadius ? props.borderRadius : 5,
          paddingHorizontal: props.padding ? props.padding : moderateScale(15),
          marginBottom:props.marginBottom

        },
      ]}>
      {
        props.inputImage && (
          <Image
            source={props.source}
            resizeMode="contain"
            style={{
              width: props.imageWidth ? props.imageWidth : 20,
              height: props.imageHeight ? props.imageHeight : 20,
              // backgroundColor:"pink"
            }}
          />
        )
      }

      <TextInput
        value={props.value}
        multiline={props.multiline}
        editable={props.editable}
        keyboardType={props.keyboardType}
        onChangeText={props.onChangeText}
        secureTextEntry={props.secureTextEntry}
        maxLength={props.maxLength}
        style={[
          styles.inputStyle,
          {
            fontSize: props.fontSize ? props.fontSize : 14,
            color: colorConstant.black,
            marginTop: props.marginTop,
            width: props.textWidth,
            // backgroundColor: "pink",
            paddingHorizontal: props.paddingHorizontal ? props.paddingHorizontal : moderateVerticalScale(10),
            height: props.height ? props.height : moderateVerticalScale(46),
            padding: props.padding ? props.padding : moderateScale(3),
            textAlignVertical: props.textAlignVertical,
            paddingTop: props.paddingTop,
          },
        ]}
        // ={props.value}
        placeholder={props.placeholder ? props.placeholder : 'Enter'}
        placeholderTextColor={
          props.placeholderTextColor ? props.placeholderTextColor : '#4F5D77CC'
        }
      />


    </View>
  );
};

export default CustomInput;

const styles = StyleSheet.create({
  inputView: {
    flexDirection: 'row',
    // borderWidth: 1,
    alignItems: 'center',
    // borderColor: colorConstant.borderColor,
    alignSelf: 'center',
    marginRight: moderateScale(5),

  },
  inputStyle: {
    paddingVertical: 0,
    width: '70%',
    // backgroundColor:"pink",
    fontFamily: fontConstant.regular,
    // color:colorConstant.black
  },
});
