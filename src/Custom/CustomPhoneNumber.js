import { Image, Platform, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React, { useRef, useState } from 'react'
import { CallingCodePicker } from '@digieggs/rn-country-code-picker';

import PhoneInput from "react-native-phone-number-input";
import { moderateScale } from 'react-native-size-matters';
import { colorConstant, fontConstant } from '../utils/constants';
// import translations from '../Localization/MainLocalization';

const CustomPhoneNumber = (props) => {

    let phoneInput = useRef(null);

  return (
      <PhoneInput
          ref={phoneInput}
          defaultValue={props.defaultValue}
          value={props.value}
          defaultCode={props.defaultCode}
          onChangeCountry={props.onChangeCountry}
          layout="second"
          onChangeText={props.onChangeText}
          onChangeFormattedText={props.onChangeFormattedText}
        //   countryPickerProps={{
        //     // withAlphaFilter:true,
        //     countryCodes: ['IN','SA','BH','KW','OM','QA','AE'],
        //     }}
          containerStyle={{
              backgroundColor: colorConstant.white,
              alignSelf: "center",
              width: "90%",
              marginRight:moderateScale(0),
              flexDirection:"row",
              justifyContent:"space-between",
            //   backgroundColor: colorConstant.grey,
             
              borderRadius: 5,
              
              marginTop: props.marginTop,

          }}
        //   flagButtonStyle={{
        //     width:-20
        //   }}
        //   placeholder={translations.Enter_Number}
        placeholder={"Enter Phone Number"}
          textInputProps={{
            // editable:props.editable,
            keyboardType:"number-pad",
            fontSize:13,
            fontFamily: fontConstant.regular,
              placeholderTextColor: "#4F5D77CC",
              maxLength:10
          }}

          codeTextStyle={{
              fontSize: 13,
              fontFamily: fontConstant.medium,
              color:colorConstant.black,
            //   color: colorConstant.placeholderColor
          }}
          withDarkTheme={false}
          textInputStyle={{
              fontSize: 14,
              color:colorConstant.black,
            //   fontFamily: fontConstant.medium
          }}
          textContainerStyle={{
              paddingVertical: 0,
              borderTopRightRadius: 10,
              paddingLeft: 0,
              borderBottomRightRadius: 10,
              backgroundColor: colorConstant.grey,
              ...Platform.select({
                  ios: {
                      height: 50
                  }
              })
          }}
      />

  )
}

export default CustomPhoneNumber