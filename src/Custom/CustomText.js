import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import { colorConstant, fontConstant } from '../utils/constants'

const CustomText = (props) => {
  return (
    <View style={[styles.container, {
      marginTop: props.marginTop ? props.marginTop : moderateVerticalScale(40),
      width: props.width ? props.width :  "90%",
    }]}>
      <Text style={[styles.TitleStyle, {
        fontFamily:props.fontFamily? props.fontFamily: fontConstant.bold,
        color: props.color ? props.color: colorConstant.black,
        fontSize: props.fontSize ? props.fontSize : 16,
        paddingHorizontal: props.paddingHorizontal
      }]}>{props.Title}</Text>
    </View>
  )
}

export default CustomText

const styles = StyleSheet.create({
    container:{
    
     alignSelf:"center",
     
    },
    
})