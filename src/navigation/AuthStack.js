import {Image, Platform} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';


import Login from '../screens/Auth/Login';
import Verify from '../screens/Auth/Verify';
import Register from '../screens/Auth/Register';
import Signup from '../screens/Auth/Signup';
import ChangePassword from '../screens/Auth/ChangePassword';
import CountryScreen from '../screens/Auth/CountryScreen';
import Congratulations from '../screens/Others/Congratulations';
import TermsCondition from '../screens/Auth/TermsCondition';
import ForgetPassword from '../screens/Auth/ForgetPassword';



const Stack = createNativeStackNavigator();
const AuthStack = props => {
 

  return (
    <Stack.Navigator
      initialRouteName="Login"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Verify" component={Verify} />
      <Stack.Screen name="Signup" component={Signup} />
      <Stack.Screen name="ChangePassword" component={ChangePassword} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Congratulations" component={Congratulations} />
      <Stack.Screen name="CountryScreen" component={CountryScreen} />
      <Stack.Screen name="TermsCondition" component={TermsCondition} />
      <Stack.Screen name="ForgetPassword" component={ForgetPassword} />

    </Stack.Navigator>
  );
};

export default AuthStack;
