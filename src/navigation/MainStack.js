import { StackActions, useNavigation } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React, { useEffect, useState } from 'react';
import { colorConstant, fontConstant, imageConstants } from '../utils/constants';
import DrawerScreen from '../screens/MainScreen/DrawerScreen';
import Homepage from '../screens/MainScreen/Homepage';
import Notification from '../screens/Others/Notification';
import { Width } from '../dimensions/dimensions';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import { Image } from 'react-native';
import Blogs from '../screens/Blogs/Blogs';
import MyProfile from '../screens/Profile/MyProfile';
import WordSearch from '../screens/Search/WordSearch';
import CitySearch from '../screens/Search/CitySearch';
import SearchPartyName from '../screens/Search/SearchPartyName';
import CourtName from '../screens/Search/CourtName';
import CaseNumber from '../screens/Search/CaseNumber';
import ByTopic from '../screens/Search/ByTopic';
import ByLaw from '../screens/Search/ByLaw';
import SearchList from '../screens/SearchList/SearchList';
import Judgment from '../screens/SearchList/Judgment';
import UpgradePlan from '../screens/SearchList/UpgradePlan';
import MyPlans from '../screens/MyPlans/MyPlans';
import PaymentHistoryList from '../screens/MyPlans/PaymentHistoryList';
import LogOut from '../screens/Profile/Logout';
import ProfileEdit from '../screens/Profile/ProfileEdit';
import Congratulations from '../screens/Others/Congratulations';
import SubscriptionPlan from '../screens/MyPlans/SubscriptionPlan';
import Setting from '../screens/Profile/Setting';
import ChangePasswordOther from '../screens/Profile/ChangePasswordOther';
import ContactUs from '../screens/Others/ContactUs';
import AboutPrivacyTerm from '../screens/Others/AboutPrivacyTerm';
import PersonalDetail from '../screens/MyPlans/PersonalDetails';
import BillingDetails from '../screens/MyPlans/BillingDetails';
import PaymentDetail from '../screens/MyPlans/PaymentDetail';
import SubcribeCongrat from '../screens/Others/SubcribeCongrat';
import BlogsDetail from '../screens/Blogs/BlogsDetail';
import PaymentReciept from '../screens/MyPlans/PaymentReciept';
import DeleteUser from '../screens/Profile/DeleteUser';
import TermsCondition from '../screens/Auth/TermsCondition';
import SearchLegislation from '../screens/Search/SearchLegislation';
import SearchResult from '../screens/Search/SearchResult';
import FreeTrialModal from '../screens/MainScreen/FreeTrialModal';



const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const DrawerStack = () => {
  return (
    <>
      <Drawer.Navigator
        screenOptions={{
          drawerPosition: 'right',
          drawerType: "front",
          overlayColor: 'transparent',
          // drawerType: Platform.OS == 'ios' ? 'front' : 'front',
          headerShown: false,
          drawerStyle: {
            width: Width * 0.65,
            backgroundColor: colorConstant.themecolor
          },
        }}
        drawerContent={props => <DrawerScreen {...props} num={1} />}>
        <Drawer.Screen name="TabStack" component={TabStack} />
      </Drawer.Navigator>
    </>
  );
};

const TabStack = () => {
  const navigation = useNavigation();

  // const [temp , setTemp] = useState(false)



  // useEffect(() => {
  //   setTemp(!temp)
  //   console.log('hello');
  //  }, [localizationValue])
  return (
    <Tab.Navigator
      backBehavior="history"
      initialRouteName={"Home"}
      screenOptions={({ route: { name } }) => ({
        tabBarIcon: ({ focused }) => {
          switch (name) {
            case "Home":
              return (
                <Image
                  source={focused ? imageConstants.homeColor : imageConstants.home}
                  style={{ width: moderateScale(20), height: moderateScale(20) }}
                />
              )
            case "My Plan":
              return (
                <Image
                  source={focused ? imageConstants.premiumColor : imageConstants.premium}
                  style={{ width: moderateScale(20), height: moderateScale(20) }}
                />
              )
            case "Blogs":
              return (
                <Image
                  source={focused ? imageConstants.blogColor : imageConstants.blog}
                  style={{ width: moderateScale(20), height: moderateScale(20) }}
                />
              )
            case "Profile":
              return (
                <Image
                  source={focused ? imageConstants.profileColor : imageConstants.profile}
                  style={{ width: moderateScale(20), height: moderateScale(20) }}
                />
              )




          }
        },

        keyboardHidesTabBar: false,
        tabBarActiveTintColor: colorConstant.blue,
        tabBarInactiveTintColor: colorConstant.tabcolor,
        tabBarLabelStyle: {
          fontSize: 13,
          // color: focused ?colorConstant.blue : colorConstant.tabcolor,
          fontFamily: fontConstant.medium,
          bottom: moderateVerticalScale(8),
        },
        tabBarStyle: {
          backgroundColor: colorConstant.themecolor,
          borderTopWidth: 0,
          width: '100%',
          height:
            Platform.OS === 'ios'
              ? moderateVerticalScale(80)
              : moderateVerticalScale(70),
        },
      })}>
      <Tab.Screen name={"Home"} options={{ headerShown: false }} component={Homepage} />
      <Tab.Screen name={"My Plan"} options={{ headerShown: false }} component={MyPlans} />
      <Tab.Screen name={"Blogs"} options={{ headerShown: false }} component={Blogs} />
      <Tab.Screen name={"Profile"} options={{ headerShown: false }}
        listeners={{
          tabPress: e => {
            setTimeout(() => {
              navigation.openDrawer();
              e.preventDefault();
            }, 100);
          },
        }}
        component={Homepage} />

    </Tab.Navigator>
  );
};


function MainStack({ navigation }) {
  return (
    <>
      <Stack.Navigator
        initialRouteName={'DrawerStack'}
        screenOptions={{
          headerShown: false,
          animation: 'default'
        }}>
        <Stack.Screen name="DrawerStack" component={DrawerStack} />
        <Stack.Screen name='Notification' component={Notification} />
        <Stack.Screen name='WordSearch' component={WordSearch} />
        <Stack.Screen name='CitySearch' component={CitySearch} />
        <Stack.Screen name='SearchPartyName' component={SearchPartyName} />
        <Stack.Screen name='CourtName' component={CourtName} />
        <Stack.Screen name='CaseNumber' component={CaseNumber} />
        <Stack.Screen name='ByTopic' component={ByTopic} />
        <Stack.Screen name='ByLaw' component={ByLaw} />
        <Stack.Screen name='SearchLegislation' component={SearchLegislation} />
        <Stack.Screen name='SearchResult' component={SearchResult} />
        <Stack.Screen name='SearchList' component={SearchList} />
        <Stack.Screen name='Judgment' component={Judgment} />
        <Stack.Screen name='UpgradePlan' component={UpgradePlan} options={{ animation: "fade", presentation: 'transparentModal' }} />
        <Stack.Screen name='FreeTrialModal' component={FreeTrialModal} options={{ animation: "fade", presentation: 'transparentModal' }} />
        <Stack.Screen name='PaymentHistoryList' component={PaymentHistoryList} />
        <Stack.Screen name='LogOut' component={LogOut} options={{ animation: "fade", presentation: 'transparentModal' }} />
        <Stack.Screen name='MyProfile' component={MyProfile} />
        <Stack.Screen name='ProfileEdit' component={ProfileEdit} />
        <Stack.Screen name='Congratulations' component={Congratulations} />
        <Stack.Screen name='SubscriptionPlan' component={SubscriptionPlan} />
        <Stack.Screen name='Setting' component={Setting} />
        <Stack.Screen name='ChangePasswordOther' component={ChangePasswordOther} />
        <Stack.Screen name='ContactUs' component={ContactUs} />
        <Stack.Screen name='AboutPrivacyTerm' component={AboutPrivacyTerm} />
        <Stack.Screen name='PersonalDetail' component={PersonalDetail} />
        <Stack.Screen name='BillingDetails' component={BillingDetails} />
        <Stack.Screen name='PaymentDetail' component={PaymentDetail} />
        <Stack.Screen name='SubcribeCongrat' component={SubcribeCongrat} />
        <Stack.Screen name='BlogsDetail' component={BlogsDetail} />
        <Stack.Screen name='PaymentReciept' component={PaymentReciept} />
        <Stack.Screen name='DeleteUser' component={DeleteUser} options={{ animation: "fade", presentation: 'transparentModal' }} />
        <Stack.Screen name='TermsCondition' component={TermsCondition} />
      </Stack.Navigator>
    </>
  )
}

export default MainStack;
