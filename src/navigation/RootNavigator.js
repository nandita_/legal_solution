import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
// import MainStack from './MainStack';
import { useSelector } from 'react-redux';
import IntroStack from './IntroStack';
import AuthStack from './AuthStack';
import MainStack from './MainStack';

const RootNavigator = () => {
  const introStatus = useSelector((state) => state.counter.apiDataFlow.introStatus)
  console.log("introStatus ----->", introStatus);
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>

      {introStatus == null && (
        <Stack.Screen name="IntroStack" component={IntroStack} />
      )}

      {introStatus == 'auth' && (
        <Stack.Screen name="AuthStack" component={AuthStack} />
      )}

      {introStatus == 'main' && (
        <Stack.Screen name="MainStack" component={MainStack} />
      )}
    </Stack.Navigator>
  );
};

export default RootNavigator;
