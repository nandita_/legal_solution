
import { createSlice } from '@reduxjs/toolkit';
import allState from './states';

const parentFlowSlice=createSlice({
    name:'parentFlow',
    initialState:allState.localStates,
    // reducers:{
    //     setIntroStatus:(state,action)=>{
    //         state.introStatus=action.payload.introStatus
    //     },
    //     setSubscriptionStatus:(state,action)=>{
    //         state.SubscriptionStatus=action.payload.SubscriptionStatus
    //     },
    //     setUserProfile:(state,action)=>{
    //         console.log(action.payload, "CHECK PROFILE");
    //         state.userProfile=action.payload.userProfile
    //     },
    //     setLogoutValue:(state, action)=>{
    //         state.logoutValue=action.payload.logoutValue
    //     },
    //     setjwtToken:(state,action) => {
    //         state.jwtToken=action.payload
    //     },
    // }
})
// export const { setIntroStatus, setSubscriptionStatus, setUserProfile , setLogoutValue, setjwtToken} = parentFlowSlice.actions;

export default parentFlowSlice.reducer;