import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from '../../utils/Interceptor';
// import axios from 'axios';
import { BASE_URL } from '../../utils/Uri';
import { useSelector } from 'react-redux';
import { store } from '../../utils/store';

const getUserData = () => {
  let storeData = store.getState();
  // console.log("storeData",storeData);
  const Token = storeData?.counter.apiDataFlow.jwtToken;
  // console.log("Token",Token);
  return { Token };
};

// LoginEmail check
const emailCheckApi = createAsyncThunk('EmailCheck', async (data) => {
  try {
    let response = await axios.post(BASE_URL + "user/login", data)
    console.log(response, "EMAIL RESPONSE=========>>>>>>>");
    return response?.data
  } catch (error) {
    console.log(error);
    return error;
  }
})

// CHANGE PASSWORD LOGIN
const changePasswordInLoginApi = createAsyncThunk('ChangePasswordCheck', async (data) => {
  try {
    let response = await axios.post(BASE_URL + "user/update_password", data, {
      headers: {
        Authorization: getUserData().Token
      }
    });
    console.log(response, "EMAIL RESPONSE=========>>>>>>>");
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// FORGOT API OTP 
const forgotOtpApi = createAsyncThunk('ForgotCheck', async (data) => {
  try {
    let response = await axios.post(BASE_URL + "user/reset_password_otp", data)
    console.log(response, "OTP RESPONSE=========>>>>>>>");
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// CHANGE PASSWORD
const forgotPasswordApi = createAsyncThunk('ForgotPassword', async (data) => {
  try {
    let response = await axios.post(BASE_URL + "user/reset_password", data);
    console.log(response, "FORGOT PASSWORD RESPONSE=========>>>>>>>");
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// REGISTER 
const registerApi = createAsyncThunk('RegisterCheck', async (data) => {
  try {
    let response = await axios.post(BASE_URL + "user/sent_otp", data)
    console.log(response, "OTP RESPONSE=========>>>>>>>");
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// SIGNUP 
const signupApi = createAsyncThunk('SignUpCheck', async (data) => {
  try {
    let response = await axios.post(BASE_URL + "user/signup", data)
    //  console.log(response.data, "SIGNUP RESPONSE=========>>>>>>>");
    return response.data


  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// GET PROFILE 
const getProfileApi = createAsyncThunk('GetProfile', async (data) => {
  try {
    let response = await axios.get(BASE_URL + "user/list", {
      headers: {
        Authorization: getUserData().Token
      }
    })
    // console.log(response.data, "GET PRofile RESPONSE=========>>>>>>>");
    return response.data


  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// UPDATE PROFILE 
const updateProfileApi = createAsyncThunk('UpdateProfileCheck', async (data) => {
  try {
    let response = await axios.post(BASE_URL + "user/update", data, {
      headers: {
        Authorization: getUserData().Token
      }
    })
    //  console.log(response.data, "SIGNUP RESPONSE=========>>>>>>>");
    return response.data


  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// LOGOUT 
const logoutApi = createAsyncThunk('LogoutCheck', async () => {
  try {
    let response = await axios.get(BASE_URL + "user/logout", {
      headers: {
        Authorization: getUserData().Token
      }
    })
    //  console.log(response.data, "SIGNUP RESPONSE=========>>>>>>>");
    return response.data


  } catch (error) {
    console.log(error.response);
    return error;
  }
})



// CHANGE PASSWORD
const changePasswordApi = createAsyncThunk('ChangePassword', async (data) => {
  try {
    let response = await axios.post(BASE_URL + "user/update_password", data, {
      headers: {
        Authorization: getUserData().Token
      }
    });
    console.log(response, "EMAIL RESPONSE=========>>>>>>>");
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// delete account 
const deleteUserApi = createAsyncThunk('DeleteUser', async () => {
  console.log("getUserData().Token", getUserData().Token);
  try {
    let response = await axios.post(BASE_URL + "user/delete", {}, {
      headers: {
        Authorization: getUserData().Token
      }
    })
    return response.data
  } catch (error) {
    console.log(error.response)
    return error;
  }
})

// static content 
const StaticContentApi = createAsyncThunk('staticContent', async (type) => {
  try {
    let response = await axios.get(BASE_URL + `user/static/list?type=${type}`, {
      headers: {
        Authorization: getUserData().Token
      }
    });
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// Country api 
const CountryApi = createAsyncThunk('countryGet', async () => {
  try {
    let response = await axios.get(BASE_URL + `web/countries`)
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// Country api 
const StateApi = createAsyncThunk('stateGet', async (countryIsoCode) => {
  try {
    let response = await axios.get(BASE_URL + `web/states/` + countryIsoCode)
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// Country api 
const CityApi = createAsyncThunk('cityGet', async (stateIsoCode) => {
  try {
    let response = await axios.get(BASE_URL + `web/cities/` + stateIsoCode)
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// Blog listing
const BlogListApi = createAsyncThunk('BlogList', async (data) => {
  try {
    let response = await axios.get(BASE_URL + `user/blog/list?category=${data}`)
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// court Name 
const CourtNameApi = createAsyncThunk('CourtName', async (data) => {
  // console.log(data, data.yearFromData, "huihuihui");
  try {
    let response = await axios.get(BASE_URL + `user/court_list?startDate=${data.yearFromData}&endDate=${data.yearToData}&countryName=${data.countryData}`)
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// popular tag
const PopularTagApi = createAsyncThunk('PopularTag', async () => {
  try {
    let response = await axios.get(BASE_URL + 'user/tag/list')
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// tag count 
const TagCountApi = createAsyncThunk('TagCount', async (data) => {
  try {
    let response = await axios.get(BASE_URL + `user/tag/count?category=${data}`)
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// Category list
const CategoryApi = createAsyncThunk('CategoryList', async () => {
  try {
    let response = await axios.get(BASE_URL + 'user/category_list')
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// Subscription plan list
const SubscriptionPlanApi = createAsyncThunk('SubscribeList', async () => {
  try {
    let response = await axios.get(BASE_URL + 'user/plan/list')
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// purchase plan 
const purchasePlanApi = createAsyncThunk('purchasePlan', async (data) => {
  try {
    let response = await axios.post(BASE_URL + 'user/plan/purchase-plan', data)
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// current plan 
const CurrentPlanApi = createAsyncThunk('CurrentPlan', async () => {
  try {
    let response = await axios.get(BASE_URL + 'user/plan/current-plan')
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// plan history 
const PlanHistoryApi = createAsyncThunk('PlanHistory', async () => {
  try {
    let response = await axios.get(BASE_URL + 'user/plan/history')
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// notification list 
const NotificationApi = createAsyncThunk('notify', async () => {
  try {
    let response = await axios.get(BASE_URL + 'user/notification/notification-list')
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})

// notification read api
const NotificationReadApi = createAsyncThunk('notifyRead', async (notificationId) => {
  try {
    let response = await axios.get(BASE_URL + `user/notifications?notificationId=${notificationId}`)
    return response.data
  } catch (error) {
    console.log(error.response);
    return error;
  }
})
const ApiDataFlow = createSlice({
  name: 'apiData',
  initialState: {
    isLoader: false,
    logoutValue: null,
    introStatus: null,
    freeTrial: false,
    trialStatus: false,
    jwtToken: null,
    PlanType: "",
    userProfile: {
      _id: "",
      firstName: "",
      lastName: "",
      phoneNumber: "",
      email: "",
      countryFlag: "",
      countryCode: "",
      image: "",
      state: "",
      country: "",
      billingAddress: "",
      designation: "",
      organization: "",
      postalCode: "",
      city: ""
    },
    SubscriptionStatus: false,
    rememberMe: false,
    emailLogin: "",
    passwordLogin: "",
    isError: false,
    isSignUpLoader: false,
    signUpData: null,
    isSignUpError: false,
    isLoginLoader: false,
    isLoginData: null,
    isLoginError: false,
  },
  reducers: {
    clearLogout: (state, action) => {
      // console.log("action",action);
      state.jwtToken = null,
        state.userProfile = {
          firstName: "",
          lastName: "",
          phoneNumber: "",
          email: "",
          countryFlag: "",
          countryCode: "",
          image: "",
          state: "",
          country: "",
          billingAddress: "",
          designation: "",
          organization: "",
          postalCode: "",
          city: ""
        }
    },
    setSubscriptionStatus: (state, action) => {
      state.SubscriptionStatus = action.payload.SubscriptionStatus
    },
    setRememberMe: (state, action) => {
      state.rememberMe = action.payload.rememberMe
    },
    setPlanType: (state, action) => {
      state.PlanType = action.payload.PlanType
    },
    setTrialStatus: (state, action) => {
      state.trialStatus = action.payload.trialStatus
    },
    setEmailLogin: (state, action) => {
      state.emailLogin = action.payload.emailLogin
    },
    setPasswordLogin: (state, action) => {
      state.passwordLogin = action.payload.passwordLogin
    },
    setIntroStatus: (state, action) => {
      state.introStatus = action.payload.introStatus
    },
    setLogoutValue: (state, action) => {
      state.introStatus = 'auth'
      state.jwtToken = null,
        state.userProfile = {
          firstName: "",
          lastName: "",
          phoneNumber: "",
          email: "",
          countryFlag: "",
          countryCode: "",
          image: "",
          state: "",
          country: "",
          billingAddress: "",
          designation: "",
          organization: "",
          postalCode: "",
          city: ""
        }
    },
    setjwtToken: (state, action) => {
      state.jwtToken = action.payload
    },
    setUserProfile: (state, action) => {
      console.log(action.payload, "CHECK PROFILE");
      state.userProfile = action.payload.userProfile
    },
    setFreeTrial: (state, action) => {
      state.freeTrial = action.payload.freeTrial
    }
  },
  extraReducers: builder => {
    builder
      // loginApi reducers
      .addCase(emailCheckApi.pending, state => {
        state.isLoader = true;
        state.isError = false;
      })
      .addCase(emailCheckApi.fulfilled, (state, action) => {
        let response = action.payload;
        let tempUser = {
          _id: response.data?._id ?? "",
          firstName: response.data?.firstName ?? "",
          lastName: response.data?.lastName ?? "",
          email: response.data?.email ?? "",
          phoneNumber: response.data?.phoneNumber ?? "",
          country: response.data?.country ?? "",
          countryCode: response.data?.countryCode ?? "",
          billingAddress: response.data?.billingAddress ?? "",
          organization: response.data?.organization ?? "",
          designation: response.data?.designation ?? "",
          postalCode: response.data?.postalCode ?? "",
          city: response.data?.city ?? "",
          state: response.data?.state ?? "",
          image: response.data?.image ?? ""
        }
        state.userProfile = tempUser;
        state.isLoader = false;
        state.jwtToken = action.payload.data?.jwtToken;
        console.log(state.userProfile, "CHECK LOGIN PROFILE======>>>>>");

      })
      .addCase(emailCheckApi.rejected, state => {
        state.isLoader = false;
        state.isError = true;
      })

      // changepasswordlogin reducers
      .addCase(changePasswordInLoginApi.pending, state => {
        state.isLoader = true;
        state.isError = false;
      })
      .addCase(changePasswordInLoginApi.fulfilled, (state, action) => {
        state.isLoader = false;
        // state.data = action.payload;
      })
      .addCase(changePasswordInLoginApi.rejected, state => {
        state.isLoader = false;
        state.isError = true;
      })

      // RegisterApi reducers
      .addCase(registerApi.pending, state => {
        state.isSignUpLoader = true;
        state.isSignUpError = false;
      })
      .addCase(registerApi.fulfilled, (state, action) => {
        state.isSignUpLoader = false;
        state.signUpData = action.payload;
      })
      .addCase(registerApi.rejected, state => {
        state.isSignUpLoader = false;
        state.isSignUpError = true;
      })

    // signup reducers
    builder.addCase(signupApi.fulfilled, (state, action) => {
      state.userProfile = action.payload.data;
      console.log(state.userProfile, "CHECK PROFILE======>>>>>");
    })

      // update profile
      .addCase(updateProfileApi.fulfilled, (state, action) => {
        state.userProfile = action.payload.data
        console.log(state.userProfile, "CHECK UPDATED PROFILE======>>>>>");
      })

      // get profile
      .addCase(getProfileApi.fulfilled, (state, action) => {
        let response = action.payload;
        let tempUser = {
          _id: response.data?._id ?? "",
          firstName: response.data?.firstName ?? "",
          lastName: response.data?.lastName ?? "",
          email: response.data?.email ?? "",
          phoneNumber: response.data?.phoneNumber ?? "",
          country: response.data?.country ?? "",
          countryCode: response.data?.countryCode ?? "",
          billingAddress: response.data?.billingAddress ?? "",
          organization: response.data?.organization ?? "",
          designation: response.data?.designation ?? "",
          postalCode: response.data?.postalCode ?? "",
          city: response.data?.city ?? "",
          state: response.data?.state ?? "",
          image: response.data.image ?? ""
        }
        state.userProfile = tempUser;
        // console.log(state.userProfile, "CHECK GET PROFILE======>>>>>");
      })




  },
});

export { emailCheckApi, changePasswordInLoginApi, registerApi, signupApi, updateProfileApi, logoutApi, changePasswordApi, getProfileApi, deleteUserApi, StaticContentApi, forgotPasswordApi, forgotOtpApi, CountryApi, StateApi, CityApi, BlogListApi, CourtNameApi, PopularTagApi, TagCountApi, CategoryApi, SubscriptionPlanApi, purchasePlanApi, CurrentPlanApi, PlanHistoryApi, NotificationApi, NotificationReadApi };
export const { clearLogout, setIntroStatus, setSubscriptionStatus, setUserProfile, setLogoutValue, setjwtToken, setRememberMe, setEmailLogin, setPasswordLogin, setPlanType, setFreeTrial, setTrialStatus } = ApiDataFlow.actions;
export default ApiDataFlow.reducer;
