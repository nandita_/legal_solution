import React, { useEffect, useState } from 'react';
import { FlatList, Image, ImageBackground, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native';


import { colorConstant, fontConstant, imageConstants } from '../../utils/constants';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { Width } from '../../dimensions/dimensions';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import CustomText from '../../Custom/CustomText';
import CustomInput from '../../Custom/CustomInput';
import CustomButton from '../../Custom/CustomButton';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import { changePasswordApi, changePasswordInLoginApi, forgotPasswordApi } from '../../redux/Slices/APISlices';
import toastShow from '../../utils/ToastMessage';
import { store } from '../../utils/store';
import { useSelector } from 'react-redux';
import Validator from '../../utils/Validator';



const initialState = {
    showFlagNew: true,
    showFlagConfirm: true,
    phoneNumber: "",
    password: "",
    passwordError: "",
    code: "",
    number: "",
    countryCode: "91",
    countryFlag: "IN",
    error: "",
}
const ChangePassword = (props) => {
    const { email, screenName } = props.route.params
    const [iState, updateState] = useState(initialState);
    const {
        showFlagNew,
        showFlagConfirm,
        phoneNumber,
        password,
        number,
        passwordError,
        code,
        countryCode,
        countryFlag,
    } = iState
    const [padding, setPadding] = useState(20);
    console.log(props.route.params.password, "hbdjcndkncldkclkdnckldjcok");

    const [oldPassword, setOldPassword] = useState();
    const [newPassword, setNewPassword] = useState();
    const [loading, setLoading] = useState(false)
    // const change = useSelector((state)=> state);

    useEffect(() => {
        setPadding(200);
    }, []);

    const changePassword = async () => {
        setLoading(true)
        let reqData = {
            newPassword: newPassword,
            oldPassword: props.route.params.password,
        }
        try {
            let response = await store.dispatch(changePasswordApi(reqData))
            if (response.payload.status === 200) {
                setLoading(false)
                console.log(response.payload, "ChangepasswordCEHCKKKKKK==========>>>>>>>>>");
                props.navigation.navigate("Congratulations")
            }
            else {
                setLoading(true)
                // toastShow(response.payload.message, colorConstant.red)
                // console.log(response , "ELSEEEEE ERRORRRRRR======>>>>>");
            }
        } catch (error) {
            console.log(error.message, "CATCH ERROR ========>>>");
        }

    }

    const forgotPassword = async () => {
        setLoading(true)
        let reqData = {
            email: email,
            password: newPassword,
        }
        try {
            let response = await store.dispatch(forgotPasswordApi(reqData))
            if (response.payload.status === 200) {
                setLoading(false)
                console.log(response.payload, "ForgotpasswordCEHCKKKKKK==========>>>>>>>>>");
                props.navigation.navigate("Congratulations", { screenName: screenName })
            }
            else {
                setLoading(false)
                toastShow(response.payload.message, colorConstant.red)
                // console.log(response , "ELSEEEEE ERRORRRRRR======>>>>>");
            }
        } catch (error) {
            console.log(error.message, "CATCH ERROR ========>>>");
        }

    }

    const validationCheck = () => {
        Validator.isEmpty(oldPassword)
            ? toastShow("Please enter a new password", colorConstant.red)
            : Validator.isEmpty(newPassword)
                ? toastShow("Please enter a confirm password", colorConstant.red)
                : !Validator.isPassword(newPassword)
                    ? toastShow("Password minimum length should be 8 and must contain at least one uppercase letter, one lowercase letter, one number, and one special character.", colorConstant.red)
                    : !Validator.isPassword(oldPassword)
                        ? toastShow("Password minimum length should be 8 and must contain at least one uppercase letter, one lowercase letter, one number, and one special character.", colorConstant.red)
                        : oldPassword != newPassword
                            ? toastShow("Password mismatch", colorConstant.red)
                            : screenName == 'Forget'
                                ? forgotPassword()
                                : changePassword()
    };

    return (
        <>
            <StatusBar barStyle={'dark-content'} translucent={true}
                // backgroundColor={props.bgColor?props.bgColor:"white"}
                backgroundColor={'transparent'}
            />
            <View style={styles.main}>
                <View style={{
                    marginTop: "20%",
                    alignSelf: "center"
                }}>
                    <Image
                        source={imageConstants.Logo}
                        resizeMode='contain'
                        style={{
                            width: Width * 0.60,
                            height: 50
                            // backgroundColor:"pink"
                        }}
                    />
                </View>


                <Text style={styles.registerText}>{screenName == 'Forget' ? "Forgot Password" : "Change Password"}</Text>

                <Text style={styles.subText}>Please change your login password</Text>

                <KeyboardAwareScrollView
                    extraHeight={moderateScale(100)}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        //   backgroundColor:"pink"
                    }}>

                    <CustomText
                        Title={"New Password"}
                        marginTop={30}
                    />

                    <View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        // justifyContent:"center",
                        alignSelf: "center",
                    }}
                    // b>
                    >
                        <CustomInput
                            textWidth={"80%"}
                            inputImage={true}
                            secureTextEntry={showFlagNew}
                            source={imageConstants.lock}
                            placeholder={"Enter New Password"}
                            fontSize={13}
                            imageWidth={15}
                            // marginLeft={"2%"}
                            width={"90%"}
                            margin={15}
                            onChangeText={text => setOldPassword(text)}
                            value={oldPassword}
                        />
                        <TouchableOpacity
                            onPress={() => updateState({ ...iState, showFlagNew: !showFlagNew })}
                            style={{
                                position: "absolute",
                                right: 20,
                                bottom: "25%",
                            }}>
                            <Image
                                source={showFlagNew ? imageConstants.closeEye : imageConstants.openEye}
                                resizeMode='contain'
                                style={{
                                    width: 20,
                                    height: 20
                                }}
                            />
                        </TouchableOpacity>
                    </View>

                    <CustomText
                        Title={"Confirm Password"}
                        marginTop={30}
                    />




                    <View style={{
                        flexDirection: "row",
                        alignItems: "center",
                        // justifyContent:"center",
                        alignSelf: "center",
                    }}

                    >
                        <CustomInput
                            textWidth={"80%"}
                            inputImage={true}
                            source={imageConstants.lock}
                            placeholder={"Confirm Password"}
                            secureTextEntry={showFlagConfirm}
                            fontSize={13}
                            imageWidth={15}
                            // marginLeft={"2%"}
                            width={"90%"}
                            margin={15}
                            onChangeText={text => setNewPassword(text)}
                            value={newPassword}
                        />

                        <TouchableOpacity
                            onPress={() => updateState({ ...iState, showFlagConfirm: !showFlagConfirm })}
                            style={{
                                position: "absolute",
                                right: 20,
                                bottom: "25%",
                            }}>
                            <Image
                                source={showFlagConfirm ? imageConstants.closeEye : imageConstants.openEye}
                                resizeMode='contain'
                                style={{
                                    width: 20,
                                    height: 20
                                }}
                            />
                        </TouchableOpacity>
                    </View>

                    <CustomButton
                        disable={loading ? true : false}
                        isLaoding={loading}
                        OnButtonPress={validationCheck}
                        buttonText={screenName == 'Forget' ? "Save Password" : "Change Password"}
                        marginTop={50}
                    />
                </KeyboardAwareScrollView>


            </View>
        </>

    )
}

export default ChangePassword

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: colorConstant.backgroundColor,
    },

    stepText: {
        position: "absolute",
        right: 15,
        top: "11.5%",
        fontFamily: fontConstant.bold,
        color: colorConstant.black,
        fontSize: 13
    },

    registerText: {
        textAlign: "center",
        fontFamily: fontConstant.bold,
        fontSize: 20,
        color: colorConstant.black,
        marginTop: moderateVerticalScale(20)
    },

    subText: {
        fontFamily: fontConstant.regular,
        fontSize: 14,
        marginTop: 8,
        color: colorConstant.black,
        textAlign: "center",
        lineHeight: 25,
        paddingRight: "2%",
        paddingLeft: "2%"
    },

    codeView: {
        backgroundColor: colorConstant.white,
        padding: moderateScale(14),
        width: Width * 0.15,
        borderRadius: 5,
    },
    codeText: {

    },

    numberView: {
        paddingHorizontal: moderateScale(20),
        marginTop: moderateVerticalScale(14)
    },

    dividerView: {
        borderBottomWidth: 0.8,
        borderBottomColor: colorConstant.secondaryText,
        width: Width * 0.40,
        marginRight: "2%"
    },
    dividerView1: {
        borderBottomWidth: 0.8,
        borderBottomColor: colorConstant.secondaryText,
        width: Width * 0.40,
        marginLeft: "2%"
    },

    dividerContainer: {
        flexDirection: "row",
        alignItems: "center",
        alignSelf: "center",
        marginTop: moderateVerticalScale(20)
    },

    orText: {
        fontFamily: fontConstant.medium,
        color: colorConstant.secondaryText + 90,
        fontSize: 14
    },

    signText: {
        textAlign: "center",
        fontFamily: fontConstant.regular,
        fontSize: 14,
        color: colorConstant.secondaryText,
        marginTop: moderateVerticalScale(30)
    },

    socialView: {
        flexDirection: "row",
        alignItems: "center",
        alignSelf: "center",
        marginTop: moderateVerticalScale(10)
    },

    userText: {
        fontFamily: fontConstant.regular,
        fontSize: 14,
        textAlign: "center",
        marginTop: moderateVerticalScale(20),
        color: colorConstant.black
    }
})