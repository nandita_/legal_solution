import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

import {
  colorConstant,
  fontConstant,
  imageConstants,
} from '../../utils/constants';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { Width } from '../../dimensions/dimensions';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import CustomText from '../../Custom/CustomText';
import CustomInput from '../../Custom/CustomInput';
import CustomButton from '../../Custom/CustomButton';
import CustomDropdown from '../../Custom/CustomDropdown';
import { store } from '../../utils/store';
import { setIntroStatus } from '../../redux/ParentFlow';
import { CityApi, CountryApi, StateApi, signupApi } from '../../redux/Slices/APISlices';
import { Country, State, City } from 'country-state-city';
import toastShow from '../../utils/ToastMessage';
import AsyncStorage from '@react-native-async-storage/async-storage';

const initialState = {
  country: '',
  countryFlag: '',
  image: '',
  state: '',
  city: '',
  error: '',
};

const CountryScreen = props => {
  const [iState, updateState] = useState(initialState);
  const [icountryCode, setCountryCode] = useState();
  const [istate, setStateCode] = useState();
  const [icity, setCityCode] = useState();
  const [countryArr, setCountryArr] = useState([])
  const [stateArr, setStateArr] = useState([])
  const [cityArr, setCityArr] = useState([])
  const [stateIsoCode, setStateIsoCode] = useState()
  const [checkbox, setStateCheckBox] = useState(false);
  const {
    //  phoneNumber,
    country,
    countryFlag,
    image,
    state,
    city,
  } = iState;
  const { firstName, lastName, phoneNumber, countryCode, email } =
    props.route.params ?? '';
  const [loading, setLoading] = useState(false)
  // console.log(firstName, lastName,phoneNumber, countryCode, email);

  const SignupApiFunction = async () => {
    let checkToken = await AsyncStorage.getItem('fcmToken');
    console.log(checkToken , "fcmToken========>>>>>>>>>>>>>>");
    setLoading(true)
    let reqData = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      phoneNumber: phoneNumber,
      image: image,
      countryCode: countryCode,
      deviceToken: checkToken,
      country:
      {
        name: country.label,
        isoCode: country.value
      },
      countryFlag: countryFlag,
      state: {
        name: state.label,
        isoCode: state.value,
        countryCode: country.value
      },
      city: {
        name: city.label,
        isoCode: state.value
      },
    };
    console.log(reqData.deviceToken, "reqData=======>");
    try {
      let response = await store.dispatch(signupApi(reqData));
      if (response.payload.status === 200) {
        setLoading(false)
        console.log(response.payload.data, 'SIGNUPP RSPONSE=======>>>>>>>>');
        props.navigation.navigate("Login")
        // store.dispatch(setIntroStatus({introStatus: 'main'}));
      } else {
        setLoading(false)
        console.log(response.payload, 'ELSE RESPONSE =======>>>>>>');
      }
    } catch (error) {
      console.log(error.message, 'SIGNUP CATCH ERRORR======>>>>');
    }
  };

  const countryData = async () => {
    try {
      let response = await store.dispatch(CountryApi())
      if (response.payload.status === 200) {
        // console.log(response.payload.data[0].name, "Country Data ========>>>>>>>>");
        let temCountry = response.payload.data
        let resp = temCountry?.length > 0 ? temCountry?.map(country => { return { label: country.name, value: country.isoCode } }).flat() : []
        setCountryArr(resp)
      }
      else {
        console.log(response.payload.message, "Else response ==========>>");
      }
    } catch (error) {
      console.log(error?.response?.data ?? error?.message, "CATCH country ERROR ========>>>");
    }
  };

  console.log(country, state, city, "araayyyy checkkkk ==========>>>>>>>>>");

  const stateData = async (countryIsoCode) => {
    try {
      let response = await store.dispatch(StateApi(countryIsoCode))
      if (response.payload.status === 200) {
        // console.log(response.payload.data[0].name, "State Data ========>>>>>>>>");
        let temState = response.payload.data
        let resp = temState?.length > 0 ? temState?.map(state => { return { label: state.name, value: state.isoCode } }).flat() : []

        setStateArr(resp)
      }
      else {
        console.log(response.payload.message, "Else response ==========>>");
      }
    } catch (error) {
      console.log(error?.response?.data ?? error?.message, "CATCH country ERROR ========>>>");
    }
  };

  const cityData = async (stateIsoCode) => {
    try {
      let response = await store.dispatch(CityApi(stateIsoCode))
      if (response.payload.status === 200) {
        // console.log(response.payload.data[0].name, "State Data ========>>>>>>>>");
        let temCity = response.payload.data
        let resp = temCity?.length > 0 ? temCity?.map(city => { return { label: city.name, value: city.name } }).flat() : []
        setCityArr(resp)
      }
      else {
        console.log(response.payload.message, "Else response ==========>>");
      }
    } catch (error) {
      console.log(error?.response?.data ?? error?.message, "CATCH country ERROR ========>>>");
    }
  };

  useEffect(() => {
    countryData()
  }, [])

  const validationCheck = () => {
    console.log(country, state, city, "validation check========>>>>>>>");
    country.label === ""
      ? toastShow("Please select country", colorConstant.red)
      : state.label === ""
        ? toastShow("Please select state", colorConstant.red)
        : city.label === ""
          ? toastShow("Please select city", colorConstant.red)
          : checkbox === false
            ? toastShow("please check term & condition", colorConstant.red)
            : SignupApiFunction()
  };

  return (
    <>
      <StatusBar
        barStyle={'dark-content'}
        translucent={true}
        // backgroundColor={props.bgColor?props.bgColor:"white"}
        backgroundColor={'transparent'}
      />
      <View style={styles.main}>
        <View style={{
          marginTop: "20%",
          alignSelf: "center",
          flexDirection: "row",
          justifyContent: "space-between",
          width: "90%"
        }}>

          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <Image
              source={imageConstants.BackIcon}
              resizeMode='contain'
              style={{
                width: 20,
                height: 20
              }}
            />
          </TouchableOpacity>
          <Image
            source={imageConstants.Logo}
            resizeMode='contain'
            style={{
              width: Width * 0.50,
              height: 50,
              alignSelf: "center",
              // backgroundColor:"pink"
            }}
          />
          <View></View>
        </View>
        <Text style={styles.stepText}>
          Step 3
          <Text
            style={{
              color: colorConstant.secondaryText,
              fontFamily: fontConstant.bold,
            }}>
            /3
          </Text>
        </Text>

        <Text style={styles.registerText}>Your Current City</Text>

        <Text style={styles.subText}>Select Country, State & City</Text>

        <CustomText Title={'Country'} marginTop={20} />

        <CustomDropdown
          source={imageConstants.mail}
          placeholder={'Country'}
          fontSize={13}
          imageWidth={15}
          width={'90%'}
          margin={15}
          data={countryArr}
          value={country.value}
          search={true}
          onChange={item => {
            updateState({ ...iState, country: item, });
            // setCountryCode(item.value);
            // console.log(item.value, "huihui")
            setStateIsoCode(item.value)
            stateData(item.value)
            setStateArr([])
            setCityArr([])
          }}
        />

        <CustomText Title={'State'} marginTop={20} />

        <CustomDropdown
          source={imageConstants.mail}
          placeholder={'State/Province'}
          fontSize={13}
          imageWidth={15}
          // marginLeft={"2%"}
          width={'90%'}
          margin={15}
          data={stateArr}
          value={state.value}
          onChange={item => {
            updateState({ ...iState, state: item, });
            cityData(stateIsoCode + '/' + item.value)
            setCityArr([])
          }}
        />

        <CustomText Title={'City'} marginTop={20} />

        <CustomDropdown
          source={imageConstants.mail}
          placeholder={'City'}
          fontSize={13}
          imageWidth={15}
          // marginLeft={"2%"}
          width={'90%'}
          margin={15}
          data={cityArr}
          value={city.value}
          onChange={item => {
            updateState({ ...iState, city: item, });
            setCityCode(item.value)
          }}
        />

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: moderateScale(20),
            marginTop: moderateVerticalScale(20),
          }}>
          <TouchableOpacity style={checkbox ? styles.checkBoxFill : styles.checkbox} onPress={() => {
            checkbox ? setStateCheckBox(false) : setStateCheckBox(true);
          }} />
          <Text style={styles.agreeText}>
            I Accept the{' '}
            <Text
              onPress={() => props.navigation.navigate('TermsCondition')}
              style={{
                color: colorConstant.blue,
                textDecorationLine: 'underline',
              }}>
              TERMS & CONDITIONS
            </Text>
          </Text>
        </View>

        <CustomButton
          disable={loading ? true : false}
          isLoading={loading}
          OnButtonPress={validationCheck}
          buttonText={'Start Free Trial'}
          marginTop={30}
        />
      </View>
    </>
  );
};

export default CountryScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.backgroundColor,
  },

  stepText: {
    position: 'absolute',
    right: 15,
    top: '11.5%',
    fontFamily: fontConstant.bold,
    color: colorConstant.black,
    fontSize: 13,
  },

  registerText: {
    textAlign: 'center',
    fontFamily: fontConstant.bold,
    fontSize: 20,
    color: colorConstant.black,
    marginTop: moderateVerticalScale(20),
  },

  subText: {
    fontFamily: fontConstant.regular,
    fontSize: 16,
    marginTop: 10,
    color: colorConstant.secondaryText,
    textAlign: 'center',
    lineHeight: 25,
    paddingRight: '2%',
    paddingLeft: '2%',
  },

  checkbox: {
    backgroundColor: colorConstant.white,
    // width:"2%",
    padding: moderateVerticalScale(6),
    borderRadius: 3,
  },

  checkBoxFill: {
    backgroundColor: colorConstant.blue,
    // width:"2%",
    padding: moderateVerticalScale(6),
    borderRadius: 3,
  },

  agreeText: {
    marginLeft: '3%',
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.secondaryText,
  },
});
