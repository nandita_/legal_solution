import React, { useEffect, useState } from 'react';
import { FlatList, Image, ImageBackground, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native';


import { colorConstant, fontConstant, imageConstants } from '../../utils/constants';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { Width } from '../../dimensions/dimensions';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import CustomText from '../../Custom/CustomText';
import CustomInput from '../../Custom/CustomInput';
import CustomButton from '../../Custom/CustomButton';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import toastShow from '../../utils/ToastMessage';
import { store } from '../../utils/store';
import { forgotOtpApi } from '../../redux/Slices/APISlices';
import { useSelector } from 'react-redux';


const initialState = {
    email: "",
    error:"",
  }

const ForgetPassword = (props) => {
   
    const [iState, updateState] = useState(initialState);
    const {
      email,
    } = iState
    const [padding, setPadding] = useState(20);

    useEffect(() => {
        setPadding(200);
    }, []);

    const ForgotPasswordApi = async () => {
        let reqData = {
            email: email
        }
        try {
            let response = await store.dispatch(forgotOtpApi(reqData))
            if (response.payload.status === 200) {
                toastShow(response.payload.data.otp, colorConstant.green)
                console.log(response.payload, "FORGOT API HIT======>>>>>>");
                props.navigation.navigate("Verify", {screenName:"Forget", email, otpData: response.payload.data})
            }
            else {
                // toastShow(response.payload.message, colorConstant.red)
                console.log(response.payload, "RESPONSE IN ELSEE====>>>>>");
            }
        } catch (error) {
            console.log(error.message, "CATCH ERRORRRR======>>>>>>");
        }
    }  

      const  validationCheck=()=>{
        email == "" 
        ? toastShow("Please enter a email",colorConstant.red)
        : ForgotPasswordApi()
      };

    return (
        <>
            <StatusBar barStyle={'dark-content'} translucent={true}
                // backgroundColor={props.bgColor?props.bgColor:"white"}
                backgroundColor={'transparent'}
            />
            <View style={styles.main}>
                <View style={{
                    marginTop: "20%",
                    alignSelf: "center",
                    flexDirection:"row",
                    justifyContent:"space-between",
                    width:"90%"
                }}>
                    <TouchableOpacity onPress={() => props.navigation.goBack()}>
                    <Image 
                    source={imageConstants.BackIcon}
                    resizeMode='contain'
                    style={{
                        width:20,
                        height:20
                    }}
                    />
                    </TouchableOpacity>
                    <Image
                        source={imageConstants.Logo}
                        resizeMode='contain'
                        style={{
                            width: Width * 0.60,
                            height: 50
                            // backgroundColor:"pink"
                        }}
                    />
                 <View></View>
                </View>


                <Text style={styles.registerText}>Forgot password</Text>

                <Text style={styles.subText}>Enter email for the OTP verification</Text>

                <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                    extraHeight={moderateScale(100)}
                    contentContainerStyle={{
                        //   backgroundColor:"pink"
                    }}>


                    <CustomText
                        Title={"Email ID"}
                        marginTop={30}
                    />

                    <CustomInput
                        onChangeText={(text) => updateState({ ...iState, email: text })}
                        value={email}
                        textWidth={"100%"}
                        inputImage={true}
                        source={imageConstants.mail}
                        placeholder={"Eg- test@yourmail.com"}
                        fontSize={13}
                        // marginLeft={"2%"}
                        width={"90%"}
                        margin={15}
                    />
                    <CustomButton
                        OnButtonPress={() => validationCheck()}
                        buttonText={"Continue"}
                        marginTop={30}
                    />
                </KeyboardAwareScrollView>


            </View>
        </>

    )
}

export default ForgetPassword

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: colorConstant.backgroundColor,
    },

    stepText: {
        position: "absolute",
        right: 15,
        top: "11.5%",
        fontFamily: fontConstant.bold,
        color: colorConstant.black,
        fontSize: 13
    },

    registerText: {
        textAlign: "center",
        fontFamily: fontConstant.bold,
        fontSize: 20,
        color: colorConstant.black,
        marginTop: moderateVerticalScale(20)
    },

    subText: {
        fontFamily: fontConstant.regular,
        fontSize: 16,
        marginTop: 8,
        color: colorConstant.black,
        textAlign: "center",
        lineHeight: 25,
        paddingRight: "2%",
        paddingLeft: "2%"
    },

    codeView: {
        backgroundColor: colorConstant.white,
        padding: moderateScale(14),
        width: Width * 0.15,
        borderRadius: 5,
    },
    codeText: {

    },

    numberView: {
        paddingHorizontal: moderateScale(20),
        marginTop: moderateVerticalScale(14)
    },

    dividerView: {
        borderBottomWidth: 0.8,
        borderBottomColor: colorConstant.secondaryText,
        width: Width * 0.40,
        marginRight: "2%"
    },
    dividerView1: {
        borderBottomWidth: 0.8,
        borderBottomColor: colorConstant.secondaryText,
        width: Width * 0.40,
        marginLeft: "2%"
    },

    dividerContainer: {
        flexDirection: "row",
        alignItems: "center",
        alignSelf: "center",
        marginTop: moderateVerticalScale(20)
    },

    orText: {
        fontFamily: fontConstant.medium,
        color: colorConstant.secondaryText + 90,
        fontSize: 14
    },

    signText: {
        textAlign: "center",
        fontFamily: fontConstant.regular,
        fontSize: 14,
        color: colorConstant.secondaryText,
        marginTop: moderateVerticalScale(30)
    },

    socialView: {
        flexDirection: "row",
        alignItems: "center",
        alignSelf: "center",
        marginTop: moderateVerticalScale(10)
    },

    userText: {
        fontFamily: fontConstant.regular,
        fontSize: 14,
        textAlign: "center",
        marginTop: moderateVerticalScale(20),
        color: colorConstant.black
    }
})