import React, { useEffect, useState } from 'react';
import { FlatList, Image, ImageBackground, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native';


import { colorConstant, fontConstant, imageConstants } from '../../utils/constants';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { Width } from '../../dimensions/dimensions';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import CustomText from '../../Custom/CustomText';
import CustomInput from '../../Custom/CustomInput';
import CustomButton from '../../Custom/CustomButton';
import { store } from '../../utils/store';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import { emailCheckApi, setEmailLogin, setIntroStatus, setPasswordLogin, setPlanType, setRememberMe } from '../../redux/Slices/APISlices';
import Validator from '../../utils/Validator';
import toastShow from '../../utils/ToastMessage';
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused } from '@react-navigation/native';
import moment from 'moment';



const initialState = {
  showFlag: true,
  email: "",
  password: "",
  passwordError: "",
  error: "",
}
const Login = (props) => {
  const dispatch = useDispatch();
  const isFocused = useIsFocused()
  const data = useSelector((state) => state.counter.apiDataFlow);
  const rememberMe = useSelector((state) => state.counter.apiDataFlow)
  console.log("rememberMe-=====>", data.userProfile);
  const [loading, setLoading] = useState(false);
  const [checkBox, setCheckBox] = useState(false)
  // console.log(data.plainPassword, "plainPassword=======>");
  const [iState, updateState] = useState(initialState);
  const {
    showFlag,
    email,
    password,
    passwordError,
  } = iState

  const [padding, setPadding] = useState(20);


  useEffect(() => {
    setPadding(200);
    if (rememberMe.rememberMe) {
      setCheckBox(true)
      updateState({ ...iState, email: rememberMe.emailLogin, password: rememberMe.passwordLogin })
    } else {
      setCheckBox(false)
      updateState({ ...iState, email: "", password: "" })
    }
  }, []);

  const EmailCheckFunction = async () => {
    setLoading(true)
    let reqData = {
      email: email,
      password: password,
    }
    try {
      let response = await store.dispatch(emailCheckApi(reqData))
      // console.log(response, "Check response");
      if (response.payload.status === 200) {
        if (response.payload.data.ispasswordChange === false) {
          setLoading(false)
          console.log(response, "EMAILLLLLLMCEHCKKKKKK==========>>>>>>>>>");
          //VALUE SAVE OF EMAIL, PASSWORD
          props.navigation.navigate("ChangePassword", { screenName: "SignUp", password })
        }
        else {
          setLoading(false)
          // console.log("response.payload.message", response.payload.data.planType);
          toastShow(response.payload.message, colorConstant.green)
          dispatch(setEmailLogin({ emailLogin: email }))
          dispatch(setPlanType({ PlanType: response.payload.data }))
          dispatch(setPasswordLogin({ passwordLogin: password }))
          dispatch(setRememberMe({ rememberMe: checkBox }))
          dispatch(setIntroStatus({ introStatus: "main" }))
        }
      }
      else {
        setLoading(false)
        // toastShow(response.payload.message, colorConstant.red)
        // console.log(response , "ELSEEEEE ERRORRRRRR======>>>>>");
      }
    } catch (error) {
      setLoading(false)
      console.log(error?.response?.data ?? error?.message, "CATCH ERROR ========>>>");
    }

  }

  useEffect(() => {
    if (!isFocused) {
      updateState({ ...iState, password: "", email: "" })
    }
  }, [isFocused])

  const validationCheck = () => {
    Validator.isEmpty(email)
      ? toastShow("Please enter Email Address", colorConstant.red)
      : !Validator.isEmail(email)
        ? toastShow("Please enter valid email", colorConstant.red)
        : Validator.isEmpty(password)
          ? toastShow("Please enter a password", colorConstant.red)
          : EmailCheckFunction()
  }

  return (
    <>
      <StatusBar barStyle={'dark-content'} translucent={true}
        // backgroundColor={props.bgColor?props.bgColor:"white"}
        backgroundColor={'transparent'}
      />
      <View style={styles.main}>
        <View style={{
          marginTop: "20%",
          alignSelf: "center"
        }}>
          <Image
            source={imageConstants.Logo}
            resizeMode='contain'
            style={{
              width: Width * 0.60,
              height: 50
              // backgroundColor:"pink"
            }}
          />
        </View>


        <Text style={styles.registerText}>Log In to SAREL</Text>

        <Text style={styles.subText}>Please login using login credentials sent to your registered mobile number</Text>


        <KeyboardAwareScrollView
          extraHeight={moderateScale(100)}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            // backgroundColor:"pink"
          }}>
          <CustomText
            Title={"Login ID"}
            marginTop={30}
          />


          <CustomInput
            keyboardType={'email-address'}
            onChangeText={(text) => updateState({ ...iState, email: text })}
            value={email}
            textWidth={"80%"}
            inputImage={true}
            source={imageConstants.mail}
            placeholder={"Eg- test@yourmail.com"}
            fontSize={13}
            imageWidth={15}
            // marginLeft={"2%"}
            width={"90%"}
            margin={15}
          />

          <CustomText
            Title={"Password"}
            marginTop={25}
          />


          <View style={{
            flexDirection: "row",
            alignItems: "center",
            // justifyContent:"center",
            alignSelf: "center",
            // backgroundColor:"pink"
          }}>
            <CustomInput
              onChangeText={(text) => updateState({ ...iState, password: text })}
              value={password}
              textWidth={"80%"}
              inputImage={true}
              source={imageConstants.lock}
              placeholder={"Enter Password"}
              // secureTextEntry={true}
              fontSize={13}
              secureTextEntry={showFlag}
              imageWidth={15}
              // marginLeft={"2%"}
              width={"90%"}
              margin={15}
            />

            <TouchableOpacity
              onPress={() => updateState({ ...iState, showFlag: !showFlag })}
              style={{
                position: "absolute",
                right: 20,
                bottom: "25%",
              }}>
              <Image
                source={showFlag ? imageConstants.closeEye : imageConstants.openEye}
                resizeMode='contain'
                style={{
                  width: 20,
                  height: 20
                }}
              />
            </TouchableOpacity>
          </View>

          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", width: Width * 0.90, alignSelf: "center", marginTop: moderateVerticalScale(15) }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <TouchableOpacity
                onPress={() => {
                  setCheckBox(!checkBox)
                }}>
                <Image
                  source={checkBox ? imageConstants.tickfill : imageConstants.remindMe}
                  resizeMode='contain'
                  style={{
                    width: 15,
                    height: 15
                  }}
                />
              </TouchableOpacity>
              <Text style={{
                fontFamily: fontConstant.regular,
                fontSize: 14,
                paddingHorizontal: moderateScale(8),
                color: colorConstant.black
              }}>Remember me</Text>
            </View>
            <Text
              onPress={() => props.navigation.navigate("ForgetPassword")}
              style={{
                fontFamily: fontConstant.regular,
                fontSize: 14,
                color: colorConstant.blue
              }}>Forgot password?</Text>
          </View>



          <CustomButton
            disabled={loading ? true : false}
            isLoading={loading}
            OnButtonPress={validationCheck}
            buttonText={"Login"}
            marginTop={30}
          />

          <View style={styles.dividerContainer}>
            <View style={styles.dividerView} />
            <Text style={styles.orText}>OR</Text>
            <View style={styles.dividerView1} />
          </View>

          {
            data?.userProfile?.plainPassword && (
              <Text style={{ color: colorConstant.black, fontSize: 12, textAlign: "center" }}>{data.userProfile.plainPassword}</Text>
            )
          }

          <Text style={styles.signText}>Sign In using</Text>

          {/* <View style={styles.socialView}>
            <Image source={imageConstants.google}
              resizeMode='contain'
              style={{
                width: 55,
                height: 55,
                marginRight: "1%"
              }}
            />
            <Image source={imageConstants.facebook}
              resizeMode='contain'
              style={{
                width: 55,
                height: 55,
                marginLeft: "1%"
              }}
            />
          </View> */}

          <Text style={styles.userText}>New user?<Text onPress={() => props.navigation.navigate("Register")} style={{ color: colorConstant.blue, fontFamily: fontConstant.medium }}>{"  "}Register now </Text></Text>
        </KeyboardAwareScrollView>
      </View>
    </>

  )
}

export default Login

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.backgroundColor,
  },

  stepText: {
    position: "absolute",
    right: 15,
    top: "11.5%",
    fontFamily: fontConstant.bold,
    color: colorConstant.black,
    fontSize: 13
  },

  registerText: {
    textAlign: "center",
    fontFamily: fontConstant.bold,
    fontSize: 20,
    color: colorConstant.black,
    marginTop: moderateVerticalScale(25)
  },

  subText: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    marginTop: 8,
    color: colorConstant.black,
    textAlign: "center",
    lineHeight: 25,
    paddingRight: "2%",
    paddingLeft: "2%"
  },

  codeView: {
    backgroundColor: colorConstant.white,
    padding: moderateScale(14),
    width: Width * 0.15,
    borderRadius: 5,
  },
  codeText: {

  },

  numberView: {
    paddingHorizontal: moderateScale(20),
    marginTop: moderateVerticalScale(14)
  },

  dividerView: {
    borderBottomWidth: 0.8,
    borderBottomColor: colorConstant.secondaryText,
    width: Width * 0.40,
    marginRight: "2%"
  },
  dividerView1: {
    borderBottomWidth: 0.8,
    borderBottomColor: colorConstant.secondaryText,
    width: Width * 0.40,
    marginLeft: "2%"
  },

  dividerContainer: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "center",
    marginTop: moderateVerticalScale(20)
  },

  orText: {
    fontFamily: fontConstant.medium,
    color: colorConstant.secondaryText,
    fontSize: 14
  },

  signText: {
    textAlign: "center",
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.black,
    marginTop: moderateVerticalScale(30)
  },

  socialView: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "center",
    marginTop: moderateVerticalScale(10)
  },

  userText: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    textAlign: "center",
    marginTop: moderateVerticalScale(20),
    color: colorConstant.black
  }
})