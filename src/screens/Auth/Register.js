import React, { useEffect, useState } from 'react';
import {FlatList, Image, ImageBackground, StatusBar, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { Width } from '../../dimensions/dimensions';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import CustomText from '../../Custom/CustomText';
import CustomInput from '../../Custom/CustomInput';
import CustomButton from '../../Custom/CustomButton';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import { store } from '../../utils/store';
import { registerApi } from '../../redux/Slices/APISlices';
import toastShow from '../../utils/ToastMessage';



const initialState = {
  email: "",
  error:"",
}
const Register = (props) => {
  const [iState, updateState] = useState(initialState);
  const [loading , setLoading] = useState(false);
  const {
    email,
  } = iState
  const [padding, setPadding] = useState(20);
  console.log(loading , "CHECK LOADING======>>>>>");
  useEffect(() => {
    setPadding(200);
  }, []);


  const RegisterApiFucntion = async () => {
    setLoading(true)
    let reqData = {
        email: email
    }
    try {
        let response = await store.dispatch(registerApi(reqData))
        if (response.payload.status === 200) {
            setLoading(false)
            toastShow(response?.payload?.data?.otp, colorConstant.green)
            props.navigation.navigate("Verify", {screenName:"SignUp" , email, otpData: response?.payload?.data})
            console.log(response.payload, "REGISTERRRR API HIT======>>>>>>");
        }
        else {
          setLoading(false)
            // toastShow(response.payload.message, colorConstant.red)
            console.log(response.payload, "RESPONSE IN ELSEE====>>>>>");
        }
    } catch (error) {
        console.log(error.message, "CATCH ERRORRRR======>>>>>>");

    }
}  


  const validationCheck=()=>{
    Validator.isEmpty(email)
    ? toastShow("Please enter a email",colorConstant.red)
    : !Validator.isEmail(email)
    ? toastShow("Please enter valid email",colorConstant.red)
    : RegisterApiFucntion()
  }

  return (
  <>
<StatusBar barStyle={'dark-content'} translucent={true}
                // backgroundColor={props.bgColor?props.bgColor:"white"}
                backgroundColor={'transparent'}
            />
    <View style={styles.main}>
      <View style={{
        marginTop:"20%",
        alignSelf:"center"
      }}>
      <Image 
      source={imageConstants.Logo}
      resizeMode='contain'
      style={{
        width:Width*0.60,
        height:50
        // backgroundColor:"pink"
      }}
      />
      </View>
      <Text style={styles.stepText}>Step 1<Text style={{color: colorConstant.secondaryText, fontFamily: fontConstant.bold}}>/3</Text></Text>

      <Text style={styles.registerText}>Register as a free user</Text>

      <Text style={styles.subText}>Free to Read, Green Judgments (India, Bangladesh, Pakistan,Sri Lanka, Nepal, Bhutan). Download & Print - <Text style={{fontFamily: fontConstant.semiBold , fontSize:16, color: colorConstant.black}}>Subscribe Now!</Text></Text>

      <KeyboardAwareScrollView
      showsVerticalScrollIndicator={false}
            // extraHeight={moderateScale(50)}
            contentContainerStyle={{
            //   backgroundColor:"pink"
            }}>

      <CustomText 
      Title={"Email ID"}
      marginTop={30}
      />
      
      <CustomInput 
      onChangeText={(text) => updateState({ ...iState, email: text })}
      value={email}
      textWidth={"100%"}
      inputImage={true}
      source={imageConstants.mail}
      placeholder={"Eg- test@yourmail.com"}
      fontSize={13}
      // marginLeft={"2%"}
      width={"90%"}
      margin={15}
      />

      <CustomButton 
      disabled={loading ? true : false}
      isLoading={loading}
      OnButtonPress={validationCheck}
      buttonText={"Continue"}
      marginTop={30}
      
      />
     
     <View style={styles.dividerContainer}>
      <View style={styles.dividerView}/>
      <Text style={styles.orText}>OR</Text>
      <View style={styles.dividerView1}/>
      </View>

      <Text style={styles.signText}>Sign In using</Text>

      {/* <View style={styles.socialView}>
        <Image source={imageConstants.google}
        resizeMode='contain'
        style={{
          width:55,
          height:55,
          marginRight:"1%"
        }}
        />
        <Image source={imageConstants.facebook}
         resizeMode='contain'
         style={{
          width:55,
          height:55,
          marginLeft:"1%"
        }}
        />
      </View> */}

      <Text style={styles.userText}>Already a user?<Text onPress={() => props.navigation.navigate("Login")} style={{color: colorConstant.blue, fontFamily: fontConstant.medium}}>{" "}Login</Text></Text>
      </KeyboardAwareScrollView>
      </View>
      </>

  )
}

export default Register

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.backgroundColor,
  },

  stepText:{
    position:"absolute",
    right:15,
    top:"11.5%",
    fontFamily: fontConstant.bold,
    color: colorConstant.black,
    fontSize:13
  },

  registerText:{
    textAlign:"center",
    fontFamily: fontConstant.bold,
    fontSize:20,
    color: colorConstant.black,
    marginTop:moderateVerticalScale(20)
  },

  subText:{
    fontFamily: fontConstant.regular,
    fontSize:16,
    marginTop:10,
    color: colorConstant.secondaryText,
    textAlign:"center",
    lineHeight:25,
    paddingRight:"2%",
    paddingLeft:"2%"
  },

  codeView:{
    backgroundColor: colorConstant.white,
    width:Width*0.15,
    borderRadius: 5,
    padding:"4.3%",
    // alignItems:"center",
  //  height:moderateVerticalScale(46),

  },
  codeText:{
   fontFamily:fontConstant.regular,
   fontSize:13,
   
  //  backgroundColor:"pink",
   textAlign:"center",
   color: colorConstant.secondaryText
  },

  numberView:{
    justifyContent:"space-between",
    // backgroundColor:"pink",
    // marginLeft:"2%",
    width:"100%",
    flexDirection:"row",
    alignItems:"center",
   paddingHorizontal:moderateScale(20),
   marginTop: moderateVerticalScale(14)
  },

  dividerView:{
    borderBottomWidth:0.8,
    borderBottomColor:colorConstant.secondaryText,
    width:Width*0.40,
    marginRight:"2%"
  },
  dividerView1:{
    borderBottomWidth:0.8,
    borderBottomColor:colorConstant.secondaryText,
    width:Width*0.40,
    marginLeft:"2%"
  },

  dividerContainer:{
    flexDirection:"row",
    alignItems:"center",
    alignSelf:"center",
    marginTop: moderateVerticalScale(20)
  },

  orText:{
    fontFamily: fontConstant.medium,
    color: colorConstant.secondaryText + 90,
    fontSize: 14
  },

  signText:{
    textAlign:"center",
    fontFamily: fontConstant.regular,
    fontSize:14,
    color: colorConstant.secondaryText,
    marginTop: moderateVerticalScale(30)
  },

  socialView:{
    flexDirection:"row",
    alignItems:"center",
    alignSelf:"center",
    marginTop: moderateVerticalScale(10)
  },

  userText:{
    fontFamily: fontConstant.regular,
    fontSize:14,
    textAlign:"center",
    marginTop: moderateVerticalScale(20),
     color: colorConstant.black
  }
})