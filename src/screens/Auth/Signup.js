import React, { useState } from 'react';
import { FlatList, Image, ImageBackground, StatusBar, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';


import { colorConstant, fontConstant, imageConstants } from '../../utils/constants';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { Width } from '../../dimensions/dimensions';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import CustomText from '../../Custom/CustomText';
import CustomInput from '../../Custom/CustomInput';
import CustomButton from '../../Custom/CustomButton';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import Validator from '../../utils/Validator';
import toastShow from '../../utils/ToastMessage';
import CountryPicker from "rn-country-picker";

const initialState = {
  firstName: "",
  lastName: "",
  phoneNumber: "",
  countryCode: "+91",
  error: "",
}
const Signup = (props) => {
  const { email } = props.route.params ?? "";
  const [iState, updateState] = useState(initialState);
  const {
    phoneNumber,
    firstName,
    lastName,
    countryCode,
  } = iState


  const validationCheck = () => {
    Validator.isEmpty(firstName)
      ? toastShow("Please enter First Name", colorConstant.red)
      : Validator.isEmpty(lastName)
        ? toastShow("Please enter Last Name", colorConstant.red)
        : phoneNumber !== "" && !Validator.isphoneNumber(phoneNumber)
          ? toastShow("Please enter valid phoneNumber", colorConstant.red)
          : props.navigation.navigate("CountryScreen", { phoneNumber, firstName, lastName, countryCode, email })
  }
  return (
    <>
      <StatusBar barStyle={'dark-content'} translucent={true}
        // backgroundColor={props.bgColor?props.bgColor:"white"}
        backgroundColor={'transparent'}
      />
      <View style={styles.main}>
        <View style={{
          marginTop: "20%",
          alignSelf: "center",
          flexDirection: "row",
          justifyContent: "space-between",
          width: "90%"
        }}>
         
         <TouchableOpacity onPress={() => props.navigation.navigate("Register")}>
          <Image
            source={imageConstants.BackIcon}
            resizeMode='contain'
            style={{
              width: 20,
              height: 20
            }}
          />
          </TouchableOpacity>
          <Image
            source={imageConstants.Logo}
            resizeMode='contain'
            style={{
              width: Width * 0.50,
              height: 50,
              alignSelf: "center",
              // backgroundColor:"pink"
            }}
          />
          <View></View>
        </View>
        <Text style={styles.stepText}>Step 2<Text style={{ color: colorConstant.secondaryText, fontFamily: fontConstant.bold }}>/3</Text></Text>

        <Text style={styles.registerText}>Enter Name & Email ID</Text>

        <Text style={styles.subText}>Enter name & email ID to complete your profile</Text>

        <KeyboardAwareScrollView
          // extraHeight={moderateScale(50)}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            //   backgroundColor:"pink"
          }}>
          <View style={styles.nameContainer}>


            <View style={{
              width: "47%",
            }}>
              <CustomText
                Title={"First Name"}
                marginTop={1}
                width={"100%"}
              />

              <CustomInput
                onChangeText={(text) => updateState({ ...iState, firstName: text })}
                value={firstName}
                textWidth={"100%"}
                inputImage={true}
                fontSize={13}
                placeholder={"First Name"}
                imageWidth={15}
                source={imageConstants.userIcon}
                // padding={1}
                width={"100%"}
              />

            </View>

            <View style={{
              width: "47%",
            }}>
              <CustomText
                Title={"Last Name"}
                marginTop={1}
                width={"100%"}
              />

              <CustomInput
                onChangeText={(text) => updateState({ ...iState, lastName: text })}
                value={lastName}
                textWidth={"100%"}
                inputImage={true}
                fontSize={13}
                placeholder={"Last Name"}
                imageWidth={15}
                source={imageConstants.userIcon}
                // padding={1}
                width={"100%"}
              />

            </View>
          </View>




          <CustomText
            Title={"Mobile Number (optional)"}
            marginTop={20}
          />


          <View style={styles.numberView}>
            <CountryPicker
              disable={false}
              animationType={"slide"}
              language="en"
              containerStyle={styles.codeView}
              pickerTitleStyle={styles.pickerTitleStyle}
              // dropDownImage={require("./res/ic_drop_down.png")}
              selectedCountryTextStyle={styles.selectedCountryTextStyle}
              countryNameTextStyle={styles.countryNameTextStyle}
              pickerTitle={"Country Picker"}
              searchBarPlaceHolder={"Search......"}
              hideCountryFlag={true}
              hideCountryCode={false}
              searchBarStyle={styles.searchBarStyle}
              // backButtonImage={require("./res/ic_back_black.png")}
              // searchButtonImage={require("./res/ic_search.png")}
              countryCode={"91"}
            // selectedValue={selectedValue}
            />


            {/* <View style={styles.codeView}>
                <Text style={styles.codeText}>+91</Text>
              </View> */}
            <CustomInput
              onChangeText={(text) => updateState({ ...iState, phoneNumber: text })}
              value={phoneNumber}
              maxLength={10}
              inputImage={true}
              textWidth={"90%"}
              source={imageConstants.phone}
              placeholder={"Enter Mobile Number"}
              fontSize={13}
              // marginLeft={"2%"}
              width={"81%"}
              margin={1}
            />


          </View>


          <CustomButton
            OnButtonPress={validationCheck}
            buttonText={"Continue"}
            marginTop={30}
          />
        </KeyboardAwareScrollView>


      </View>
    </>

  )
}

export default Signup

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.backgroundColor,
  },

  stepText: {
    position: "absolute",
    right: 15,
    top: "11.5%",
    fontFamily: fontConstant.bold,
    color: colorConstant.black,
    fontSize: 13
  },

  registerText: {
    textAlign: "center",
    fontFamily: fontConstant.bold,
    fontSize: 20,
    color: colorConstant.black,
    marginTop: moderateVerticalScale(20)
  },

  subText: {
    fontFamily: fontConstant.regular,
    fontSize: 16,
    marginTop: 10,
    color: colorConstant.secondaryText,
    textAlign: "center",
    lineHeight: 25,
    paddingRight: "2%",
    paddingLeft: "2%"
  },

  nameContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: moderateVerticalScale(40),
    width: Width * 0.90,
    alignSelf: "center",
    justifyContent: "space-between"
  },


  codeView: {
    backgroundColor: colorConstant.white,
    width: Width * 0.15,
    borderRadius: 5,
    padding: "4.3%",
    // alignItems:"center",
    //  height:moderateVerticalScale(46),

  },
  codeText: {
    fontFamily: fontConstant.regular,
    fontSize: 13,

    //  backgroundColor:"pink",
    textAlign: "center",
    color: colorConstant.secondaryText
  },

  numberView: {
    justifyContent: "space-between",
    // backgroundColor:"pink",
    // marginLeft:"2%",
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: moderateScale(20),
    marginTop: moderateVerticalScale(14)
  },



})