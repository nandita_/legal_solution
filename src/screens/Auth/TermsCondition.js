import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import {fontConstant, imageConstants} from '../../utils/constants';
import { StaticContentApi } from '../../redux/Slices/APISlices';
import { store } from '../../utils/store';

const TermsCondition = () => {
const [data , setData] = useState("")
  const StaticContentFunction = async() => {
    try {
      let response = await store.dispatch(StaticContentApi('TERMS'));
      if(response.payload.status === 200){
        console.log("Static success", response);
        setData(response.payload.data.description)
      }
      else{
        console.log("Static else", response);
      }
    } catch (error) {
      console.log("Static success", error.response);
    }
  }

  useEffect(() => {
    StaticContentFunction()
  },[])
  return (
    <AppSafeAreaView
      title="Term & Conditions"
      bgColor="#D7F1FF">
         <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={{ fontFamily: fontConstant.regular,
        fontSize: 16,
        color: 'black',
        lineHeight: 30
        }}>
        {data} 
        </Text>
        </ScrollView>
      </AppSafeAreaView>
  );
};

export default TermsCondition;
