import React, { useEffect, useState } from 'react';
import { FlatList, Image, ImageBackground, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native';


import { colorConstant, fontConstant, imageConstants } from '../../utils/constants';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { Height, Width } from '../../dimensions/dimensions';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import CustomText from '../../Custom/CustomText';
import CustomInput from '../../Custom/CustomInput';
import CustomButton from '../../Custom/CustomButton';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { forgotOtpApi, registerApi } from '../../redux/Slices/APISlices';
import toastShow from '../../utils/ToastMessage';
import { store } from '../../utils/store';
import Validator from '../../utils/Validator';


const initialState = {
    otp: "",
    error: "",
}
const Verify = (props) => {
    const [iState, updateState] = useState(initialState);
    const [data, setData] = useState();
    const [loading, setLoading] = useState(false)
    const {
        otp,
    } = iState

    const { screenName, email } = props.route.params ?? "";
    console.log(screenName, email, "PARAMMMMSS");

    const RegisterApiFucntion = async () => {
        setLoading(true)
        let reqData = {
            email: email
        }
        try {
            let response = await store.dispatch(screenName == 'Forget' ? forgotOtpApi(reqData) : registerApi(reqData))
            if (response.payload.status === 200) {
                setLoading(false)
                toastShow(response.payload.data.otp, colorConstant.green)
                console.log(response.payload.data.otp,"REGISTERRRR API HIT======>>>>>>");
                setData(response.payload.data)
            }
            else {
                setLoading(false)
                toastShow(response.payload.message, colorConstant.red)
                console.log(response.payload, "RESPONSE IN ELSEE====>>>>>");
            }
        } catch (error) {
            console.log(error.message, "CATCH ERRORRRR======>>>>>>");

        }
    }

    useEffect(() => {
            setData(props.route.params.otpData) ?? ""
    }, [])

    const validationCheck = () => {
        Validator.isEmpty(otp)
            ? toastShow('Please enter OTP',colorConstant.red)
            : otp?.length != 4
                ? toastShow('OTP incomplete', colorConstant.red)
                : data?.otp != otp 
                ? toastShow('OTP Invalid', colorConstant.red)
                : screenName == 'Forget'
                ? props.navigation.navigate("ChangePassword",{screenName:"Forget", email})
                : props.navigation.navigate("Signup",{email})
    };
    return (
        <>
            <StatusBar barStyle={'dark-content'} translucent={true}
                // backgroundColor={props.bgColor?props.bgColor:"white"}
                backgroundColor={'transparent'}
            />
            <View style={styles.main}>
                <View style={{
                    marginTop: "20%",
                    alignSelf: "center"
                }}>
                    <Image
                        source={imageConstants.Logo}
                        resizeMode='contain'
                        style={{
                            width: Width * 0.60,
                            height: 50
                            // backgroundColor:"pink"
                        }}
                    />
                </View>

                <Text style={styles.registerText}>Verify code</Text>

                <Text style={styles.subText}>The verification code has sent to your email.</Text>

                <OTPInputView
                    style={{ width: Width, height: Height * 0.10, marginTop: moderateVerticalScale(10) }}
                    pinCount={4}
                    code={otp} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                    onCodeChanged={code => updateState({ ...iState, otp: code })}
                    autoFocusOnLoad={false}
                    codeInputFieldStyle={styles.underlineStyleBase}
                    codeInputHighlightStyle={styles.underlineStyleHighLighted}
                    borderStyleHighLighted={styles.borderStyleHighLighted}
                    borderStyleBase={styles.borderStyleBase}
                    onCodeFilled={(code => {
                        console.log(`Code is ${code}, you are good to go!`)
                    })}
                />


                <CustomButton
                    OnButtonPress={validationCheck}
                    buttonText={"Submit"}
                    marginTop={20}
                />







                <Text style={styles.userText}>Didn’t receive the code?<Text onPress={() => RegisterApiFucntion()} style={{ color: colorConstant.blue, fontFamily: fontConstant.medium }}>{" "}Resend</Text></Text>
            </View>
        </>

    )
}

export default Verify

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: colorConstant.backgroundColor,
    },

    stepText: {
        position: "absolute",
        right: 15,
        top: "11.5%",
        fontFamily: fontConstant.bold,
        color: colorConstant.black,
        fontSize: 13
    },

    registerText: {
        textAlign: "center",
        fontFamily: fontConstant.bold,
        fontSize: 20,
        color: colorConstant.black,
        marginTop: moderateVerticalScale(20)
    },

    subText: {
        fontFamily: fontConstant.regular,
        fontSize: 16,
        marginTop: 10,
        color: colorConstant.black,
        textAlign: "center",
        lineHeight: 25,
        paddingRight: "2%",
        paddingLeft: "2%"
    },

    codeView: {
        backgroundColor: colorConstant.white,
        padding: moderateScale(14),
        width: Width * 0.15,
        borderRadius: 5,
    },
    codeText: {

    },

    numberView: {
        justifyContent: "space-between",
        // backgroundColor:"pink",
        // marginLeft:"2%",
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: moderateScale(20),
        marginTop: moderateVerticalScale(14)
    },

    dividerView: {
        borderBottomWidth: 0.8,
        borderBottomColor: colorConstant.secondaryText,
        width: Width * 0.40,
        marginRight: "2%"
    },
    dividerView1: {
        borderBottomWidth: 0.8,
        borderBottomColor: colorConstant.secondaryText,
        width: Width * 0.40,
        marginLeft: "2%"
    },

    dividerContainer: {
        flexDirection: "row",
        alignItems: "center",
        alignSelf: "center",
        marginTop: moderateVerticalScale(20)
    },

    orText: {
        fontFamily: fontConstant.medium,
        color: colorConstant.secondaryText + 90,
        fontSize: 14
    },

    signText: {
        textAlign: "center",
        fontFamily: fontConstant.regular,
        fontSize: 14,
        color: colorConstant.secondaryText,
        marginTop: moderateVerticalScale(30)
    },

    socialView: {
        flexDirection: "row",
        alignItems: "center",
        alignSelf: "center",
        marginTop: moderateVerticalScale(10)
    },

    userText: {
        fontFamily: fontConstant.regular,
        fontSize: 14,
        textAlign: "center",
        marginTop: moderateVerticalScale(20),
        color: colorConstant.black
    },

    borderStyleBase: {
        width: 30,
        height: 45
    },

    borderStyleHighLighted: {
        borderColor: "#03DAC6",
    },

    underlineStyleBase: {
        width: 60,
        padding: "10%",
        alignSelf: "center",
        // height: 45,
        borderWidth: 0,
        fontFamily: fontConstant.regular,
        fontSize: 14,
        color: colorConstant.black,
        borderRadius: 5,
        textDecorationLine: "underline",
        textDecorationColor: "red",
        backgroundColor: colorConstant.white
    },

    underlineStyleHighLighted: {
        borderColor: "#999999",
        //  borderBottomWidth: 1,
        //  bottom:"2"
        // marginBottom:"2%"
    },
})