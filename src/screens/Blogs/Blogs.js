import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants';
import { Height, Width } from '../../dimensions/dimensions';
import SearchBar from '../../Components/SearchBar';
import { store } from '../../utils/store';
import { BlogListApi, PopularTagApi, TagCountApi } from '../../redux/Slices/APISlices';
import moment from 'moment';
import { useIsFocused } from '@react-navigation/native';
import RenderHTML from 'react-native-render-html';
import { moderateVerticalScale } from 'react-native-size-matters';


const Blogs = props => {
  const IsFocused = useIsFocused("")
  const [activeHeader, setActiveHeader] = useState(0);
  const [blogData, setBlogData] = useState([])
  const [blogsHeaderArr, setBLogsHeaderArr] = useState([])
  const [data, setData] = useState([]);


  const onSearch = text => {
    console.log(text, "SEARCVH TERXTTTTT");
    if (text.length === 0) {
      setBlogData(data);
    } else {
      const filteredData = data.filter(designer => {
        console.log(designer, "{{}}{}{}{}{}");
        const author = designer?.title?.toLowerCase();
        if (author?.includes(text?.toLowerCase())) {
          return true;
        }
      });
      setBlogData(filteredData);
    }
  };

  useEffect(() => {
    if (data.length > 0) {
      setBlogData(data);
    } else {
      setBlogData([]);
    }
  }, [data]);

  // tags api 
  const PopularTagFunction = async () => {
    try {
      let response = await store.dispatch(PopularTagApi())
      if (response.payload.status === 200) {
        let tempTags = response.payload?.data?.length > 0 && response.payload?.data?.map((item) => item.name);
        tempTags.unshift("All");
        setBLogsHeaderArr(tempTags);
      }
      else {
        console.log(response.payload.data.message, "ELSE TAG RESPONSE========>>>>>");
      }
    } catch (error) {
      console.log(error?.response?.data?.message ?? error?.message, "CATCH REPONSE=====>>>>>>");
    }
  }

  const BlogListingFunction = async (data) => {
    try {

      // let payload = ;
      // console.log("payload",payload);
      let response = await store.dispatch(BlogListApi(data == "All" ? "" : data))
      if (response.payload.status === 200) {
        console.log(response.payload.data.result, "BLOG RESPONSE=======>>>>>");
        setData(response.payload.data.result)

      }
      else {
        console.log(response.payload.message, "ELSE BLOG RESPONSE========>>>>>");
      }
    } catch (error) {
      console.log(error?.response?.data?.message ?? error?.message, "CATCH REPONSE=====>>>>>>");
    }
  }

  const TagCountFunction = async (data) => {
    try {
      let payload = data == "All" ? "" : data;
      let response = await store.dispatch(TagCountApi(payload))
      if (response.payload.status === 200) {
        console.log(response?.payload?.message, "TAG COUNT RESPONSE=======>>>>>");
      }
      else {
        console.log(response?.payload?.message, "ELSE TAG COUNT RESPONSE=========>>>>>")
      }
    } catch (error) {
      console.log(error?.response?.data ?? error?.message, "CATCH REPONSE=====>>>>>>");
    }
  }

  useEffect(() => {
    if (IsFocused) {
      BlogListingFunction("");
      PopularTagFunction()
      setActiveHeader(0)
    }
  }, [IsFocused])

  const tagsStyles = {
    body: {
      whiteSpace: 'normal',
      color: colorConstant.black,
      display: 'block',
      overflow: 'hidden',
      height: 35,
      fontSize: 14
      // backgroundColor: "pink"
    },
    span: {

    }
  };

  const onCellClick = (item, index) => {
    console.log("msms", item);
    setActiveHeader(index);
    BlogListingFunction(item);
    // TagCountFunction(item)
  }
  const HeaderCell = ({ item, index }) => (
    <TouchableOpacity style={headerStyle.headerVw}
      // onPress={() => setActiveHeader(index)}
      onPress={() => onCellClick(item, index)}
    >
      <Text style={[headerStyle.headerTxt, { fontFamily: activeHeader === index ? fontConstant.medium : fontConstant.regular }]}>{item}</Text>
      <View style={[headerStyle.headerBottomVw, { backgroundColor: activeHeader === index ? colorConstant.black : "", height: activeHeader === index ? 2 : 0, }]}></View>
    </TouchableOpacity>
  );

  const BlogListCell = ({ item, index }) => (
    // console.log(item)
    <TouchableOpacity style={BlogCellStyle.cellVw} activeOpacity={0.8} onPress={() => {
      // console.log(item?.categories?.[0]?.name, "ITEMMMM");
      TagCountFunction(item?.categories?.[0]?.name)
      props.navigation.navigate("BlogsDetail", { item })
    }}>
      <View style={BlogCellStyle.cellBottomVw}>
        <View style={{
          marginTop:30,
           width: "15%" , 
           height: "30%",
           overflow:"hidden",
           borderRadius: 10
        }}>
        <Image style={{  width: "100%", height: "100%",  }} resizeMode='cover' source={{ uri: item?.thumbnailImage }} />
        </View>
        <View style={BlogCellStyle.blogBottomVw}>
          <View style={BlogCellStyle.blogNameVw}>
            <Text style={BlogCellStyle.blogName}>{item?.author}</Text>
            <Text style={BlogCellStyle.blogDate}> • {moment(item?.createdAt).format("DD MMM YYYY")} • Company</Text>
          </View>
          <Text numberOfLines={2} style={BlogCellStyle.blogTitleTxt}>
            {item?.title}
          </Text>
          <Text 
          numberOfLines={2}
          style={BlogCellStyle.blogSubTitleTxt}>
{item?.blog }
          {/* <RenderHTML
            tagsStyles={tagsStyles}
            contentWidth={Width}
            source={{ html: }}
          /> */}
          {/* {getText(item?.blog ?? "")} */}
          </Text>

          <View style={BlogCellStyle.designResVw}>
            <Text style={BlogCellStyle.designTxt}>Design</Text>
            <Text style={BlogCellStyle.researchTxt}>Research</Text>
          </View>
        </View>
      </View>
      <View style={BlogCellStyle.lineVw}></View>
    </TouchableOpacity>
  );

  return (
    <View style={{ backgroundColor: '#D7F1FF', flex: 1 }}>
      <View style={{ backgroundColor: '#D7F1FF' }}>
        <View style={styles.appSafeAreaVw}>
          <Image style={styles.LogoIcon} source={imageConstants.Logo} />
          <TouchableOpacity activeOpacity={0.8} onPress={() => props.navigation.navigate("Notification")}>
            <Image source={imageConstants.bellIcon} resizeMode='contain' style={{ width: 20, height: 20 }} />
          </TouchableOpacity>
        </View>
        <View style={styles.searchBottomVw}>
          <View >
            <SearchBar
              onChangeText={text => onSearch(text)}
              containerStyle={styles.searchVw}
              placholder={'Search for tests'}
              label={''}
              type={'input'}
            />
          </View>
          {/* <TouchableOpacity>
            <Image source={imageConstants.FilterIcon} />
          </TouchableOpacity> */}
        </View>
        <FlatList
          data={blogsHeaderArr}
          renderItem={HeaderCell}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          keyExtractor={item => item.id}
          contentContainerStyle={{
            height: 60,
            // backgroundColor:"yellow",
            // width:Width
          }}
        />
        <FlatList
          data={blogData}
          renderItem={BlogListCell}
          showsVerticalScrollIndicator={false}
          keyExtractor={item => item.id}
          contentContainerStyle={{
            paddingLeft: 2,
            // height: Height
            paddingBottom: moderateVerticalScale(500),
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  appSafeAreaVw: {
    // paddingTop: 30,
    marginTop: "12%",
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 16,
  },
  LogoIcon: {
    height: 30,
    width: Width * 0.245,
    alignItems: 'flex-start',
    resizeMode: 'contain',
  },
  searchBottomVw: {
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    alignItems: 'center',
    padding: 16,
  },
  searchVw: {
    height: 43,
  },
});

const headerStyle = StyleSheet.create({
  headerVw: {
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 18,
    // backgroundColor:"pink",
    height: 60,
    // width:"40%"
  },

  headerTxt: {
    fontSize: 14,
    color: 'black',
    alignItems: 'center',
    alignSelf: 'center',
  },

  headerBottomVw: {


    width: '100%',
  },
});

const BlogCellStyle = StyleSheet.create({
  cellVw: {
    paddingHorizontal: 10
  },

  cellBottomVw: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },

  lineVw: {
    height: 1,
    backgroundColor: 'gray',
    width: '100%',
  },

  blogBottomVw: {
    width: '68%',
    paddingHorizontal:10
  },

  blogNameVw: {
    flexDirection: 'row',
    paddingTop: 13,
  },

  blogName: {
    fontWeight: '600',
    fontSize: 12,
    color: '#2B59FF',
  },

  blogDate: {
    fontWeight: '600',
    fontSize: 12,
    color: 'black',
  },

  blogTitleTxt: {
    fontWeight: '700',
    fontSize: 16,
    color: 'black',
    width: '80%',
  },

  blogSubTitleTxt: {
    fontFamily: fontConstant.medium,
    fontSize: 14,
    // width: '20%',
    color: '#4F5D77',
    // backgroundColor: "pink"
  },

  designResVw: {
    flexDirection: 'row',
  },

  designTxt: {
    fontWeight: '400',
    fontSize: 12,
    color: '#026AA2',
    backgroundColor: '#F0F9FF',
    padding: 2,
    marginTop: 8,
    marginRight: 8,
    marginBottom: 4,
    borderRadius: 3,
    marginBottom: 16,
  },
  researchTxt: {
    fontWeight: '400',
    fontSize: 12,
    color: '#C11574',
    backgroundColor: '#FDF2FA',
    padding: 2,
    paddingLeft: 10,
    marginTop: 8,
    marginBottom: 4,
    width: 69,
    height: 19,
    borderRadius: 3,
  },
});

export default Blogs;
