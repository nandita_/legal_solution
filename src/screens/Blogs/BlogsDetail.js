import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants';
import { Height, Width } from '../../dimensions/dimensions';
import moment from 'moment';
import RenderHTML from 'react-native-render-html';
import { moderateScale } from 'react-native-size-matters';

const BlogsDetail = (props) => {
  const { item } = props.route.params ?? "";
  console.log(item, "item log=======>>>>>>>>");
  

  return (
    <AppSafeAreaView title=" " bgColor="#D7F1FF" noPadding="false">
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.scrollVw}>
        <Text style={styles.headerVw}>{item?.title}</Text>
        <View style={styles.blogNameVw}>
          <Text style={styles.blogName}>{item?.author}</Text>
          <Text style={styles.blogDate}> • {moment(item?.createdAt).format("DD MMM YYYY")} • Company</Text>
        </View>
        <View style={styles.designResVw}>
          <Text style={styles.designTxt}>Design</Text>
          <Text style={styles.researchTxt}>Research</Text>
        </View>
        <View style={[styles.BlogImage, {
          overflow:"hidden", 
          borderRadius: 20,
}]}
>
        <Image style={styles.BlogImage} resizeMode='cover' source={{ uri: item?.image }} />
        </View>
        
        
        <Text style={{
          fontFamily: fontConstant.regular,
          fontSize: 14,
          marginTop:10
        }}>
          {item?.blog }
        </Text>
      </ScrollView>
    </AppSafeAreaView>
  );
};

const styles = StyleSheet.create({
  scrollVw: {
    paddingLeft: 16,
    paddingRight: 16,
  },

  headerVw: {
    fontWeight: '700',
    fontSize: 18,
    color: 'black',
  },

  blogNameVw: {
    flexDirection: 'row',
    paddingTop: 13,
  },

  blogName: {
    fontWeight: '600',
    fontSize: 12,
    color: '#2B59FF',
  },

  blogDate: {
    fontWeight: '600',
    fontSize: 12,
    color: 'black',
  },

  designResVw: {
    flexDirection: 'row',
    marginTop: 5,
  },

  designTxt: {
    fontWeight: '400',
    fontSize: 12,
    color: '#026AA2',
    backgroundColor: '#F0F9FF',
    padding: 2,
    marginTop: 8,
    marginRight: 8,
    marginBottom: 4,
    borderRadius: 3,
    marginBottom: 16,
  },
  researchTxt: {
    fontWeight: '400',
    fontSize: 12,
    color: '#C11574',
    backgroundColor: '#FDF2FA',
    padding: 2,
    paddingLeft: 10,
    marginTop: 8,
    marginBottom: 4,
    width: 69,
    height: 19,
    borderRadius: 3,
  },

  BlogImage: {
    width: "100%",
    height: Height * 0.30,
    // alignSelf: "center",
    
    // paddingBottom: 40,
    // backgroundColor: "pink"
  },

  descriptionTxt: {
    marginTop: 10,
    lineHeight: 30,
    // width: Width * 0.80,
    // backgroundColor: "pink"
    // marginHorizontal: moderateScale(20)
  },

});

export default BlogsDetail;