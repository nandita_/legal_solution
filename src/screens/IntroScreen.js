import {
    FlatList,
    Image,
    ImageBackground,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
  } from 'react-native';
  import React, {useCallback, useRef, useState} from 'react';
  import {moderateScale, moderateVerticalScale} from 'react-native-size-matters';
  
  import { Height, Width } from '../dimensions/dimensions';
  import { colorConstant, fontConstant, imageConstants } from '../utils/constants';
  import CustomButton from '../Custom/CustomButton';
import { useDispatch, useSelector } from 'react-redux';
import { setIntroStatus, setPlanType } from '../redux/Slices/APISlices';

  let indexx = Number(0)
  
  const IntroScreen = (props) => {
    const [currentPage, setCurrentPage] = useState(0);

    const dispatch = useDispatch()
    

    let flatListRef = useRef();
    const scrollToIndex = index => {
      flatListRef.current.scrollToIndex({index});
      setCurrentPage(index)
      indexx = parseInt(index)
      console.log("asdkas=>",currentPage, typeof indexx)
    };

    // const handleNextPress = () => {
    //     // Call the Swiper's scrollBy method to move to the next slide
    //     flatListRef.current.scrollBy(1);
    //   };

    const _onViewableItemsChanged = useCallback(({ viewableItems, changed }) => {
      setCurrentPage(viewableItems?.[0]?.key)
      indexx = parseInt(viewableItems?.[0]?.key)
      console.log("qwert=>",currentPage, typeof indexx)
    }, []);
    const _viewabilityConfig = {
      // waitForInteraction: true, 
      // viewAreaCoveragePercentThreshold: 95,
      itemVisiblePercentThreshold: 50
    }
    const handlePress = () => {
      console.log("===>huihui",currentPage, typeof indexx);
      // if(currentPage == 0){
      //   indexx = 1
       
      // }else if(currentPage == 1){
      //   setCurrentPage(2)
      // }
      indexx += Number(1)
      
      // setCurrentPage(indexx)
      // console.log
      console.log("===>huihuiashjdajsv",currentPage,typeof indexx);
      
      flatListRef.current.scrollToIndex({
        animated: true,
        index: indexx,
        viewPosition:0.5,
      });
      
    };

    const OnboardingData = [
      {screen: <Screen1 scrollToIndex={scrollToIndex} props={props} />},
      {screen: <Screen2 scrollToIndex={scrollToIndex}  props={props} />},
      {screen: <Screen3 />},
    ];
  
    // const handleScroll = event => {
    //   const {contentOffset} = event.nativeEvent;
    //   const page = Math.round(contentOffset.x / Width);
    //   setCurrentPage(page);
    // };
  
    const cardTittle = {
      0: `Search Environmental Judgments`,
      1: `Lorem ipsum dolor sit amet`,
      2: 'Lorem ipsum dolor sit amet',
    };
    const cardDescription = {
      0: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`,
      1: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`,
      2: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`,
    };
  
    const renderItem = ({item}) => {
      // console.log(item);
      return (
        <View style={{width: Width, height: Height}}>
          {item?.screen}
        </View>
      );
    };
  
    return (
    
      <View style={{flex:1, justifyContent: 'flex-end', backgroundColor:"white"}}>
       
        <View style={{
            position:"absolute",
            height: Height*0.75,
            width:Width,
            backgroundColor: colorConstant.themecolor,
            // backgroundColor:"yellow",
            marginTop: -30,
            justifyContent: 'flex-end',
            borderTopLeftRadius: 30,
            borderTopRightRadius: 30,
            paddingBottom: '10%' 
          }}>
         
            <View style={{flexDirection: 'row', alignSelf: 'center', position:"absolute", top:"35%"}}>
              {OnboardingData?.map((_, index) => (
                <View
                  key={index}
                  style={[
                    styles.paginationDot,
                    {
                      backgroundColor:
                        index === indexx
                          ? colorConstant.blue
                          : colorConstant.white,
                      width: index === indexx ? 30 : 13,
                      height: index === indexx ? 13 :13,
                      borderColor: index === indexx ? colorConstant.blue : colorConstant.black,
                      borderWidth: 0.5,
                    },
                  ]}
                />
              ))}
            </View>
           
            <View style={{position:"absolute", top:"50%" , alignSelf:"center"}}>
            <Text
              style={{
                width:"100%",
                color: colorConstant.black,
                fontFamily: fontConstant.bold,
                fontSize: 20,
                lineHeight: 25,
                textAlign: 'center',
                marginTop: moderateVerticalScale(0),
              }}>
              {cardTittle[currentPage]}
            </Text>
            <Text
              style={{
                color: colorConstant.black + 90,
                lineHeight: 25,
                fontFamily: fontConstant.regular, 
                fontSize:14,
                width:Width*0.85,
                // paddingLeft:"2%",
                textAlign: 'center',
                marginTop: 10
              }}>
              {cardDescription[currentPage]}
            </Text>
  
            
          </View>
         
        </View >
        <FlatList
        style={{ backgroundColor:"transparent"}}
          pagingEnabled
          // keyExtractor={item => item.id}
          ref={flatListRef}
          extraData={currentPage}
          horizontal
          showsHorizontalScrollIndicator={false}
          data={OnboardingData}
          renderItem={renderItem}
          onViewableItemsChanged={_onViewableItemsChanged}
            viewabilityConfig={_viewabilityConfig}
          // onScroll={handleScroll}
        />

       {currentPage == 2 ? (
        <>
        <CustomButton OnButtonPress={() =>{
          dispatch(setIntroStatus({introStatus:"auth"}))
        } }
        buttonText={"Get 6 Month Free Trial"}
        color={colorConstant.white} 
        bottom={"20%"}
      borderRadius={5}
        />
        
        <Text style={styles.skipText} onPress={() => dispatch(setIntroStatus({introStatus:"auth"}))}>Login</Text>
          </>
     
    ) : (
      <>
      <CustomButton buttonText={'Next'} 
      OnButtonPress={() => { 
        handlePress()
      }}
      color={colorConstant.white}
      bottom={"20%"}
      borderRadius={5}
      
      />
      <Text onPress={() => dispatch(setIntroStatus({introStatus:"auth"}))}
      style={styles.skipText}>Skip</Text>
      </>
    )}
    </View>
    );
  };
  
  export default IntroScreen;
  
  const styles = StyleSheet.create({
    paginationDot: {
      height: 10,
      width: 10,
      marginRight: 5,
      borderRadius: 100,
      
      
    },
    skipText:{
      textAlign:"center",
      bottom:"7%",
      color: colorConstant.blue,
      fontFamily: fontConstant.medium,
      fontSize:14

    }
  });
   
  const Screen1 = ({props}) => {

    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'transparent',
        }}>
        <Image
          style={{
            width: Width*0.80,
            height: Height*0.50,
            alignSelf:"center"
          }}
          source={imageConstants.Intro1}
          resizeMode="contain"
        />
      </View>
    );
  };
  const Screen2 = ({props}) => {

    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'transparent',

        }}>
        
        <Image
          style={{
            width: Width*0.80,
            height: Height*0.50,
            alignSelf:"center"
          }}
          source={imageConstants.Intro2}
          resizeMode="contain"
        />
      </View>
    );
  };
  const Screen3 = () => {
    return (
       <View
        style={{
          flex: 1,
          backgroundColor: 'transparent',
        }}>
        <Image
          style={{
            width: Width*0.80,
            height: Height*0.50,
            alignSelf:"center",
           
          }}
          source={imageConstants.Intro3}
          resizeMode="contain"
        />
      </View>
    );
  };
  