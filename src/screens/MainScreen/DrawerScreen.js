import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import { useSelector } from 'react-redux'

const DrawerScreen = (props) => {
  const userProfile = useSelector((state) => state.counter.apiDataFlow.userProfile)
  return (
    <View style={styles.main}>
      <TouchableOpacity onPress={() => props.navigation.goBack()}>
        <Image
          source={imageConstants.cross}
          resizeMode='contain'
          style={{
            alignSelf: 'flex-end',
            right: 20,
            width: 30,
            height: 30
          }}
        />
      </TouchableOpacity>

      <View style={styles.upperContent}>
        <Image
          source={userProfile?.image ? { uri: userProfile?.image } : imageConstants.DummyProfile}
          resizeMode='cover'
          style={{
            width: 50,
            height: 50,
            borderRadius: 50
          }}
        />
        <View style={{
          paddingHorizontal: moderateScale(10),
          // paddingTop:moderateVerticalScale(10)
        }}>
          <Text style={styles.userNameText}>{userProfile?.firstName}{" "}{userProfile?.lastName}</Text>
          <Text style={styles.emailText}>{userProfile?.email}</Text>
        </View>
      </View>

      <View style={styles.Container}>
        <TouchableOpacity style={styles.innerContainer} onPress={() => props.navigation.navigate("MyProfile")}>
          <Image
            source={imageConstants.DrawerUser}
            resizeMode='contain'
            style={{
              width: 24,
              height: 24
            }}
          />
          <Text style={styles.TextStyle}>My Profile</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.innerContainer} onPress={() => props.navigation.navigate("SubscriptionPlan")}>
          <Image
            source={imageConstants.DrawerUser}
            resizeMode='contain'
            style={{
              width: 24,
              height: 24
            }}
          />
          <Text style={styles.TextStyle}>Subscription</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.innerContainer} onPress={() => props.navigation.navigate("AboutPrivacyTerm", { screenName: 'ABOUT_US' })}>
          <Image
            source={imageConstants.Page}
            resizeMode='contain'
            style={{
              width: 24,
              height: 24
            }}
          />
          <Text style={styles.TextStyle}>About SAREL</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.innerContainer} onPress={() => props.navigation.navigate("ContactUs", { screenName: 'CONTACT_US' })}>
          <Image
            source={imageConstants.Calendar}
            resizeMode='contain'
            style={{
              width: 24,
              height: 24
            }}
          />
          <Text style={styles.TextStyle}>Contact Us</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.innerContainer} onPress={() => props.navigation.navigate("Setting")}>
          <Image
            source={imageConstants.Settings}
            resizeMode='contain'
            style={{
              width: 24,
              height: 24
            }}
          />
          <Text style={styles.TextStyle}>Settings</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.innerContainer} onPress={() => props.navigation.navigate("AboutPrivacyTerm", { screenName: 'PRIVACY' })}>
          <Image
            source={imageConstants.privacy}
            resizeMode='contain'
            style={{
              width: 24,
              height: 24
            }}
          />
          <Text style={styles.TextStyle}>Privacy Policy</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.innerContainer} onPress={() => props.navigation.navigate("LogOut")}>
          <Image
            source={imageConstants.LogoutDrawer}
            resizeMode='contain'
            style={{
              width: 24,
              height: 24
            }}
          />
          <Text style={[styles.TextStyle, { color: colorConstant.red }]}>Logout Account</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default DrawerScreen

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.themecolor,
    paddingTop: "30%"
  },

  upperContent: {
    paddingHorizontal: moderateVerticalScale(20),
    marginTop: moderateVerticalScale(20),
    flexDirection: "row",
    alignItems: "center"
  },

  userNameText: {
    fontFamily: fontConstant.bold,
    color: colorConstant.black,
    fontSize: 16,
    lineHeight: 20,
  },

  emailText: {
    fontFamily: fontConstant.regular,
    lineHeight: 25,
    fontSize: 14,
    color: colorConstant.black
  },
  Container: {
    paddingHorizontal: moderateVerticalScale(30),
    paddingTop: moderateVerticalScale(20)
  },

  innerContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: moderateVerticalScale(30)
  },

  TextStyle: {
    paddingHorizontal: moderateVerticalScale(10),
    fontFamily: fontConstant.medium,
    fontSize: 14,
    color: colorConstant.black
  }


})