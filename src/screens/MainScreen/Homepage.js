import { FlatList, Image, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants'
import { Width } from '../../dimensions/dimensions'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import { useSelector } from 'react-redux'
import { request, PERMISSIONS, RESULTS } from 'react-native-permissions';
import { checkNotifications } from 'react-native-permissions';
import messaging from '@react-native-firebase/messaging';
import FreeTrialModal from './FreeTrialModal'
import { stat } from 'react-native-fs'
import { NotificationApi } from '../../redux/Slices/APISlices'
import { store } from '../../utils/store'
import { useIsFocused } from '@react-navigation/native'

const Homepage = (props) => {
  const isFocused = useIsFocused()
  const PlanType = useSelector((state) => state.counter.apiDataFlow.PlanType)
  const freeTrial = useSelector((state) => state.counter.apiDataFlow.freeTrial)
  const userProfile = useSelector((state) => state.counter.apiDataFlow)
  console.log(userProfile.jwtToken, "cjecljdnk");
  const [notiCount, setNotiCount] = useState()





  const data = [
    {
      id: 1,
      title: "Word Search",
      arrowImage: imageConstants.ArrowRight,
      screenName: "WordSearch"
    },
    {
      id: 2,
      title: "Find by Law",
      arrowImage: imageConstants.ArrowRight,
      screenName: "ByLaw"
    },
    {
      id: 3,
      title: "Find by Citation",
      arrowImage: imageConstants.ArrowRight,
      screenName: "CitySearch"
    },
    {
      id: 4,
      title: "Find by Party Name",
      arrowImage: imageConstants.ArrowRight,
      screenName: "SearchPartyName"
    },
    {
      id: 5,
      title: "Find by Court Name",
      arrowImage: imageConstants.ArrowRight,
      screenName: "CourtName"
    },
    {
      id: 6,
      title: "Find by Case No.",
      arrowImage: imageConstants.ArrowRight,
      screenName: "CaseNumber"
    },
    {
      id: 7,
      title: "Find by Topic",
      arrowImage: imageConstants.ArrowRight,
      screenName: "ByTopic"
    },

  ]

  const NotificationFunction = async () => {
    try {
      let response = await store.dispatch(NotificationApi())
      if (response.payload.status === 200) {
        // console.log(response.payload.data, "?>??>?>?>?>?>?>?>???>?>?>?>?>?")
        setNotiCount(response.payload.data?.[0]?.notificationCount)
        console.log(response.payload.data?.[0]?.notificationCount, "]][][][][][][][]][][][");
      }
      else {
        toastShow(response.payload.message, colorConstant.red)
      }
    } catch (error) {
      console.log(error.response.message && error.message, "Catch error=============>>>>>");
    }
  }
  // console.log(notifyData?.[0].notification?.[0]?.title, ",.,..,.,.,.,.,.,.,");
  useEffect(() => {
   
    if (isFocused) {
      console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>");
      NotificationFunction()
    }
  
  }, [isFocused])

  useEffect(() => {
    const unSubcribe = messaging().onMessage(async () => {
        await NotificationFunction()
        return unSubcribe;
      })
      console.log("===============================");
    
  }, [])

  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity style={styles.divContainer} onPress={() => props.navigation.navigate(item.screenName)} activeOpacity={0.8}>

        <View style={styles.innerView}>
          <Text style={styles.titleText}>{item.title}</Text>
          <Image
            source={item.arrowImage}
            resizeMode='contain'
            style={{
              width: 20,
              height: 20
            }}
          />
        </View>

      </TouchableOpacity>
    )
  }

  useEffect(() => {
    if (PlanType.planType === "freeTrial" && freeTrial === false) {
      props.navigation.navigate('FreeTrialModal', { PlanType })
    }
  }, [])


  useEffect(() => {
    // checkPermission();
    requestPermission()

  }, []);


  const requestPermission = async () => {
    await request(PERMISSIONS.ANDROID.POST_NOTIFICATIONS);
  };

  return (
    <>
      <StatusBar barStyle={'dark-content'} translucent={true}
        // backgroundColor={props.bgColor?props.bgColor:"white"}
        backgroundColor={'transparent'}
      />
      <View style={styles.main}>
        <View style={styles.appSafeAreaVw}>
          <Image style={styles.LogoIcon} source={imageConstants.Logo} />
          <TouchableOpacity onPress={() => props.navigation.navigate("Notification")}>
            <Image source={imageConstants.bellIcon} resizeMode='contain' style={{ width: 20, height: 20 }} />
            {
              notiCount > 0 ?
                <View style={styles.NotifyContainer} >
                  <Text style={styles.notifyy}>{notiCount}</Text>
                </View>
                :
                <></>
            }
          </TouchableOpacity>
        </View>

        <Text style={styles.headerText}>The Ultimate in <Text style={{ color: colorConstant.blue }}>Environmental Legal Research<Text style={{ color: colorConstant.black }}> ™</Text></Text></Text>

        <Text style={styles.subText}>Utilize Advanced Search for precision or Search Tools to explore by Topic, Case, Citation, or section, etc.</Text>

        <View style={styles.Container}>

          <FlatList
            showsVerticalScrollIndicator={false}
            data={data}
            keyExtractor={item => item.id}
            renderItem={renderItem}
            ListHeaderComponent={<Text style={{ fontFamily: fontConstant.bold, color: colorConstant.black, fontSize: 16 }}>Search Court Orders</Text>}
            ListFooterComponent={
              <>
                <Text style={{ fontFamily: fontConstant.bold, color: colorConstant.black, fontSize: 16, marginTop: moderateVerticalScale(20) }}>Search for Legislations</Text>

                <TouchableOpacity style={styles.divContainer} onPress={() => props.navigation.navigate("SearchLegislation")} activeOpacity={0.8}>

                  <View style={styles.innerView}>
                    <Text style={styles.titleText}>Search Legislations</Text>
                    <Image
                      source={imageConstants.ArrowRight}
                      resizeMode='contain'
                      style={{
                        width: 20,
                        height: 20
                      }}
                    />
                  </View>

                </TouchableOpacity>
              </>

            }
          />


        </View>
      </View>
    </>
  )
}

export default Homepage

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.themecolor,
  },

  appSafeAreaVw: {
    marginTop: "12%",
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 16,
  },
  LogoIcon: {
    height: 30,
    width: Width * 0.245,
    alignItems: 'flex-start',
    resizeMode: 'contain',
  },

  headerText: {
    fontFamily: fontConstant.bold,
    fontSize: 25,
    color: colorConstant.black,
    //  backgroundColor:"pink",
    width: Width,
    paddingHorizontal: moderateScale(10)
  },

  subText: {
    paddingHorizontal: moderateScale(10),
    fontFamily: fontConstant.regular,
    color: colorConstant.black,
    fontSize: 13,
    lineHeight: 18,
    marginTop: moderateVerticalScale(8)
  },

  Container: {
    flex: 1,
    backgroundColor: colorConstant.backgroundColor,
    padding: "5%",
    marginTop: moderateVerticalScale(20)
  },

  divContainer: {
    // width:Width*0.90,
    // alignSelf:"center",
    borderRadius: 5,
    backgroundColor: colorConstant.blue,
    marginTop: moderateVerticalScale(15)
  },

  innerView: {
    padding: "4%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },

  titleText: {
    color: colorConstant.white,
    fontFamily: fontConstant.semiBold,
    fontSize: 14
  },

  notifyy: {
    color: "#fff",
    // top: moderateScale(),
    fontSize: moderateScale(10),
    textAlign: 'center',
    textAlignVertical: 'center',
  },

  NotifyContainer: {
    backgroundColor: colorConstant.red,
    // padding: "6%",
    height: moderateScale(14),
    width: moderateScale(14),
    borderRadius: moderateScale(20),
    position: "absolute",
    top: moderateScale(-4),
    zIndex: 999,
    right: moderateScale(5),
  }
})