import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'

import { colorConstant, fontConstant, imageConstants } from '../../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import { Width } from '../../dimensions/dimensions'
import AppSafeAreaView from '../../Components/AppSafeAreaView'
import CustomButton from '../../Custom/CustomButton'
import CustomText from '../../Custom/CustomText'
import CustomInput from '../../Custom/CustomInput'
import CustomDropdown from '../../Custom/CustomDropdown'
import CustomDropdownSearch from '../../Custom/CustomDropdownSearch'
import { store } from '../../utils/store'
import { CityApi, CountryApi, StateApi } from '../../redux/Slices/APISlices'
import { useSelector } from 'react-redux'
// import CustomInfoModal from '../Custom/CustomInfoModal'
// import CustomHeaderText from '../Custom/CustomHeaderText'

const initialState = {
  billingAddress: "",
  postalCode: "",
  country: "",
  state: "",
  city: "",
  designation: "",
  organization: ""
}

const BillingDetails = (props) => {
  const userProfile = useSelector((state) => state.counter.apiDataFlow.userProfile)
  const { data , firstName, lastName, email, phoneNumber, countryCode } = props.route.params;
  console.log(data , firstName, lastName, email, phoneNumber);
  const [iState, updateState] = useState(initialState);
  const {
    country,
    countryFlag,
    image,
    state,
    city,
    postalCode,
    designation,
    organization,
    billingAddress
  } = iState
  const [countryArr, setCountryArr] = useState([])
  const [stateArr, setStateArr] = useState([])
  const [cityArr, setCityArr] = useState([])
  const [stateIsoCode, setStateIsoCode] = useState()
  const [isInfoModalVisible, setInfoModalVisible] = useState(false);
  const toggleModal = () => {
    console.log('logout working');
    setInfoModalVisible(!isInfoModalVisible);
  };

  const countryData = async () => {
    try {
      let response = await store.dispatch(CountryApi())
      if (response.payload.status === 200) {
        // console.log(response.payload.data[0].name, "Country Data ========>>>>>>>>");
        let temCountry = response.payload.data
        let resp = temCountry?.length > 0 ? temCountry?.map(country => { return { label: country.name, value: country.isoCode } }).flat() : []
        setCountryArr(resp)
      }
      else {
        console.log(response.payload.message, "Else response ==========>>");
      }
    } catch (error) {
      console.log(error?.response?.data ?? error?.message, "CATCH country ERROR ========>>>");
    }
  };

  console.log(country, state, city, "araayyyy checkkkk ==========>>>>>>>>>");

  const stateData = async (countryIsoCode) => {
    try {
      let response = await store.dispatch(StateApi(countryIsoCode))
      if (response.payload.status === 200) {
        // console.log(response.payload.data[0].name, "State Data ========>>>>>>>>");
        let temState = response.payload.data
        let resp = temState?.length > 0 ? temState?.map(state => { return { label: state.name, value: state.isoCode } }).flat() : []

        setStateArr(resp)
      }
      else {
        console.log(response.payload.message, "Else response ==========>>");
      }
    } catch (error) {
      console.log(error?.response?.data ?? error?.message, "CATCH country ERROR ========>>>");
    }
  };

  const cityData = async (stateIsoCode) => {
    try {
      let response = await store.dispatch(CityApi(stateIsoCode))
      if (response.payload.status === 200) {
        // console.log(response.payload.data[0].name, "State Data ========>>>>>>>>");
        let temCity = response.payload.data
        let resp = temCity?.length > 0 ? temCity?.map(city => { return { label: city.name, value: city.name } }).flat() : []
        setCityArr(resp)
      }
      else {
        console.log(response.payload.message, "Else response ==========>>");
      }
    } catch (error) {
      console.log(error?.response?.data ?? error?.message, "CATCH country ERROR ========>>>");
    }
  };

  const mappingState = () => {
    updateState({
      ...iState,
      country: userProfile?.country?.name ?? "",
      countryFlag: userProfile?.countryFlag ?? "",
      image: userProfile?.image ?? "",
      state: userProfile?.state?.name ?? "",
      city: userProfile?.city?.name ?? "",
      postalCode: userProfile?.postalCode ?? "",
      designation: userProfile?.designation ?? "",
      organization: userProfile?.organization ?? "",
      billingAddress: userProfile?.billingAddress ?? ""

    })
  }

  useEffect(() => {
    // updateState({...iState , ...userProfile});
    mappingState();
    countryData()
  }, []);

  return (
    <AppSafeAreaView title="Billing Details" bgColor={colorConstant.backgroundColor}>
      <View style={styles.main}>

        

       
         
          <ScrollView 
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: moderateVerticalScale(100)
          }} >
            

            <CustomText
              Title={"Billing Address"}
              marginTop={40}
              width={"100%"}
            />

            <CustomInput
            value={billingAddress}
            onChangeText={(text) => updateState({ ...iState, billingAddress: text })}
              multiline={true}
              textAlignVertical={'top'}
              paddingTop={10}
              //  textAlignVertical={true}
              textWidth={"90%"}
              margin={10}
              fontSize={13}
              placeholder={"Enter Address"}
              imageWidth={15}
          height={moderateVerticalScale(100)}
              //  source={imageConstants.mail}
              padding={1}
              width={"100%"}
            />


            <View style={[styles.nameContainer, { marginTop: moderateVerticalScale(20) }]}>


              <View style={{
                width: "47%",
              }}>
                <CustomText
                  width={"100%"}
                  Title={"Postal Code"}
                  marginTop={1}
                />


                <CustomInput
                value={postalCode}
                onChangeText={(text) => updateState({ ...iState, postalCode: text })}
                  textWidth={"100%"}
                  margin={10}
                  fontSize={13}
                  placeholder={"Enter Postal Code"}
                  // imageWidth={15}
                  // source={imageConstants.mail}
                  padding={5}
                  width={"100%"}
                />

              </View>

              <View style={{
                width: "47%",
              }}>
                <CustomText
                  width={"100%"}
                  Title={"Country"}
                  marginTop={1}
                />

                <CustomDropdownSearch
                data={countryArr}
                value={country.value}
                onChange={item => {
                  updateState({...iState, country: item, state: "" , city: "", phoneNumber: iState.phoneNumber, email: iState.email, firstName: iState.firstName , lastName: iState.lastName , postalCode: iState.postalCode , billingAddress: iState.billingAddress , designation: iState.designation , organization: iState.organization});
                  // setCountryCode(item.value);
                  // console.log(item.value, "huihui")
                  setStateIsoCode(item.value)
                  stateData(item.value)
                  setStateArr([])
                  setCityArr([])
                }}
                  // source={imageConstants.mail}
                  margin={10}
                  placeholder={country}
                  fontSize={13}
                  // imageWidth={15}
                  // marginLeft={"2%"}
                  width={"100%"}
                padding={5}
                />

              </View>
            </View>

            <View style={[styles.nameContainer, { marginTop: moderateVerticalScale(20) }]}>


              <View style={{
                width: "47%",
              }}>
                <CustomText
                  width={"100%"}
                  Title={"State"}
                  marginTop={1}
                />


                <CustomDropdownSearch
                 data={stateArr}
                 value={state.value}
                 onChange={item => {
                   updateState({...iState,state: item, city: "", phoneNumber: iState.phoneNumber, email: iState.email, firstName: iState.firstName , lastName: iState.lastName, postalCode: iState.postalCode , billingAddress: iState.billingAddress, designation: iState.designation , organization: iState.organization});
                   cityData(stateIsoCode + '/' + item.value)
                   setCityArr([])
                 }}
                  // source={imageConstants.mail}
                  margin={10}
                  placeholder={state}
                  fontSize={13}
                  // imageWidth={15}
                  // marginLeft={"2%"}
                  width={"100%"}
                padding={5}
                />

              </View>

              <View style={{
                width: "47%",
              }}>
                <CustomText
                  width={"100%"}
                  Title={"City"}
                  marginTop={1}
                />

                <CustomDropdownSearch
                data={cityArr}
                value={city.value}
                onChange={item => {
                  updateState({...iState, city: item, phoneNumber: iState.phoneNumber, email: iState.email, firstName: iState.firstName , lastName: iState.lastName, postalCode: iState.postalCode , billingAddress: iState.billingAddress, designation: iState.designation , organization: iState.organization});
                  setCityCode(item.value)
                }}
                  margin={10}
                  padding={5}
                  placeholder={city}
                  fontSize={13}
                  width={"100%"}
                />

              </View>
            </View>

            <CustomText
              Title={"Designation (optional)"}
              marginTop={20}
              width={"100%"}
            />

            <CustomInput
            value={designation}
            onChangeText={(text) => updateState({ ...iState, designation: text })}
              textWidth={"90%"}
              margin={10}
              fontSize={13}
              placeholder={"Enter Designation "}
              // imageWidth={15}
              // source={imageConstants.mail}
              padding={5}
              width={"100%"}
            />

            <CustomText
              Title={"Organization (optional)"}
              marginTop={20}
              width={"100%"}
            />

            <CustomInput
            value={organization}
            onChangeText={(text) => updateState({ ...iState, organization: text })}
              textWidth={"90%"}
              margin={10}
              fontSize={13}
              placeholder={"Enter Organization "}
              // imageWidth={15}
              // source={imageConstants.mail}
              padding={5}
              width={"100%"}
            />
          </ScrollView>
          <View style={{
            width: Width,
            backgroundColor: colorConstant.backgroundColor,
            position: "absolute",
            bottom: 0,
            alignSelf:"center"
            // paddingVertical: moderateVerticalScale(20)
          }}>
            <CustomButton
              // position={"absolute"}
              bottom={10}
              OnButtonPress={() => props.navigation.navigate("PaymentDetail", {
                data , firstName, lastName, email, phoneNumber, countryCode , billingAddress, postalCode, country, state, city, designation, organization
              })} 
              buttonText={"Continue"}
            />
          </View>
        

      </View>
    </AppSafeAreaView>
  )
}

export default BillingDetails

const styles = StyleSheet.create({
  main: {
    flex: 1,
    // backgroundColor: "#EDF7FF"
  },
//   container: {
//     flex: 1,
//     backgroundColor: colorConstant.backgroundColor,
//     width: Width,
//     marginTop: moderateVerticalScale(60),
//     alignSelf: "center",
//   },
  TextContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "90%",
    alignSelf: "center",
    marginTop: moderateVerticalScale(15)
  },

  headTitle: {
    fontFamily: fontConstant.medium,
    fontSize: 14,
    color: colorConstant.black
  },
  subTitle: {
    fontFamily: fontConstant.bold,
    fontSize: 14,
    color: colorConstant.black
  },
  DocContainer: {
    backgroundColor: colorConstant.white,
    width: "90%",
    padding: moderateVerticalScale(10),
    marginTop: moderateVerticalScale(20),
    alignSelf: "center",
    borderRadius: 20,
    borderColor: colorConstant.borderColor,
    borderWidth: 0.5,
    paddingVertical: moderateVerticalScale(8),
    paddingHorizontal: moderateScale(12),
    flexDirection: "row",
    alignItems: "center"
  },
  DocText: {
    fontFamily: fontConstant.bold,
    fontSize: 14,
    lineHeight: 25,
    color: colorConstant.black
  },
  CardText: {
    fontFamily: fontConstant.medium,
    fontSize: 14,
    lineHeight: 28,
    color: colorConstant.black
  },

  UploadText: {
    marginTop: "15%",
    textAlign: "center",
    fontFamily: fontConstant.semiBold,
    color: colorConstant.blue,
    fontSize: 16
  },

  billText: {
    paddingHorizontal: moderateVerticalScale(20),
    marginTop: moderateVerticalScale(20),
    fontFamily: fontConstant.bold,
    color: colorConstant.black,
    fontSize: 15
  },

  addrText: {
    paddingHorizontal: moderateVerticalScale(20),
    marginTop: moderateVerticalScale(10),
    fontFamily: fontConstant.regular,
    color: colorConstant.black,
    lineHeight: 22,
    fontSize: 15
  },

  nameContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: moderateVerticalScale(30),
    width: "100%",
    alignSelf: "center",
    justifyContent: "space-between"
  },

  codeView: {
    backgroundColor: colorConstant.white,
    padding: moderateScale(14),
    width: Width * 0.15,
    borderRadius: 5,
  },
  codeText: {

  },

  numberView: {
    justifyContent: "space-between",
    // backgroundColor:"pink",
    // marginLeft:"2%",
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    // paddingHorizontal: moderateScale(20),
    marginTop: moderateVerticalScale(14)
  },
})