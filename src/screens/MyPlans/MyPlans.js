import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants';
import { Width } from '../../dimensions/dimensions';
import CustomButton from '../../Custom/CustomButton';
import { store } from '../../utils/store';
import { useDispatch, useSelector } from 'react-redux';
import { CurrentPlanApi, setSubscriptionStatus, setTrialStatus } from '../../redux/Slices/APISlices';
import moment from 'moment';
import { useIsFocused } from '@react-navigation/native';
import { moderateVerticalScale } from 'react-native-size-matters';

const MyPlans = (props) => {
  const givenDate = moment(plandata?.plans?.[0]?.subscriptionExpiryDate);
  const currentDate = moment();
  const dispatch = useDispatch()
  const IsFocused = useIsFocused()
  const [plandata, setPlanData] = useState('')
  const [freeData, setFreeData] = useState('')
  const [upcoming, setUpcoming] = useState(false);
  const [planStatus, setPlanStatus] = useState(false)
  const SubscriptionStatus = useSelector((state) => state.counter.apiDataFlow.SubscriptionStatus)
  const PlanType = useSelector((state) => state.counter.apiDataFlow.PlanType)
  console.log(PlanType, "check state==========>>>>>>>>>>>>");


  const PlanFunction = async () => {
    try {
      let response = await store.dispatch(CurrentPlanApi())
      if (response.payload.status === 200) {
        setPlanData(response.payload?.data)
        setFreeData(response.payload?.data.freeTrialInfo)
        let exipryDate = moment(response.payload.data?.freeTrialInfo?.expiredOn ?? "2024-04-9T07:44:32.397Z")
        if (currentDate.isBefore(exipryDate)) {
          setPlanStatus(true)
          dispatch(setTrialStatus({ trialStatus: true }))
          if (response.payload.data?.upcomingPlan?.length > 0) {
            setUpcoming(true)
          }
          else {
            setUpcoming(false)
          }
        } else {
          setPlanStatus(false)
          setUpcoming(false)
          if (response.payload.data?.upcomingPlan?.length > 0) {
            dispatch(setTrialStatus({ trialStatus: true }))
          } else {
            dispatch(setTrialStatus({ trialStatus: false }))
          }

        }
        console.log(response.payload.data, "PLAN DATA ==-=-==-=-==-==-=-");
      }
      else {
        console.log(response.payload.message, "PLAN ELSE DATA =-=-==-=-==-==-=-");
      }
    } catch (error) {
      console.log(error.response.message && error.message, "CATCH PLAN ERROR =-=-==-=-==-==-=-");
    }
  }

  // console.log(plandata?.upcomingPlan?.[0], "PLandataaaaa");

  useEffect(() => {
    if (IsFocused) {
      PlanFunction()
    }
  }, [IsFocused])

  const capitalizeFirstLetter = (str) => {
    return str?.split(' ')?.map(word => word?.charAt(0)?.toUpperCase() + word?.slice(1))?.join(' ');
  }

  const PlanHeader = () => (
    <ScrollView contentContainerStyle={{
      paddingBottom: moderateVerticalScale(20)
    }}>
      <Text style={planHeaderStyle.currentPlanTxt}>My Current Plan</Text>
      <View style={planHeaderStyle.planFullVw}>
        <View>
          <Text style={planHeaderStyle.planNameTxt}>Plan Name</Text>
          <Text style={planHeaderStyle.planYearTxt}>{planStatus ? capitalizeFirstLetter(freeData?.planName) : capitalizeFirstLetter(plandata?.upcomingPlan?.[0]?.plans?.[0]?.name)}</Text>
        </View>
        <View style={planHeaderStyle.rightPayVw}>
          <TouchableOpacity style={planHeaderStyle.recieptBtn} activeOpacity={0.8}
            onPress={() => {
              if (currentDate.isAfter(givenDate)) {
                props.navigation.navigate("SubscriptionPlan")
              }
              else if (planStatus) {
                props.navigation.navigate("SubscriptionPlan")
              }
            }}>
            <Text style={planHeaderStyle.receiptTxt}>{planStatus ? "Renew Plan" : "Pay Amount"}</Text>
            {/* <Text style={planHeaderStyle.receiptTxt}>Renew Plan</Text> */}
          </TouchableOpacity>
          <Text style={planHeaderStyle.nextDueTxt}>{planStatus ? " " : `Next Due on ${moment(plandata?.upcomingPlan?.[0]?.plans?.[0]?.subscriptionExpiryDate).format('DD/MM/YYYY')}`}</Text>
          <Text style={planHeaderStyle.nextDueTxt}></Text>
        </View>
      </View>
      <View style={planHeaderStyle.lineVw} />
      <View style={planHeaderStyle.PlanBottomVw}>
        <View style={planHeaderStyle.leftVw}>
          <Text style={planHeaderStyle.lightPlanTxt}>Activated On</Text>
          <Text style={planHeaderStyle.darkPlanTxt}>{planStatus ? moment(freeData?.activatedOn).format('DD/MM/YYYY') : moment(plandata?.upcomingPlan?.[0]?.plans?.[0]?.subscriptionActivateDate).format('DD/MM/YYYY')}</Text>
          <Text style={planHeaderStyle.lightPlanTxt}>Payment Type</Text>
          <Text style={planHeaderStyle.darkPlanTxt}>{SubscriptionStatus ? "Monthly" : "--"}</Text>
        </View>
        <View style={planHeaderStyle.rightVw}>
          <Text style={planHeaderStyle.lightPlanTxt}>Expiry Date</Text>
          <Text style={planHeaderStyle.darkPlanTxt}>{planStatus ? moment(freeData?.expiredOn).format('DD/MM/YYYY') : moment(plandata?.upcomingPlan?.[0]?.plans?.[0]?.subscriptionExpiryDate).format('DD/MM/YYYY')}</Text>
          <Text style={planHeaderStyle.lightPlanTxt}>Price</Text>
          <Text style={planHeaderStyle.darkPlanTxt}>{planStatus ? "₹00" : plandata?.upcomingPlan?.[0]?.plans?.[0]?.actualPrice}</Text>
        </View>
      </View>
      <View style={planHeaderStyle.lineVw} />

      {
        upcoming && (
          <>
            <Text style={planHeaderStyle.currentPlanTxt}>My Upcoming Plan</Text>
            <View style={planHeaderStyle.planFullVw}>
              <View>
                <Text style={planHeaderStyle.planNameTxt}>Plan Name</Text>
                <Text style={planHeaderStyle.planYearTxt}>{capitalizeFirstLetter(plandata?.upcomingPlan?.[0]?.plans?.[0]?.name) ?? ""}</Text>
              </View>
            </View>
            <View style={planHeaderStyle.lineVw} />
            <View style={planHeaderStyle.PlanBottomVw}>
              <View style={planHeaderStyle.leftVw}>
                <Text style={planHeaderStyle.lightPlanTxt}>Activated On</Text>
                <Text style={planHeaderStyle.darkPlanTxt}>{moment(plandata?.upcomingPlan?.[0]?.plans?.[0]?.subscriptionActivateDate).format('DD/MM/YYYY') ?? ""}</Text>
              </View>
              <View style={planHeaderStyle.rightVw}>
                <Text style={planHeaderStyle.lightPlanTxt}>Expiry Date</Text>
                <Text style={planHeaderStyle.darkPlanTxt}>{moment(plandata?.upcomingPlan?.[0]?.plans?.[0]?.subscriptionExpiryDate).format('DD/MM/YYYY') ?? ""}</Text>
              </View>
            </View>
            <View style={planHeaderStyle.lineVw} />
          </>
        )
      }


    </ScrollView>
  );

  const PlanCell = ({ item, index }) => {
    console.log(item, "iteemmmmmmmm");
    return (
      // console.log(item?.plans?.[0]?.termsConditions?.[0], index , "bchfdcjfdnjkfdnkjdnfknj")
      <View style={PlanCellStyle.cellVw}>
        <Image style={PlanCellStyle.tickImage} source={imageConstants.tickIcon} />
        <Text style={PlanCellStyle.cellTxt}>{item?.plans?.[0]?.termsConditions?.[0]}</Text>
      </View>
    )
  };

  return (
    <View style={{ backgroundColor: '#D7F1FF', flex: 1 }}>
      <View style={{ backgroundColor: '#D7F1FF', flex: 1 }}>
        <View style={styles.appSafeAreaVw}>
          <Image style={styles.LogoIcon} source={imageConstants.Logo} />
          <TouchableOpacity onPress={() => props.navigation.navigate("Notification")}>
            <Image source={imageConstants.bellIcon} resizeMode='contain' style={{ width: 20, height: 20 }} />
          </TouchableOpacity>
        </View>
        <FlatList
          data={plandata}
          renderItem={PlanCell}
          keyExtractor={item => item.id}
          // ListEmptyComponent={() =>
          //   <View style={{
          //     alignItems: "center"
          //   }}>
          //     <Text>You Have No Current Plan </Text>
          //   </View>
          // }
          ListHeaderComponent={PlanHeader}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: 180
          }}
        />
      </View>
      {
        PlanType.planType !== "freeTrial" && (
          <CustomButton
            // position={'absolute'}
            OnButtonPress={() => props.navigation.navigate("PaymentHistoryList")}
            buttonText={'Subscription Payment History '}
            bottom={10}
          />
        )
      }

    </View>
  );
};

const styles = StyleSheet.create({
  appSafeAreaVw: {
    paddingTop: "15%",
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 16,
  },
  LogoIcon: {
    height: 30,
    width: Width * 0.245,
    alignItems: 'flex-start',
    resizeMode: 'contain',
  },
});

const PlanCellStyle = StyleSheet.create({
  cellVw: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignSelf: 'flex-start',
    paddingLeft: 16,
    paddingTop: 16,
  },

  tickImage: {
    height: 20,
    width: 20,
  },

  cellTxt: {
    paddingLeft: 10,
    fontFamily: fontConstant.regular,
    fontSize: 12,
    color: 'black',
  },
});

const planHeaderStyle = StyleSheet.create({
  currentPlanTxt: {
    fontWeight: '700',
    fontSize: 16,
    color: 'black',
    paddingLeft: 17,
    paddingTop: 17,
  },

  planFullVw: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 17,
  },

  planNameTxt: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.black,
  },

  planYearTxt: {
    fontWeight: '700',
    fontSize: 24,
    color: '#14B082',
    paddingTop: 10,
  },

  nextDueTxt: {
    fontFamily: fontConstant.regular,
    fontSize: 12,
    color: 'red',
    paddingTop: 10,
  },

  rightPayVw: {
    justifyContent: 'flex-end',
    alignSelf: 'center',
    alignItems: 'center',
  },

  recieptBtn: {
    backgroundColor: colorConstant.blue,
    justifyContent: 'center',
    alignItems: 'center',
    padding: "2.5%",
    width: Width * 0.40,
    // height: 37,
    borderRadius: 4,
  },

  receiptTxt: {
    fontWeight: '600',
    fontSize: 16,
    color: 'white',
  },

  lineVw: {
    height: 0.8,
    width: '100%',
    backgroundColor: 'gray',
  },

  PlanBottomVw: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 17,
    paddingBottom: 0,
  },

  leftVw: {
    justifyContent: 'flex-start',
    alignSelf: 'flex-start',
    alignItems: 'flex-start',
  },

  rightVw: {
    justifyContent: 'flex-end',
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
  },

  lightPlanTxt: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.black,
    paddingBottom: 10,
  },

  darkPlanTxt: {
    fontFamily: fontConstant.semiBold,
    fontSize: 16,
    color: 'black',
    paddingBottom: 20,
  },
});

export default MyPlans;
