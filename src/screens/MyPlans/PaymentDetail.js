import React, { useState } from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import {
  colorConstant,
  fontConstant,
  imageConstants,
} from '../../utils/constants';
import { moderateVerticalScale } from 'react-native-size-matters';
import { Width } from '../../dimensions/dimensions';
import { store } from '../../utils/store';
import { purchasePlanApi, setPlanType } from '../../redux/Slices/APISlices';
import toastShow from '../../utils/ToastMessage';
import { useDispatch } from 'react-redux';

const PaymentDetail = (props) => {
  const { data, firstName, lastName, email, phoneNumber, countryCode, billingAddress, postalCode, country, state, city, designation, organization } = props.route.params;
  // console.log(data?._id);
  const dispatch = useDispatch();
  const [fill, setFill] = useState(false);
  const [active, setActive] = useState(0)
  const [finalData, setFinalData] = useState('')
  const [payOption, setPayOption] = useState('monthly')
  const paymentArr = [
    { id: 0, title: 'Monthly Payment', price: data?.discountPrice / 12, payOption: "monthly" },
    { id: 1, title: 'Quarterly Payment', price: data?.discountPrice / 4, payOption: "quaterly" },
    { id: 2, title: 'Half Yearly Payment', price: data?.discountPrice / 3, payOption: "halfyearly" },
    { id: 3, title: 'Full Payment', price: data?.discountPrice, payOption: "fullpayment" },
  ];

  console.log(payOption, "ACTIVEEEEE");

  const PurchasePlanFunction = async () => {
    let reqData = {
      personalDetails: {
        firstName: firstName ?? "",
        lastName: lastName ?? "",
        email: email ?? "",
        contact: countryCode + phoneNumber ?? ""
      },
      billingDetails: {
        address: billingAddress ?? "",
        code: postalCode ?? "",
        country: country ?? "",
        city: city ?? "",
        state: state ?? "",
        designation: designation ?? "",
        organization: organization ?? ""
      },
      paymentDetails: {
        paymentOptions: payOption
      },
      checkOut: {
        paymentType: ""
      },
      paymentStatus: "confirmed",
      planId: data?._id ?? "",

    }
    console.log(reqData, "reqData");
    try {
      let response = await store.dispatch(purchasePlanApi(reqData))
      if (response.payload.status === 200) {
        toastShow("Success", colorConstant.green)
        setFinalData(response.payload?.data)
        dispatch(setPlanType({ PlanType: response.payload.data }))
        props.navigation.navigate("SubcribeCongrat", {
          data, finalData: response.payload?.data?.subscriptionExpiryDate
        })
        console.log(response.payload.data, "responseee------>>>>>>>");
      }
      else {
        console.log(response.payload.message, "else reponse -------->>>>>");
      }
    } catch (error) {
      console.log(error.response.message && error.message, "Catch error ------->>>>>");
    }
  }

  // console.log(finalData.subscriptionExpiryDate, data ,  "data------------------>>>>>");

  const paymentCell = ({ item, index }) => (
    <View style={[paymentCellStyle.cellVw, {
      borderWidth: active === index ? 0.8 : 0
    }]}>
      <TouchableOpacity onPress={() => {
        setActive(index)
        setPayOption(item.payOption)
      }}>
        <Image
          style={paymentCellStyle.cellImage}
          source={active === index ? imageConstants.circleFill : imageConstants.payCircle}
        />
      </TouchableOpacity>
      <Text style={paymentCellStyle.paymentTxt}>{item.title}</Text>
      <Text style={paymentCellStyle.priceTxt}>₹{item.price.toFixed(0)}</Text>
    </View>
  );

  const paymentHeaderCell = ({ item, index }) => (
    <View>
      <View style={paymentHeaderStyles.planVw}>
        <Text style={paymentHeaderStyles.yearTxt}>{data?.name}</Text>
        <View style={paymentHeaderStyles.priceVw}>
          <Text style={paymentHeaderStyles.priceTxt}>₹{data?.discountPrice}</Text>
          <Text style={paymentHeaderStyles.gstTxt}>Excluding GST</Text>
        </View>
      </View>
      <Text style={paymentHeaderStyles.priceDetail}>Price Details</Text>
      <View style={paymentHeaderStyles.fareVw}>
        <View style={paymentHeaderStyles.leftVw}>
          <Text style={paymentHeaderStyles.grayTxt}>Base Fare</Text>
          <Text style={paymentHeaderStyles.grayTxt}>GST (18%)</Text>
          <Text style={paymentHeaderStyles.totalTxt}>Total</Text>
        </View>
        <View style={paymentHeaderStyles.rightVw}>
          <Text style={paymentHeaderStyles.grayTxt}>₹{(data?.discountPrice * 0.82).toFixed(0)}</Text>
          <Text style={paymentHeaderStyles.grayTxt}>₹{(data?.discountPrice * 0.18).toFixed(0)}</Text>
          <Text style={paymentHeaderStyles.totalTxt}>₹{data?.discountPrice}</Text>
        </View>
      </View>
      <View style={paymentHeaderStyles.lineVw} />
      <Text style={paymentHeaderStyles.paymentOptionTxt}>Payment Options</Text>
    </View>
  );

  const paymentFooterCell = ({ item, index }) => (
    <View>
      <View style={paymentFooterStyle.termVw}>
        <TouchableOpacity onPress={() => setFill(!fill)}>
          <Image style={paymentFooterStyle.termBox} source={fill ? imageConstants.tickfill : imageConstants.remindMe} />
        </TouchableOpacity>
        <Text style={paymentFooterStyle.agreeText}>I have read <Text style={paymentFooterStyle.termText} onPress={() => props.navigation.navigate("TermsCondition")}>Terms & conditions</Text><Text> & </Text><Text style={paymentFooterStyle.termText}>Cancellation Policies</Text></Text>
      </View>
      <TouchableOpacity style={paymentFooterStyle.continueBtn} onPress={() => checkPayment()}>
        <Text style={paymentFooterStyle.conntinueTxt}>Continue</Text>
      </TouchableOpacity>
    </View>
  );

  const checkPayment = async () => {
    if (fill) {
      PurchasePlanFunction()
    } else {
      toastShow("Please check Term & Condition", colorConstant.red)
    }
  }

  return (
    <AppSafeAreaView
      title="Payment Details"
      bgColor="#D7F1FF"
      noPadding="false">
      <FlatList
        data={paymentArr}
        renderItem={paymentCell}
        keyExtractor={item => item.id}
        ListHeaderComponent={paymentHeaderCell}
        ListFooterComponent={paymentFooterCell}
      />
    </AppSafeAreaView>
  );
};

const paymentCellStyle = StyleSheet.create({
  cellVw: {
    flexDirection: 'row',
    height: 55,
    justifyContent: 'flex-start',
    alignSelf: 'center',
    alignItems: 'center',
    width: '90%',
    backgroundColor: colorConstant.white,
    borderRadius: 5,
    padding: 14,
    marginBottom: 7,
    marginBottom: 18,
  },

  cellImage: {
    height: 18,
    width: 18,
    resizeMode: 'contain',
  },

  paymentTxt: {
    fontFamily: fontConstant.medium,
    fontSize: 16,
    color: colorConstant.black,
    paddingLeft: 10,
  },

  priceTxt: {
    position: 'absolute',
    fontFamily: fontConstant.bold,
    fontSize: 14,
    color: colorConstant.black,
    right: 12,
  },
});

const paymentHeaderStyles = StyleSheet.create({
  planVw: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: colorConstant.blue,
    width: '90%',
    height: 88,
    padding: 12,
    margin: 18,
    borderRadius: 8,
  },

  yearTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 18,
    color: colorConstant.white,
  },

  priceVw: {
    alignItems: 'flex-end',
  },

  priceTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 26,
    color: colorConstant.white,
  },

  gstTxt: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.white,
  },

  priceDetail: {
    fontFamily: fontConstant.bold,
    fontSize: 14,
    color: colorConstant.black,
    paddingLeft: 18,
    paddingBottom: 10,
  },

  fareVw: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    width: '90%',
    height: 100,
  },

  totalTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 14,
    color: colorConstant.black,
    paddingBottom: 10,
  },

  leftVw: {
    alignItems: 'flex-start',
  },

  rightVw: {
    alignItems: 'flex-end',
  },

  grayTxt: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.black,
    paddingBottom: 10,
  },

  lineVw: {
    height: 1,
    backgroundColor: 'gray',
  },

  paymentOptionTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 16,
    color: colorConstant.black,
    padding: 18,
  },
});

const paymentFooterStyle = StyleSheet.create({

  termVw: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignSelf: 'center',
    width: Width * 0.90,
    paddingHorizontal: moderateVerticalScale(2)
  },

  agreeText: {
    paddingLeft: 10,
    fontFamily: fontConstant.regular,
    fontSize: 13,
    color: colorConstant.black,
  },

  termText: {
    paddingLeft: 10,
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.blue, textDecorationLine: "underline"
  },

  termBox: {
    height: 18,
    width: 18,
  },

  continueBtn: {
    margin: 18,
    backgroundColor: colorConstant.blue,
    height: 44,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    width: '90%',
    borderRadius: 4,
  },

  conntinueTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 16,
    color: colorConstant.white,
  },
});

export default PaymentDetail;
