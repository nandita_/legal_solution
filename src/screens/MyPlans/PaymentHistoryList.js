import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants';
import { moderateVerticalScale } from 'react-native-size-matters';
import { store } from '../../utils/store';
import { PlanHistoryApi } from '../../redux/Slices/APISlices';
import moment from 'moment';

const PaymentHistoryList = (props) => {
  const [historyData, setHistoryData] = useState([])
  const PaymentHistoryFunction = async () => {
    try {
      let response = await store.dispatch(PlanHistoryApi())
      if (response.payload.status === 200) {
        setHistoryData(response?.payload?.data)
        console.log(response.payload.data, "Response ---------------->>>>>>");
      }
      else {
        console.log(response.payload.message, "Else response ---=-===-=-=-=-=-=->>>");
      }
    } catch (error) {
      console.log(response.error.message && error.message, "Catch response ======>>>>");
    }
  }

  useEffect(() => {
    PaymentHistoryFunction()
  }, [])


  const paymentCell = (item, index) => {
    console.log("{}{}{}{}{}{}{}{}{}{}{}{}{}{", item);
    return (
      <View style={styles.paymentCellVw}>
        <View style={styles.leftVw}>
          <Text style={styles.dateLbl}>Date</Text>
          <Text style={styles.dateTxt}>{moment(item?.subscriptionStartDate).format("DD/MM/YYYY")}</Text>
          <Text style={styles.dateLbl}>Price</Text>
          <Text style={styles.dateTxt}>₹{item?.item?.plans?.actualPrice}</Text>
        </View>
        <View style={styles.rightVw}>
          <Text style={styles.dateLbl}>Reference Number</Text>
          <Text style={styles.dateTxt}>0215-15450-5956</Text>
          <TouchableOpacity style={styles.recieptBtn} onPress={() => props.navigation.navigate("PaymentReciept")}>
            <Text style={styles.receiptTxt}>View Receipt</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  return (
    <AppSafeAreaView title="Payment History" bgColor="#D7F1FF">
      <FlatList
        data={historyData ?? []}
        renderItem={paymentCell}
        ListHeaderComponent={
          <Text style={styles.headerTxt}>Subscription Payment History </Text>
        }
        keyExtractor={item => item.id}
      />
    </AppSafeAreaView>
  );
};

const styles = StyleSheet.create({
  headerTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 16,
    color: 'black',
    paddingBottom: 12,
    paddingTop: "8%",
  },

  paymentCellVw: {
    flexDirection: 'row',
    // height: 130,
    marginTop: moderateVerticalScale(10),
    // marginBottom: 14,
    borderColor: 'lightgrey',
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: 'white',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    width: '100%',
    padding: 10,
  },

  leftVw: {
    // backgroundColor: 'red',
  },

  rightVw: {
    alignItems: 'flex-end',
  },

  dateLbl: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.black,
    paddingTop: 4,
  },

  dateTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 13,
    color: 'black',
    paddingTop: 4,
    paddingBottom: 4,
  },

  recieptBtn: {
    backgroundColor: colorConstant.blue,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 4,
    width: 150,
    height: 37,
    borderRadius: 4,
    marginTop: 10,
  },

  receiptTxt: {
    fontFamily: fontConstant.semiBold,
    fontSize: 14,
    color: 'white',
  },
});

export default PaymentHistoryList;
