import { Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { colorConstant, fontConstant } from '../../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import AppSafeAreaView from '../../Components/AppSafeAreaView'
import CustomButton from '../../Custom/CustomButton'
import { Width } from '../../dimensions/dimensions'
import { PlanHistoryApi } from '../../redux/Slices/APISlices'
import { store } from '../../utils/store'
import moment from 'moment'
import RNPrint from 'react-native-print';

const PaymentReciept = (props) => {
  const [historyData, setHistoryData] = useState([])
  const [selectedPrinter, setSelectedPrinter] = useState(null);

  const selectPrinter = async () => {
    const printer = await RNPrint.selectPrinter({ x: 100, y: 100 });
    setSelectedPrinter(printer);
  };

  const silentPrint = async () => {
    if (!selectedPrinter) {
      alert('Must Select Printer First');
      return;
    }
  }

  const printRemotePDF = async () => {
    await RNPrint.print({ filePath: item?.digestNotes });
  };

  const customOptions = () => {
    return (
      <View>
        {selectedPrinter &&
          <View>
            <Text>{`Selected Printer Name: ${selectedPrinter.name}`}</Text>
            <Text>{`Selected Printer URI: ${selectedPrinter.url}`}</Text>
          </View>
        }
        <Button onPress={selectPrinter} title="Select Printer" />
        <Button onPress={silentPrint} title="Silent Print" />
      </View>
    );
  };


  const PaymentHistoryFunction = async () => {
    try {
      let response = await store.dispatch(PlanHistoryApi())
      if (response.payload.status === 200) {
        setHistoryData(response?.payload?.data?.[0])
        // console.log(response.payload.data, "Response ---------------->>>>>>");
      }
      else {
        console.log(response.payload.message, "Else response ---=-===-=-=-=-=-=->>>");
      }
    } catch (error) {
      console.log(response.error.message && error.message, "Catch response ======>>>>");
    }
  }

  useEffect(() => {
    PaymentHistoryFunction()
  }, [])

  console.log(historyData, "[]][][][][][][][][][][[][][][][");

  return (
    <AppSafeAreaView title="Payment Receipt" bgColor={colorConstant.backgroundColor} noPadding="false">
      <View style={styles.main}>
        <Text style={styles.headerText}>Receipt</Text>
        <View style={styles.paymentCellVw}>
          <View style={styles.leftVw}>
            <Text style={styles.dateLbl}>Plan Name</Text>
            <Text style={[styles.dateTxt, { color: colorConstant.green, fontSize: 16, fontFamily: fontConstant.semiBold }]}>{historyData?.plans?.name}</Text>
            <Text style={[styles.dateLbl, { marginTop: moderateVerticalScale(15) }]}>Payment Due</Text>
            <Text style={styles.dateTxt}>{moment(historyData.subscriptionExpiryDate).format("DD/MM/YYYY")}</Text>

            <Text style={[styles.dateLbl, { marginTop: moderateVerticalScale(15) }]}>Transaction ID</Text>
            <Text style={styles.dateTxt}>15450-595656</Text>

          </View>
          <View style={styles.rightVw}>
            <Text style={styles.dateLbl}>Reference Number</Text>
            <Text style={styles.dateTxt}>0215-15450-5956</Text>
            <Text style={[styles.dateLbl, { marginTop: moderateVerticalScale(15) }]}>Payment Date</Text>
            <Text style={styles.dateTxt}>{moment(historyData.subscriptionStartDate).format("DD/MM/YYYY")}</Text>
          </View>


        </View>

        <Text style={styles.headerText}>Price Details</Text>
        <View style={{
          flexDirection: "row",
          justifyContent: "space-between",
          width: Width * 0.90,
          alignSelf: "center",
          marginTop: moderateVerticalScale(10)
        }}>
          <View>
            <Text style={styles.leftText}>Sub Total</Text>
            <Text style={styles.leftText}>GST (18%)</Text>
            <Text style={[styles.leftText, { fontFamily: fontConstant.bold, marginTop: moderateVerticalScale(10) }]}>Total</Text>
          </View>
          <View style={{
            alignItems: "flex-end"
          }}>
            <Text style={styles.rightText}>₹82</Text>
            <Text style={styles.rightText}>₹18</Text>
            <Text style={[styles.rightText, { fontFamily: fontConstant.bold, marginTop: moderateVerticalScale(10) }]}>₹{historyData?.plans?.actualPrice}</Text>
          </View>
        </View>
        {Platform.OS === 'ios' && customOptions()}
        <CustomButton
          OnButtonPress={() => printRemotePDF()}
          position={"absolute"}
          bottom={20}
          buttonText={"Print"}
        />

      </View>
    </AppSafeAreaView>
  )
}

export default PaymentReciept

const styles = StyleSheet.create({
  main: {
    flex: 1
  },
  headerText: {
    fontFamily: fontConstant.bold,
    color: colorConstant.black,
    paddingHorizontal: moderateScale(20),
    marginTop: moderateVerticalScale(20)
  },
  paymentCellVw: {
    flexDirection: 'row',
    // height: 130,
    marginTop: moderateVerticalScale(15),
    // marginBottom: 14,
    borderColor: 'lightgrey',
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: 'white',
    justifyContent: 'space-between',
    // alignItems: 'center',
    alignSelf: 'center',
    width: '90%',
    padding: 10,
  },

  leftVw: {
    // backgroundColor: 'red',
  },

  rightVw: {
    alignItems: 'flex-end',
  },

  dateLbl: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.black,
    paddingTop: 4,
  },

  dateTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 14,
    color: 'black',
    paddingTop: 4,
    paddingBottom: 4,
  },

  recieptBtn: {
    backgroundColor: colorConstant.blue,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 4,
    width: 150,
    height: 37,
    borderRadius: 4,
    marginTop: 10,
  },

  receiptTxt: {
    fontFamily: fontConstant.semiBold,
    fontSize: 14,
    color: colorConstant.black,
  },

  leftText: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.black,
    lineHeight: 28
  },

  rightText: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.black,
    lineHeight: 28
  }
})