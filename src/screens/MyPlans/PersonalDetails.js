import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import {
  colorConstant,
  fontConstant,
  imageConstants,
} from '../../utils/constants';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import { Width } from '../../dimensions/dimensions';
import CustomInput from '../../Custom/CustomInput';
import CustomText from '../../Custom/CustomText';
import CustomButton from '../../Custom/CustomButton';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import { useSelector } from 'react-redux';

const initialState = {
  firstName: "",
  lastName: "",
  email: "",
  phoneNumber: "",
  countryCode: "+91",
}

const PersonalDetail = (props) => {
  const { data } = props.route.params;
  const userProfile = useSelector((state) => state.counter.apiDataFlow.userProfile)
  const [iState, updateState] = useState(initialState);
  const {
    firstName,
    lastName,
    email,
    countryCode,
    phoneNumber,
  } = iState

  const mappingState = () => {
    updateState({
      ...iState,
      firstName: userProfile?.firstName ?? "",
      lastName: userProfile?.lastName ?? "",
      email: userProfile?.email ?? "",
      countryCode: userProfile?.countryCode ?? "",
      phoneNumber: userProfile?.phoneNumber ?? "",

    })
  }

  useEffect(() => {
    // updateState({...iState , ...userProfile});
    mappingState();
  }, []);
  // console.log(userProfile.phoneNumber, "DATAAAAAAA");
  const paymentHeaderCell = ({ item, index }) => (
    <>
      <View style={paymentHeaderStyles.planVw}>
        <Text style={paymentHeaderStyles.yearTxt}>{data?.name}</Text>
        <View style={paymentHeaderStyles.priceVw}>
          <Text style={paymentHeaderStyles.priceTxt}>₹{data?.discountPrice}</Text>
          <Text style={paymentHeaderStyles.priceTxt1}>₹{data?.actualPrice}</Text>
          <Text style={paymentHeaderStyles.gstTxt}>Excluding GST</Text>
        </View>
      </View>

      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        extraHeight={moderateScale(100)}
        contentContainerStyle={{
          //   backgroundColor:"pink"
        }}>
        <View style={paymentHeaderStyles.nameContainer}>


          <View style={{
            width: "47%",
          }}>
            <CustomText
              Title={"First Name"}
              marginTop={1}
              width={"100%"}
            />

            <CustomInput
              value={firstName}
              onChangeText={(text) => updateState({ ...iState, firstName: text })}
              textWidth={"100%"}
              inputImage={true}
              margin={10}
              fontSize={13}
              placeholder={"First Name"}
              imageWidth={15}
              source={imageConstants.userIcon}
              // padding={5}
              width={"100%"}
            />

          </View>

          <View style={{
            width: "47%",
          }}>
            <CustomText
              Title={"Last Name"}
              marginTop={1}
              width={"100%"}
            />

            <CustomInput
              value={lastName}
              onChangeText={(text) => updateState({ ...iState, lastName: text })}
              textWidth={"100%"}
              inputImage={true}
              margin={10}
              fontSize={13}
              placeholder={"Last Name"}
              imageWidth={15}
              source={imageConstants.userIcon}
              // padding={1}
              width={"100%"}
            />

          </View>
        </View>

        <CustomText
          Title={"Email ID"}
          marginTop={20}
          width={"90%"}
        />

        <CustomInput
          value={email}
          onChangeText={(text) => updateState({ ...iState, email: text })}
          inputImage={true}
          textWidth={"100%"}
          margin={10}
          fontSize={13}
          placeholder={"Eg- test@yourmail.com"}
          imageWidth={15}
          source={imageConstants.mail}
          // padding={1}
          width={"90%"}
        />

        <CustomText
          Title={"Mobile Number"}
          marginTop={20}
        />

        <View style={paymentHeaderStyles.numberView}>
          <View style={paymentHeaderStyles.codeView}>
            <Text style={paymentHeaderStyles.codeText}>{countryCode ?? "+91"}</Text>
          </View>
          <CustomInput
            value={phoneNumber + ""}
            // value={"23344"}
            onChangeText={(text) => updateState({ ...iState, phoneNumber: text })}
            inputImage={true}
            textWidth={"90%"}
            source={imageConstants.phone}
            placeholder={"Enter Mobile Number"}
            fontSize={13}
            // marginLeft={"2%"}
            width={"81%"}
            margin={1}
          />
        </View>
      </KeyboardAwareScrollView>

    </>
  );



  return (
    <AppSafeAreaView
      title="Personal Details"
      bgColor="#D7F1FF"
      noPadding="false">

      <FlatList
        data={[]}
        // renderItem={paymentCell}
        keyExtractor={item => item.id}
        ListHeaderComponent={paymentHeaderCell}
      // ListFooterComponent={paymentFooterCell}
      />


      <View style={{
        width: "100%",
        backgroundColor: colorConstant.backgroundColor,
        position: "absolute",
        bottom: 10,
        // paddingVertical: moderateVerticalScale(20)
      }}>
        <CustomButton
          // position={"absolute"}
          bottom={10}
          OnButtonPress={() => props.navigation.navigate("BillingDetails", {
            data, firstName, lastName, email, phoneNumber, countryCode
          })}
          buttonText={"Continue"}
        />
      </View>
    </AppSafeAreaView>
  );
};


const paymentHeaderStyles = StyleSheet.create({

  codeView: {
    backgroundColor: colorConstant.white,
    width: Width * 0.15,
    borderRadius: 5,
    padding: "4.3%",
    // alignItems:"center",
    //  height:moderateVerticalScale(46),

  },
  codeText: {
    fontFamily: fontConstant.regular,
    fontSize: 13,

    //  backgroundColor:"pink",
    textAlign: "center",
    color: colorConstant.secondaryText
  },
  numberView: {
    justifyContent: "space-between",
    // backgroundColor:"pink",
    // marginLeft:"2%",
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: moderateScale(20),
    marginTop: moderateVerticalScale(14)
  },
  planVw: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: colorConstant.themecolor,
    width: '90%',
    height: 88,
    padding: 12,
    margin: 18,
    borderRadius: 8,
  },

  yearTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 18,
    color: colorConstant.black,
  },

  priceVw: {
    alignItems: 'flex-end',
  },

  priceTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 24,
    color: colorConstant.black,
  },
  priceTxt1: {
    fontFamily: fontConstant.bold,
    fontSize: 16,
    textDecorationLine: "line-through",
    color: colorConstant.black,
  },

  gstTxt: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.black,
  },

  priceDetail: {
    fontFamily: fontConstant.bold,
    fontSize: 14,
    color: colorConstant.black,
    paddingLeft: 18,
    paddingBottom: 10,
  },

  fareVw: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    width: '90%',
    height: 100,
  },

  totalTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 14,
    color: colorConstant.black,
    paddingBottom: 10,
  },

  leftVw: {
    alignItems: 'flex-start',
  },

  rightVw: {
    alignItems: 'flex-end',
  },

  grayTxt: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.black,
    paddingBottom: 10,
  },

  lineVw: {
    height: 1,
    backgroundColor: 'gray',
  },

  paymentOptionTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 16,
    color: colorConstant.black,
    padding: 18,
  },

  nameContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: moderateVerticalScale(10),
    width: Width * 0.90,
    alignSelf: "center",
    justifyContent: "space-between"
  },
});


export default PersonalDetail;
