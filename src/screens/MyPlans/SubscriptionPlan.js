import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants';
import { Width } from '../../dimensions/dimensions';
import CustomButton from '../../Custom/CustomButton';
import { store } from '../../utils/store';
import { SubscriptionPlanApi } from '../../redux/Slices/APISlices';
import { useIsFocused } from '@react-navigation/native';

const SubscriptionPlan = props => {
  const isFocused = useIsFocused()
  const [subData, setSubData] = useState([])
  const [planStatus, setUpPlanStatus] = useState('OneYearPlan')
  const [subIndex, setSubIndex] = useState(0)
  console.log(subData[subIndex]?.termsConditions, "CHECK INDEX");


  useEffect(() => {
    if (isFocused) {
      setUpPlanStatus('OneYearPlan')
      setSubIndex(0)
    }
  }, [isFocused])

  const SubscriptionPlanFuction = async () => {
    try {
      let response = await store.dispatch(SubscriptionPlanApi())
      if (response.payload.status === 200) {
        // console.log(response.payload.data);
        setSubData(response.payload.data)
      }
      else {
        console.log(response.payload.message, "ELse response =======>>>>");
      }
    } catch (error) {
      console.log(error.response.message && error.message, "Catch error ========>>>>>");
    }
  }
  // console.log(subData, "SUBDATATTATA====>>>>");

  useEffect(() => {
    SubscriptionPlanFuction()
  }, [])

  const PlanHeader = (props) => (
    <View style={planHeaderStyle.bottomVw}>
      <TouchableOpacity activeOpacity={0.8} style={planStatus == 'OneYearPlan' ? planHeaderStyle.firstVw : planHeaderStyle.firstSelectVw}
        onPress={() => {
          setSubIndex(0)
          setUpPlanStatus('OneYearPlan');
        }}>
        <Text style={planStatus == 'OneYearPlan' ? planHeaderStyle.titleSelectTxt : planHeaderStyle.titleVw}>{subData[0]?.name}</Text>
        <Text style={planStatus == 'OneYearPlan' ? planHeaderStyle.priceSelectTxt : planHeaderStyle.priceTxt}>₹{subData[0]?.discountPrice}</Text>
        <Text style={planStatus == 'OneYearPlan' ? planHeaderStyle.priceSelectTxt1 : planHeaderStyle.priceTxt1}>₹{subData[0]?.actualPrice}</Text>
        <Text style={planStatus == 'OneYearPlan' ? planHeaderStyle.gstSelectTxt : planHeaderStyle.gstTxt}>Excluding GST</Text>
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={0.8} style={planStatus == 'TwoYearPlan' ? planHeaderStyle.secondSelectVw : planHeaderStyle.secondVw}
        onPress={() => {
          setSubIndex(1)
          setUpPlanStatus('TwoYearPlan');
        }}>
        <Text style={planStatus == 'TwoYearPlan' ? planHeaderStyle.titleSelectTxt : planHeaderStyle.titleVw}>{subData[1]?.name}</Text>
        <Text style={planStatus == 'TwoYearPlan' ? planHeaderStyle.priceSelectTxt : planHeaderStyle.priceTxt}>₹{subData[1]?.discountPrice}</Text>
        <Text style={planStatus == 'TwoYearPlan' ? planHeaderStyle.priceSelectTxt1 : planHeaderStyle.priceTxt1}>₹{subData[1]?.actualPrice}</Text>
        <Text style={planStatus == 'TwoYearPlan' ? planHeaderStyle.gstSelectTxt : planHeaderStyle.gstTxt}>Excluding GST</Text>
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={0.8} style={planStatus == 'ThreeYearPlan' ? planHeaderStyle.thirdSelectVw : planHeaderStyle.thirdVw}
        onPress={() => {
          setSubIndex(2)
          setUpPlanStatus('ThreeYearPlan');
        }}>
        <Text style={planStatus == 'ThreeYearPlan' ? planHeaderStyle.titleSelectTxt : planHeaderStyle.titleVw}>{subData[2]?.name}</Text>
        <Text style={planStatus == 'ThreeYearPlan' ? planHeaderStyle.priceSelectTxt : planHeaderStyle.priceTxt}>₹{subData[2]?.discountPrice}</Text>
        <Text style={planStatus == 'ThreeYearPlan' ? planHeaderStyle.priceSelectTxt1 : planHeaderStyle.priceTxt1}>₹{subData[2]?.actualPrice}</Text>
        <Text style={planStatus == 'ThreeYearPlan' ? planHeaderStyle.gstSelectTxt : planHeaderStyle.gstTxt}>Excluding GST</Text>
      </TouchableOpacity>
    </View>
  );

  const PlanCell = ({ item, index }) => (
    <></>
    // <View key={index} style={PlanCellStyle.cellVw}>
    //   <Image style={PlanCellStyle.tickImage} source={imageConstants.tickIcon} />
    //   <Text style={PlanCellStyle.cellTxt}>{item}</Text>
    // </View>
  );

  return (
    <AppSafeAreaView title="Subscription Plan" bgColor="#D7F1FF">
      <View>
        <Text style={styles.titleTxt}>
          Choose The Right Price Plan For Your Needs
        </Text>
        <FlatList
          // data={subData[subIndex]?.termsConditions ?? []}
          renderItem={PlanCell}
          keyExtractor={item => item.id}
          ListHeaderComponent={PlanHeader}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: 10,
          }}
        />

        {
          subData[subIndex]?.termsConditions?.split("\n")?.map((item, index) => {
            return (
              <View style={PlanCellStyle.cellVw}>
                <Image style={PlanCellStyle.tickImage} source={imageConstants.tickIcon} />
                <Text style={PlanCellStyle.cellTxt}>{item}</Text>
              </View>
            )
          })
        }




      </View>


      <CustomButton
        position={'absolute'}
        OnButtonPress={() => {
          props.navigation.navigate('PersonalDetail', { data: subData[subIndex] })
        }}
        buttonText={'Buy Subscription'}
        bottom={30}
      />
    </AppSafeAreaView>
  );
};

const styles = StyleSheet.create({
  titleTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 26,
    color: 'black',
    paddingBottom: 20,
  },
});

const planHeaderStyle = StyleSheet.create({
  bottomVw: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    alignItems: 'center',
    // backgroundColor: colorConstant.white,
    // padding:"8%",
    width: '100%',
    borderRadius: 8,
  },

  firstVw: {
    backgroundColor: '#077CA1',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    width: '33.5%',
    padding: "2%",
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8
  },

  firstSelectVw: {
    backgroundColor: '#C2EAFF',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    width: '33.5%',
    padding: "2%",
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8
  },

  secondVw: {
    backgroundColor: '#C2EAFF',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    width: '33.5%',
    padding: "2%",
    borderLeftWidth: 1,
    borderLeftColor: colorConstant.white,
    borderRightWidth: 1,
    borderRightColor: colorConstant.white,
  },

  secondSelectVw: {
    backgroundColor: '#077CA1',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    width: '33.5%',
    padding: "2%",
    borderLeftWidth: 1,
    borderLeftColor: colorConstant.white,
    borderRightWidth: 1,
    borderRightColor: colorConstant.white,
  },

  thirdVw: {
    backgroundColor: '#C2EAFF',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    width: '33.5%',
    padding: "2%",
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8
  },

  thirdSelectVw: {
    backgroundColor: '#077CA1',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    width: '33.5%',
    padding: "2%",
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8
  },

  titleTxt: {
    fontFamily: fontConstant.regular,
    fontSize: 12,
    color: colorConstant.black,
  },

  titleSelectTxt: {
    fontFamily: fontConstant.regular,
    fontSize: 12,
    color: colorConstant.white,
  },

  priceTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 20,
    color: colorConstant.black,
  },

  priceSelectTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 20,
    color: colorConstant.white,
  },

  priceTxt1: {
    fontFamily: fontConstant.bold,
    fontSize: 15,
    textDecorationLine: "line-through",
    color: colorConstant.black,
  },

  priceSelectTxt1: {
    textDecorationLine: "line-through",
    fontFamily: fontConstant.bold,
    fontSize: 15,
    color: colorConstant.white,
  },

  gstTxt: {
    fontFamily: fontConstant.regular,
    fontSize: 10,
    color: colorConstant.black,
  },

  gstSelectTxt: {
    fontFamily: fontConstant.regular,
    fontSize: 10,
    color: colorConstant.white,
  },

});

const PlanCellStyle = StyleSheet.create({
  cellVw: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignSelf: 'flex-start',
    // backgroundColor: "pink",
    paddingTop: 16,
  },

  tickImage: {
    height: 20,
    width: 20,
  },

  cellTxt: {
    paddingLeft: 10,
    fontWeight: '400',
    fontSize: 12,
    color: 'black',
    lineHeight: 20,
  },
});

export default SubscriptionPlan;
