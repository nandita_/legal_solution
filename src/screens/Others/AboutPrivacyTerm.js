import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { fontConstant, imageConstants } from '../../utils/constants';
import { store } from '../../utils/store';
import { StaticContentApi } from '../../redux/Slices/APISlices';
import RenderHTML from 'react-native-render-html';
import { Width } from '../../dimensions/dimensions';

const AboutPrivacyTerm = (props) => {
  const [data, setData] = useState("");
  const { screenName } = props.route.params ?? "";
  console.log(screenName, "screenName");


  const StaticContentFunction = async () => {
    try {
      let response = await store.dispatch(StaticContentApi(screenName));
      if (response.payload.status === 200) {
        console.log("Static success", response);
        setData(response.payload.data)
      }
      else {
        console.log("Static else", response);
      }
    } catch (error) {
      console.log("Static success", error.response);
    }
  }

  useEffect(() => {
    StaticContentFunction()
  }, [])

  const tagsStyles = {
    body: {
      fontSize: 14,
      lineHeight: 20,
    }
  };

  return (
    <AppSafeAreaView
      title={data?.title}
      bgColor="#D7F1FF">
      <ScrollView showsVerticalScrollIndicator={false}>
  
        <RenderHTML
          tagsStyles={tagsStyles}
          contentWidth={Width * 0.80}
          source={{ html: data?.description }}
        />
      </ScrollView>
    </AppSafeAreaView>
  );
};

export default AboutPrivacyTerm;
