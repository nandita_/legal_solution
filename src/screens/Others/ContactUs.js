import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Linking,
  Platform,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import {colorConstant, imageConstants} from '../../utils/constants';
import { useSelector } from 'react-redux';
import { StaticContentApi } from '../../redux/Slices/APISlices';
import toastShow from '../../utils/ToastMessage';
import { store } from '../../utils/store';

const ContactUs = (props) => {
  const [data, setData] = useState("");
  const { screenName } = props.route.params ?? "";
  const userProfile = useSelector((state) => state.counter.apiDataFlow.userProfile)
  console.log(userProfile, "profile number===========>>>>>>>>");

  const dialCall = () => {
    let phoneNumber = data.contact ?? ""
    if(phoneNumber !== "") {
      if (Platform.OS !== 'android') {
        Linking.openURL(`telprompt:${phoneNumber}`)
    }
    else {
        Linking.openURL(`tel:${phoneNumber}`)

    }
    }
    else{
      toastShow('Sorry there is no contact number', colorConstant.red)
    }
    
}

const StaticContentFunction = async () => {
  try {
    let response = await store.dispatch(StaticContentApi(screenName));
    if (response.payload.status === 200) {
      console.log("Static success", response);
      setData(response.payload.data)
    }
    else {
      console.log("Static else", response);
    }
  } catch (error) {
    console.log("Static success", error.response);
  }
}

useEffect(() => {
  StaticContentFunction()
}, [])

console.log(data.contact, "{}{}{}{}{}{}{}{}{}{}{}{}{}");

const openMail = () => {
  let emailAddress = data?.email ?? ""
  if(emailAddress !== "") {
    Linking.openURL(`mailto:${emailAddress}`)
  }
  else{
    toastShow('Sorry there is no email', colorConstant.red)
  }
}


  return (
    <AppSafeAreaView title="Contact Us" bgColor="#D7F1FF" noPadding="false">
      <TouchableOpacity style={styles.callVw} onPress={() => dialCall()}>
        <Image source={imageConstants.callBigIcon} />
        <Text style={styles.callNo}>{userProfile?.countryCode}{data?.contact}</Text>
        <Text style={styles.callTxt}>Calls Us</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.emailVw} onPress={() => openMail()}>
        <Image source={imageConstants.emailBigIcon} />
        <Text style={styles.emailNo}>{data?.email}</Text>
        <Text style={styles.emailTxt}>Email Us</Text>
      </TouchableOpacity>
    </AppSafeAreaView>
  );
};

const styles = StyleSheet.create({
  callVw: {
    backgroundColor: '#EFF9FF',
    height: 187,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },

  callNo: {
    fontWeight: '600',
    fontSize: 18,
    color: 'black',
    padding: 10,
    paddingTop: 20,
  },

  callTxt: {
    fontWeight: '400',
    fontSize: 14,
    color: 'black',
    padding: 10,
  },

  emailVw: {
    backgroundColor: '#EFF9FF',
    height: 187,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },

  emailNo: {
    fontWeight: '600',
    fontSize: 18,
    color: 'black',
    padding: 10,
    paddingTop: 20,
  },

  emailTxt: {
    fontWeight: '400',
    fontSize: 14,
    color: 'black',
    padding: 10,
  },
});

export default ContactUs;
