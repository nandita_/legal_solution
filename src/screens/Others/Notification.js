import React, { useEffect, useRef, useState } from 'react';
import { FlatList, Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { colorConstant } from '../../utils/constants';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { NativeModules } from 'react-native';
import CustomButton from '../../Custom/CustomButton';
import { NotificationApi, NotificationReadApi } from '../../redux/Slices/APISlices';
import { store } from '../../utils/store';
import moment from 'moment';
import { useSelector } from 'react-redux';
import toastShow from '../../utils/ToastMessage';


const Notification = () => {
  const [notifyData, setNotifyData] = useState([])
  const userProfile = useSelector((state) => state.counter.apiDataFlow)
  const NotificationFunction = async () => {
    try {
      let response = await store.dispatch(NotificationApi())
      if (response.payload.status === 200) {
        setNotifyData(response.payload.data)
        // console.log(response.payload.data, "]][][][][][][][]][][][");
      }
      else {
        toastShow(response.payload.message, colorConstant.red)
      }
    } catch (error) {
      console.log(error.response.message && error.message, "Catch error=============>>>>>");
    }
  }

  useEffect(() => {
    // if(userProfile?.jwtToken !== null){
    NotificationFunction()
    
  }, [])



  const notifyReadFunction = async (id) => {
    try {
      let response = await store.dispatch(NotificationReadApi(id))
      if (response.payload.status === 200) {
        NotificationFunction()
        console.log(response.payload.data, "++++++++++++++++++++++++++");
      }
      else {
        console.log(response.payload.message, "++++++++++++++++++++++++++------------------");
      }
    } catch (error) {
      console.log(response.error.message && error.message, colorConstant.red);
    }
  }

  const notificationCell = ({ item, index }) => (
    <>
      <TouchableOpacity style={notificationStyle.notificationVw}
        onPress={() =>
          notifyReadFunction(item._id)
        }>
        {item.isRead === false && <View style={notificationStyle.ImageCircle} />}
        <View style={notificationStyle.notificationBottomVw}>
          <Text style={notificationStyle.titleTxt}>{item?.title}{" "}{item?.content}</Text>
          <Text style={notificationStyle.timeTxt}>Last seen at{" "}{moment(item?.createdAt).format("LT")}</Text>
        </View>
      </TouchableOpacity>
      <View style={notificationStyle.LineVw} />
    </>
  );

  return (
    <AppSafeAreaView title="Notifications" bgColor="#D7F1FF" noPadding="false">
      <View style={styles.main}>

        <FlatList
          data={notifyData?.[0]?.notifications}
          renderItem={notificationCell}
          keyExtractor={item => item._id}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{
            backgroundColor: colorConstant.backgroundColor,
          }}
        />
      </View>
    </AppSafeAreaView>
  );
};

const notificationStyle = StyleSheet.create({
  notificationVw: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },

  notificationBottomVw: {
    width: '100%',
    padding: 15,
    paddingLeft: 30,
  },

  titleTxt: {
    color: 'black',
    fontWeight: 'semibold',
    fontSize: 14,
    paddingBottom: 10,
    lineHeight: 22,
  },

  timeTxt: {
    color: 'gray',
    fontWeight: 'semibold',
    fontSize: 14,
  },

  ImageCircle: {
    height: 10,
    width: 10,
    borderRadius: 10,
    backgroundColor: '#90CDF4',
    marginTop: 22,
    left: 16,
  },
  LineVw: {
    backgroundColor: 'white',
    height: 2,
  },
});

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.backgroundColor,
  },
});

export default Notification;
