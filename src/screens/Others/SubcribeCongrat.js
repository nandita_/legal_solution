import React from 'react';
import { FlatList, Image, ImageBackground, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';


import { Height, Width } from '../../dimensions/dimensions';
import CustomButton from '../../Custom/CustomButton';
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants';
import { store } from '../../utils/store';
import { setIntroStatus } from '../../redux/ParentFlow';
import moment from 'moment';




const SubcribeCongrat = (props) => {
  const {screenName} = props.route.params ?? "";

  const {finalData} = props.route.params ?? "";
  const {data} = props.route?.params ?? ""
  console.log(finalData,"LOGGOGOGOG",props);
  return (
    <>
      <StatusBar barStyle={'dark-content'} translucent={true}
        backgroundColor={'transparent'}
      />
      <View style={styles.main}>
        <View style={{
          marginTop: "30%",
          alignSelf: "center"
        }}>
          <Image
            source={imageConstants.Success}
            resizeMode='contain'
            style={{
              width: Width*0.50 ,
              height: Height*0.30,
            //   backgroundColor:"pink"
            }}
          />
        </View>


        <Text style={styles.registerText}>Congratulations</Text>

        <Text style={styles.subText}>Your <Text style={{fontFamily: fontConstant.semiBold }}>{data?.name}</Text> is activated. Now you can download and print all judgments.</Text>

        <Text style={styles.subcribeText}>Subscription Expiry: {moment(finalData).format('DD/MM/YYYY')}</Text>

        <CustomButton
          OnButtonPress={() => props.navigation.navigate("Home")}
          buttonText={ "Continue" }
          marginTop={40}
        />

       
      </View>
    </>

  )
}

export default SubcribeCongrat

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.backgroundColor,
  },

  stepText: {
    position: "absolute",
    right: 15,
    top: "11.5%",
    fontFamily: fontConstant.bold,
    color: colorConstant.black,
    fontSize: 13
  },

  registerText: {
    textAlign: "center",
    fontFamily: fontConstant.bold,
    fontSize: 20,
    color: colorConstant.black,
    // 
  },

  subText: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    marginTop: 8,
    color: colorConstant.black,
    textAlign: "center",
    lineHeight: 25,
    paddingRight: "2%",
    paddingLeft: "2%",
    marginTop: moderateVerticalScale(15)
  },

  subcribeText:{
    fontFamily: fontConstant.semiBold , 
    color: colorConstant.black,
    fontSize:14,
    textAlign: "center",
    marginTop: moderateVerticalScale(20)
  },

  codeView: {
    backgroundColor: colorConstant.white,
    padding: moderateScale(14),
    width: Width * 0.15,
    borderRadius: 5,
  },
  codeText: {

  },

  numberView: {
    paddingHorizontal: moderateScale(20),
    marginTop: moderateVerticalScale(14)
  },

  dividerView: {
    borderBottomWidth: 0.8,
    borderBottomColor: colorConstant.secondaryText,
    width: Width * 0.40,
    marginRight: "2%"
  },
  dividerView1: {
    borderBottomWidth: 0.8,
    borderBottomColor: colorConstant.secondaryText,
    width: Width * 0.40,
    marginLeft: "2%"
  },

  dividerContainer: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "center",
    marginTop: moderateVerticalScale(20)
  },

  orText: {
    fontFamily: fontConstant.medium,
    color: colorConstant.secondaryText + 90,
    fontSize: 14
  },

  signText: {
    textAlign: "center",
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.secondaryText,
    marginTop: moderateVerticalScale(30)
  },

  socialView: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "center",
    marginTop: moderateVerticalScale(10)
  },

  userText: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    textAlign: "center",
    marginTop: moderateVerticalScale(20),
    color: colorConstant.black
  }
})