import React, { useState } from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import {colorConstant, imageConstants} from '../../utils/constants';
import CustomText from '../../Custom/CustomText';
import CustomInput from '../../Custom/CustomInput';
import CustomButton from '../../Custom/CustomButton';
import toastShow from '../../utils/ToastMessage';
import { store } from '../../utils/store';
import { changePasswordApi } from '../../redux/Slices/APISlices';
import Validator from '../../utils/Validator';



const initialState = {
  showFlagCurrent:true,
  showFlagNew: true,
  showFlagConfirm: true,
  phoneNumber: "",
  password: "",
  passwordError: "",
  code: "",
  number: "",
  countryCode: "91",
  countryFlag: "IN",
  error: "",
}
const ChangePasswordOther = (props) => {
  const [iState, updateState] = useState(initialState);
    const {
        showFlagNew,
        showFlagCurrent,
        showFlagConfirm,
        phoneNumber,
        password,
        number,
        passwordError,
        code,
        countryCode,
        countryFlag,
    } = iState
  const screenName = "ChangePasswordOther"

  const [oldPassword, setOldPassword] = useState("")
  const [newPassword, setNewPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")
  const [loading, setLoading] = useState(false)

  const changePassword = async() => {
    setLoading(true)
    let reqData = {
        newPassword: newPassword,
        oldPassword: oldPassword,
    }
    try {
      let response = await store.dispatch(changePasswordApi(reqData))
      if(response.payload.status === 200){
        setLoading(false)
        console.log(response.payload , "ChangepasswordCEHCKKKKKK==========>>>>>>>>>");
        props.navigation.navigate('Congratulations', {screenName})
      }
      else{
        setLoading(false)
        // toastShow(response.payload.message, colorConstant.red)
        console.log(response , "ELSEEEEE ERRORRRRRR======>>>>>");
      }
    } catch (error) {
      console.log(error.message, "CATCH ERROR ========>>>");
    }
    
  }

  const validationCheck=()=>{ 
    console.log("dhasbd", oldPassword);
    Validator.isEmpty(oldPassword)
    ? toastShow("Please enter a current password",colorConstant.red)
    : Validator.isEmpty(newPassword)
    ? toastShow("Please enter a new password",colorConstant.red)
    : !Validator.isPassword(newPassword)
    ? toastShow("Password minimum length should 8,at least one uppercase, one lowercase, one number,one special character.", colorConstant.red)
    : Validator.isEmpty(confirmPassword)
    ? toastShow("Please enter a confirm password",colorConstant.red)
    : newPassword != confirmPassword 
    ? toastShow("Password mismatch",colorConstant.red)
    : changePassword()
  }

  return (
    <AppSafeAreaView
      title="Change Password"
      bgColor="#D7F1FF"
      noPadding={'false'}>
      <ScrollView 
      showsVerticalScrollIndicator={false}
      >
        <CustomText Title={'Current Password'} marginTop={30} />
        <View style={styles.passwordVw}>
          <CustomInput
          textWidth={"100%"}
          secureTextEntry={showFlagCurrent}
            source={imageConstants.lock}
            placeholder={'Enter Current Password'}
            fontSize={13}
            imageWidth={15}
            inputImage={true}
            marginLeft={'2%'}
            width={'90%'}
            margin={15}
            onChangeText={text => setOldPassword(text)}
            value={oldPassword}
          />
         <TouchableOpacity
                            onPress={() => updateState({ ...iState, showFlagCurrent: !showFlagCurrent })}
                            style={{
                                position: "absolute",
                                right: 16,
                                bottom: "25%",
                            }}>
          <Image
            source={showFlagCurrent ? imageConstants.closeEye : imageConstants.openEye}
            resizeMode="contain"
            style={{
              width: 20,
              height: 20,
            }}
          />
          </TouchableOpacity>
        </View>
        <CustomText Title={'New Password'} marginTop={30} />
        <View style={styles.passwordVw}>
          <CustomInput
          textWidth={"100%"}
          inputImage={true}
            source={imageConstants.lock}
            placeholder={'New Password'}
            secureTextEntry={showFlagNew}
            fontSize={13}
            imageWidth={15}
            marginLeft={'2%'}
            width={'90%'}
            margin={15}
            onChangeText={text => setNewPassword(text)}
            value={newPassword}
          />
           <TouchableOpacity
                            onPress={() => updateState({ ...iState, showFlagNew: !showFlagNew })}
                            style={{
                                position: "absolute",
                                right: 16,
                                bottom: "25%",
                            }}>
          <Image
            source={showFlagNew ? imageConstants.closeEye : imageConstants.openEye}
            resizeMode="contain"
            style={{
              width: 20,
              height: 20,
            }}
          />
          </TouchableOpacity>
        </View>
        <CustomText Title={'Confirm Password'} marginTop={30} />
        <View style={styles.passwordVw}>
          <CustomInput
            textWidth={"100%"}
            inputImage={true}
            source={imageConstants.lock}
            placeholder={'Confirm Password'}
            secureTextEntry={showFlagConfirm}
            fontSize={13}
            imageWidth={15}
            marginLeft={'2%'}
            width={'90%'}
            margin={15}
            onChangeText={text => setConfirmPassword(text)}
            value={confirmPassword}
          />
         <TouchableOpacity
                            onPress={() => updateState({ ...iState, showFlagConfirm: !showFlagConfirm })}
                            style={{
                                position: "absolute",
                                right: 16,
                                bottom: "25%",
                            }}>
          <Image
            source={showFlagConfirm ? imageConstants.closeEye : imageConstants.openEye}
            resizeMode="contain"
            style={{
              width: 20,
              height: 20,
            }}
          />
          </TouchableOpacity>
        </View>
      </ScrollView>
      <CustomButton
      disable={loading ? true : false}
      isLoading={loading}
        OnButtonPress={validationCheck}
        buttonText={'Change Password'}
        marginBottom={50}
      />
    </AppSafeAreaView>
  );
};

const styles = StyleSheet.create({
  passwordVw: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignSelf: 'center',
  },

  eyebtn: {},
});
export default ChangePasswordOther;
