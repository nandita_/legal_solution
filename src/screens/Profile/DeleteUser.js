import React from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import {colorConstant, fontConstant, imageConstants} from '../../utils/constants';
import { store } from '../../utils/store';
import { clearLogout, deleteUserApi, setIntroStatus } from '../../redux/Slices/APISlices';
import toastShow from '../../utils/ToastMessage';
import { useDispatch } from 'react-redux';


const DeleteUser = props => {
  // const userProfile = useSelector((state)=>state.counter.apiDataFlow.userProfile)
  // console.log(userProfile , "cjecljdnk");

const dispatch =  useDispatch();
  const deleteUserFunction = async() => {
    console.log("yes");
    try {
      let response = await store.dispatch(deleteUserApi());
      console.log("response",JSON.stringify(response));
      if (response.payload.status === 200) {
        console.log("response.payload.message",response.payload);
        toastShow(response.payload.message, colorConstant.green)
        setTimeout(()=> {
            dispatch(clearLogout())
            dispatch(setIntroStatus({introStatus:"auth"}));
        },100)
      }
      else{
        console.log(response.payload.message , "ELSE DELETE REPONSE=========>>>>>>");
      }
    } catch (error) {
      console.log(error.response , "Catch delete error");
    }
  }
  return (
    <View style={{backgroundColor:"rgba(0,0,0,0.7)", flex: 1}}>
      <View style={styles.bottomVw}>
        <Image source={imageConstants.LogoutImage} />
        <Text style={styles.TitleTxt}>Delete Account</Text>
        <Text style={styles.SubTitleTxt}>Are you sure want to Delete?</Text>
        <TouchableOpacity
          style={styles.NoBtn}
          onPress={() => {
            props.navigation.goBack();
          }}>
          <Text style={styles.NoTxt}>No</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.YesBtn} onPress={() => deleteUserFunction()}>
          <Text style={styles.YesTxt}>Yes</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  bottomVw: {
    height: 505,
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: 'white',
    margin: 17,
    top: 150,
  },

  TitleTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 20,
    color: 'black',
    padding: 16,
  },

  SubTitleTxt: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: 'black',
    padding: 16,
  },

  NoBtn: {
    margin: 14,
    height: 44,
    backgroundColor: colorConstant.blue,
    width: '90%',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    marginBottom: 0,
  },

  YesBtn: {
    margin: 16,
    height: 44,
    width: '90%',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'lightgray',
    borderWidth: 2,
  },

  NoTxt: {
    fontFamily: fontConstant.semiBold,
    fontSize: 16,
    color: 'white',
  },

  YesTxt: {
    fontFamily: fontConstant.semiBold,
    fontSize: 16,
    color: 'black',
  },
});

export default DeleteUser;
