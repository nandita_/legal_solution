import React from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import {colorConstant, fontConstant, imageConstants} from '../../utils/constants';
import { store } from '../../utils/store';
import { useDispatch, useSelector } from 'react-redux';
import { logoutApi, setIntroStatus, setUserProfile, setjwtToken } from '../../redux/Slices/APISlices';

const LogOut = props => {
  const dispatch = useDispatch();
  // const userProfile = useSelector((state)=>state.counter.apiDataFlow.userProfile)
  // console.log(userProfile , "cjecljdnk");


  const LogOutApiFunction = async() => {
      try {
        let response = await store.dispatch(logoutApi())
        console.log(response, "LOGOUT RESPONSEEE======>>>>>");
        if (response.payload.status === 200) {
          setTimeout(() => {
            dispatch(setUserProfile({userProfile:null}))
            dispatch(setjwtToken({jwtToken:''}))
            dispatch(setIntroStatus({introStatus:"auth"}))
          },100)
          // store.dispatch({userProfile:"null"})
        }
        else{
          console.log(response, "ESLE LOGOUT RESPONSEEE======>>>>>");
        }
      } catch (error) {
        console.log(error, "CATCH LOGOUT RESPONSEEE======>>>>>");
      }
  }
  return (
    <View style={{backgroundColor:"rgba(0,0,0,0.7)", flex: 1}}>
      <View style={styles.bottomVw}>
        <Image source={imageConstants.LogoutImage} />
        <Text style={styles.TitleTxt}>Logout</Text>
        <Text style={styles.SubTitleTxt}>Are you sure want to logout?</Text>
        <TouchableOpacity
          style={styles.NoBtn}
          onPress={() => {
            props.navigation.goBack();
          }}>
          <Text style={styles.NoTxt}>No</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.YesBtn} onPress={() => LogOutApiFunction()}>
          <Text style={styles.YesTxt}>Yes</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  bottomVw: {
    height: 505,
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: 'white',
    margin: 17,
    top: 150,
  },

  TitleTxt: {
    fontFamily: fontConstant.bold,
    fontSize: 20,
    color: 'black',
    padding: 16,
  },

  SubTitleTxt: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: 'black',
    padding: 16,
  },

  NoBtn: {
    margin: 14,
    height: 44,
    backgroundColor: colorConstant.blue,
    width: '90%',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    marginBottom: 0,
  },

  YesBtn: {
    margin: 16,
    height: 44,
    width: '90%',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'lightgray',
    borderWidth: 2,
  },

  NoTxt: {
    fontFamily: fontConstant.semiBold,
    fontSize: 16,
    color: 'white',
  },

  YesTxt: {
    fontFamily: fontConstant.semiBold,
    fontSize: 16,
    color: 'black',
  },
});

export default LogOut;
