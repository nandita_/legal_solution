import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import { Width } from '../../dimensions/dimensions'
import AppSafeAreaView from '../../Components/AppSafeAreaView'
import CustomButton from '../../Custom/CustomButton'
import { useDispatch, useSelector } from 'react-redux'
import { store } from '../../utils/store'
import { getProfileApi } from '../../redux/Slices/APISlices'
// import CustomInfoModal from '../Custom/CustomInfoModal'
// import CustomHeaderText from '../Custom/CustomHeaderText'



const MyProfile = (props) => {
  const dispatch = useDispatch();
  const [isInfoModalVisible, setInfoModalVisible] = useState(false);
  const toggleModal = () => {
    console.log('logout working');
    setInfoModalVisible(!isInfoModalVisible);
  };

  const userProfile = useSelector((state) => state.counter.apiDataFlow.userProfile)
  console.log(userProfile, "cjecljdnk");

  useEffect(() => {
    getUserProfileCall();
  }, [])

  const getUserProfileCall = async () => {
    try {
      let response = await store.dispatch(getProfileApi()).unwrap();
      // console.log(response, '{}{}{}{}{}{}}{}{{}}{}{{}}{}}{}');
      if (response.status === 200) {
        // console.log("get profile checkk========>>>>>>", response.data);

      }


    } catch (error) {
      console.log("error", error);
    }
  }



  return (
    <AppSafeAreaView title="My Profile" bgColor={colorConstant.themecolor}>
      <View style={styles.main}>
        {/* {uploadedImages !== "" ? */}
        <Image source={userProfile.image ? { uri: userProfile.image } : imageConstants.DummyProfile}
          resizeMode='cover'
          style={{
            width: 100,
            height: 100,
            borderRadius: 50,
            position: "absolute",
            alignSelf: "center",
            top: moderateVerticalScale(10),
            zIndex: 100
            // backgroundColor:"pink "
          }}
        />
        {/* :
          <Image source={imageConstants.DummyProfile}
            resizeMode='contain'
            style={{
              width: 100,
              height: 100,
              borderRadius: 50,
              position: "absolute",
              alignSelf: "center",
              top: moderateVerticalScale(10),
              zIndex: 100
              // backgroundColor:"pink "
            }}
          />
        } */}

        <View style={styles.container}>
          <View style={{
            flex: 1
          }}>
            {/* <TouchableOpacity>
              <Text style={styles.UploadText}>Upload/Change</Text>
            </TouchableOpacity> */}
            <View style={{ marginTop: "10%" }}>
              {/* <View style={{ borderBottomWidth: 0.8, borderBottomColor: "#00000080", width: Width, alignSelf: "center", marginTop: moderateVerticalScale(8) }} /> */}
              <View style={styles.TextContainer}>
                <Text style={styles.headTitle}>First name</Text>
                <Text style={styles.subTitle}>{userProfile?.firstName}</Text>
              </View>

              <View style={{ borderBottomWidth: 0.8, borderBottomColor: "#00000080", width: Width, alignSelf: "center", marginTop: moderateVerticalScale(15) }} />
              <View style={[styles.TextContainer, {
                // marginTop: moderateVerticalScale(20)
              }]}>
                <Text style={styles.headTitle}>Last Name</Text>
                <Text style={styles.subTitle}>{userProfile?.lastName}</Text>
              </View>

              <View style={{ borderBottomWidth: 0.8, borderBottomColor: "#00000080", width: Width, alignSelf: "center", marginTop: moderateVerticalScale(15) }} />

              <View style={[styles.TextContainer, {
                // marginTop: moderateVerticalScale(20)
              }]}>
                <Text style={styles.headTitle}>Email ID</Text>
                <Text style={styles.subTitle}>{userProfile?.email}</Text>
              </View>

              <View style={{ borderBottomWidth: 0.8, borderBottomColor: "#00000080", width: Width, alignSelf: "center", marginTop: moderateVerticalScale(15) }} />

              <View style={[styles.TextContainer, {
                // marginTop: moderateVerticalScale(20)
              }]}>
                <Text style={styles.headTitle}>Mobile Number</Text>
                <Text style={styles.subTitle}>{userProfile?.countryCode}{userProfile?.phoneNumber}</Text>
              </View>

              <View style={{ borderBottomWidth: 0.8, borderBottomColor: "#00000080", width: Width, alignSelf: "center", marginTop: moderateVerticalScale(15) }} />

              {
                userProfile?.designation !== undefined && (
                  <>
                    <View style={[styles.TextContainer, {
                      // marginTop: moderateVerticalScale(20)
                    }]}>
                      <Text style={styles.headTitle}>Designation</Text>
                      <Text style={styles.subTitle}>{userProfile?.designation}</Text>
                    </View>
                    <View style={{ borderBottomWidth: 0.8, borderBottomColor: "#00000080", width: Width, alignSelf: "center", marginTop: moderateVerticalScale(15) }} />
                  </>
                )
              }


              {
                userProfile?.organization !== undefined && (
                  <>
                    <View style={[styles.TextContainer, {
                      // marginTop: moderateVerticalScale(20)
                    }]}>
                      <Text style={styles.headTitle}>Oraganization</Text>
                      <Text style={styles.subTitle}>{userProfile?.organization}</Text>
                    </View>

                    <View style={{ borderBottomWidth: 0.8, borderBottomColor: "#00000080", width: Width, alignSelf: "center", marginTop: moderateVerticalScale(15) }} />
                  </>
                )
              }

            </View>

            <Text style={styles.billText}>Billing Address</Text>

            <Text style={styles.addrText}>{userProfile?.billingAddress},{" "}{userProfile?.city?.name},{" "}{userProfile?.state?.name},{" "}{userProfile?.country?.name}{" "}-{" "}{userProfile?.postalCode}</Text>
          </View>
          <CustomButton
            bottom={moderateVerticalScale(20)}
            OnButtonPress={() => props.navigation.navigate("ProfileEdit")}
            buttonText={"Edit"}
          />

        </View>


      </View>
    </AppSafeAreaView>
  )
}

export default MyProfile

const styles = StyleSheet.create({
  main: {
    flex: 1,
    // backgroundColor: "#EDF7FF"
  },
  container: {
    flex: 1,
    backgroundColor: colorConstant.backgroundColor,
    width: Width,
    marginTop: moderateVerticalScale(60),
    alignSelf: "center",
  },
  TextContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "90%",
    alignSelf: "center",
    marginTop: moderateVerticalScale(15)
  },

  headTitle: {
    fontFamily: fontConstant.medium,
    fontSize: 14,
    color: colorConstant.black
  },
  subTitle: {
    fontFamily: fontConstant.bold,
    fontSize: 14,
    color: colorConstant.black
  },
  DocContainer: {
    backgroundColor: colorConstant.white,
    width: "90%",
    padding: moderateVerticalScale(10),
    marginTop: moderateVerticalScale(20),
    alignSelf: "center",
    borderRadius: 20,
    borderColor: colorConstant.borderColor,
    borderWidth: 0.5,
    paddingVertical: moderateVerticalScale(8),
    paddingHorizontal: moderateScale(12),
    flexDirection: "row",
    alignItems: "center"
  },
  DocText: {
    fontFamily: fontConstant.bold,
    fontSize: 14,
    lineHeight: 25,
    color: colorConstant.black
  },
  CardText: {
    fontFamily: fontConstant.medium,
    fontSize: 14,
    lineHeight: 28,
    color: colorConstant.black
  },

  UploadText: {
    marginTop: "15%",
    textAlign: "center",
    fontFamily: fontConstant.semiBold,
    color: colorConstant.blue,
    fontSize: 16
  },

  billText: {
    paddingHorizontal: moderateVerticalScale(20),
    marginTop: moderateVerticalScale(20),
    fontFamily: fontConstant.bold,
    color: colorConstant.black,
    fontSize: 15
  },

  addrText: {
    paddingHorizontal: moderateVerticalScale(20),
    marginTop: moderateVerticalScale(10),
    fontFamily: fontConstant.regular,
    color: colorConstant.black,
    lineHeight: 22,
    fontSize: 15
  }
})