import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View, } from 'react-native'
import React, { useEffect, useState } from 'react'

import { colorConstant, fontConstant, imageConstants } from '../../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import { Width } from '../../dimensions/dimensions'
import AppSafeAreaView from '../../Components/AppSafeAreaView'
import CustomButton from '../../Custom/CustomButton'
import CustomText from '../../Custom/CustomText'
import CustomInput from '../../Custom/CustomInput'
import CustomDropdown from '../../Custom/CustomDropdown'
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view'
import { useSelector } from 'react-redux'
import { store } from '../../utils/store'
import { CityApi, CountryApi, StateApi, updateProfileApi } from '../../redux/Slices/APISlices'
import { Country, State, City } from 'country-state-city';
import Validator from '../../utils/Validator'
import toastShow from '../../utils/ToastMessage'
import { uploadImageOnS3 } from '../../utils/uploadFile'
import { launchImageLibrary } from 'react-native-image-picker';
// import CustomInfoModal from '../Custom/CustomInfoModal'
// import CustomHeaderText from '../Custom/CustomHeaderText'


const initialState = {
  firstName: "",
  lastName: "",
  email: "",
  image: "",
  phoneNumber: "",
  countryCode: "+91",
  billingAddress: "",
  postalCode: "",
  country: "",
  state: "",
  city: "",
  designation: "",
  organization: ""
}
const ProfileEdit = (props) => {
  const [icountryCode, setCountryCode] = useState();
  const [istate, setStateCode] = useState();
  const [icity, setCityCode] = useState();
  const [iState, updateState] = useState(initialState);
  const {
    firstName,
    lastName,
    email,
    countryCode,
    phoneNumber,
    country,
    countryFlag,
    image,
    state,
    city,
    postalCode,
    designation,
    organization,
    billingAddress
  } = iState
  const [countryArr, setCountryArr] = useState([])
  const [stateArr, setStateArr] = useState([])
  const [cityArr, setCityArr] = useState([])
  const [stateIsoCode, setStateIsoCode] = useState()
  const [loading, setLoading] = useState(false)
  const [uploadedImages, setUploadedImages] = useState('');
  const userProfile = useSelector((state) => state.counter.apiDataFlow.userProfile)
  console.log(userProfile, "cjecljdnk");


  const countryData = async () => {
    try {
      let response = await store.dispatch(CountryApi())
      if (response.payload.status === 200) {
        // console.log(response.payload.data[0].name, "Country Data ========>>>>>>>>");
        let temCountry = response.payload.data
        let resp = temCountry?.length > 0 ? temCountry?.map(country => { return { label: country.name, value: country.isoCode } }).flat() : []
        setCountryArr(resp)
      }
      else {
        console.log(response.payload.message, "Else response ==========>>");
      }
    } catch (error) {
      console.log(error?.response?.data ?? error?.message, "CATCH country ERROR ========>>>");
    }
  };


  const stateData = async (countryIsoCode) => {
    try {
      let response = await store.dispatch(StateApi(countryIsoCode))
      if (response.payload.status === 200) {
        // console.log(response.payload.data[0].name, "State Data ========>>>>>>>>");
        let temState = response.payload.data
        let resp = temState?.length > 0 ? temState?.map(state => { return { label: state.name, value: state.isoCode } }).flat() : []

        setStateArr(resp)
      }
      else {
        console.log(response.payload.message, "Else response ==========>>");
      }
    } catch (error) {
      console.log(error?.response?.data ?? error?.message, "CATCH country ERROR ========>>>");
    }
  };

  const cityData = async (stateIsoCode) => {
    try {
      let response = await store.dispatch(CityApi(stateIsoCode))
      if (response.payload.status === 200) {
        // console.log(response.payload.data[0].name, "State Data ========>>>>>>>>");
        let temCity = response.payload.data
        let resp = temCity?.length > 0 ? temCity?.map(city => { return { label: city.name, value: city.name } }).flat() : []
        setCityArr(resp)
      }
      else {
        console.log(response.payload.message, "Else response ==========>>");
      }
    } catch (error) {
      console.log(error?.response?.data ?? error?.message, "CATCH country ERROR ========>>>");
    }
  };

  useEffect(() => {
    // updateState({...iState , ...userProfile});
    mappingState();
    countryData()
  }, []);


  const handleUploadImage = async () => {
    await launchImageLibrary({}, async img => {
      if (!img?.didCancel) {
        const { uri, type: contentType, fileName } = img.assets[0];

        await uploadImageOnS3(
          uri,
          contentType,
          fileName,
          addImage,
          setLoading,
        );
      }
    });
  };

  const addImage = imageUri => {
    updateState({ ...iState, image: imageUri })
  };



  const mappingState = () => {
    updateState({
      ...iState,
      firstName: userProfile?.firstName ?? "",
      lastName: userProfile?.lastName ?? "",
      email: userProfile?.email ?? "",
      countryCode: userProfile?.countryCode ?? "",
      phoneNumber: userProfile?.phoneNumber ?? "",
      country: userProfile?.country?.name ?? "",
      countryFlag: userProfile?.countryFlag ?? "",
      image: userProfile?.image ?? "",
      state: userProfile?.state?.name ?? "",
      city: userProfile?.city?.name ?? "",
      postalCode: userProfile?.postalCode ?? "",
      designation: userProfile?.designation ?? "",
      organization: userProfile?.organization ?? "",
      billingAddress: userProfile?.billingAddress ?? ""

    })
  }

  const UpdateProfileApiFunction = async () => {
    setLoading(true)
    let reqData = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      image: image,
      phoneNumber: phoneNumber,
      countryCode: "+91",
      billingAddress: billingAddress,
      postalCode: postalCode,
      country:
      {
        name: country.label,
        isoCode: country.value
      },
      state: {
        name: state.label,
        isoCode: state.value,
        countryCode: country.value
      },
      city: {
        name: city.label,
        isoCode: state.value
      },
      designation: designation,
      organization: organization
    }

    try {
      console.log("reqData", reqData);
      let response = await store.dispatch(updateProfileApi(reqData));
      if (response.payload.status === 200) {
        setLoading(false)
        console.log(response.payload.data, "PROFILE DATA=====>>>>>>");
        props.navigation.navigate("MyProfile")
      }
      else {
        setLoading(false)
        console.log(response.payload, "ELSE PROFILE DATA=====>>>>>>");
      }
    } catch (error) {
      console.log(error?.response?.data ?? error?.message, "UPDATE PROFILE ERROR========>>>>>");
    }
  }


  console.log("user", userProfile);
  console.log("iState", iState);

  return (
    <AppSafeAreaView title="My Profile" bgColor={colorConstant.themecolor}>
      <View style={styles.main}>


        <Image source={image ? { uri: image } : imageConstants.DummyProfile}
          resizeMode='cover'
          style={{
            width: 100,
            height: 100,
            borderRadius: 50,
            position: "absolute",
            alignSelf: "center",
            top: moderateVerticalScale(10),
            zIndex: 100
            // backgroundColor:"pink "
          }}
        />

        <View style={styles.container}>
          <TouchableOpacity onPress={handleUploadImage}>
            <Text style={styles.UploadText}>Upload/Change</Text>
          </TouchableOpacity>
          <KeyboardAwareScrollView
            extraHeight={moderateScale(50)}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              //   backgroundColor:"pink"
              paddingBottom: moderateVerticalScale(80)
            }}

          >
            <View style={styles.nameContainer}>


              <View style={{
                width: "47%",
              }}>
                <CustomText
                  Title={"First Name"}
                  marginTop={1}
                  width={"100%"}
                />

                <CustomInput
                  onChangeText={(text) => updateState({ ...iState, firstName: text })}
                  value={firstName}
                  textWidth={"100%"}
                  inputImage={true}
                  margin={10}
                  fontSize={13}
                  placeholder={"First Name"}
                  imageWidth={15}
                  source={imageConstants.userIcon}
                  // padding={5}
                  width={"100%"}
                />

              </View>

              <View style={{
                width: "47%",
              }}>
                <CustomText
                  Title={"Last Name"}
                  marginTop={1}
                  width={"100%"}
                />

                <CustomInput
                  onChangeText={(text) => updateState({ ...iState, lastName: text })}
                  value={lastName}
                  textWidth={"100%"}
                  inputImage={true}
                  margin={10}
                  fontSize={13}
                  placeholder={"Last Name"}
                  imageWidth={15}
                  source={imageConstants.userIcon}
                  // padding={1}
                  width={"100%"}
                />

              </View>
            </View>

            <CustomText
              Title={"Email ID"}
              marginTop={20}
              width={"90%"}
            />

            <CustomInput
              editable={false}
              onChangeText={(text) => updateState({ ...iState, email: text })}
              value={email}
              inputImage={true}
              textWidth={"100%"}
              margin={10}
              fontSize={13}
              placeholder={"Eg- test@yourmail.com"}
              imageWidth={15}
              source={imageConstants.mail}
              // padding={1}
              width={"90%"}
            />

            <CustomText
              Title={"Mobile Number"}
              marginTop={20}
            />

            <View style={styles.numberView}>
              <View style={styles.codeView}>
                <Text style={styles.codeText}>{userProfile?.countryCode ?? "+91"}</Text>
              </View>
              <CustomInput
                // editable={true}
                maxLength={10}
                onChangeText={(text) => updateState({ ...iState, phoneNumber: text })}
                value={phoneNumber + ""}
                inputImage={true}
                textWidth={"90%"}
                source={imageConstants.phone}
                placeholder={"Enter Mobile Number"}
                fontSize={13}
                // marginLeft={"2%"}
                width={"81%"}
                margin={1}
              />
            </View>

            <CustomText
              Title={"Billing Address"}
              marginTop={20}
              width={"90%"}
            />

            <CustomInput
              onChangeText={(text) => updateState({ ...iState, billingAddress: text })}
              value={billingAddress}
              multiline={true}
              textAlignVertical={'top'}
              paddingTop={10}
              //  textAlignVertical={true}
              textWidth={"90%"}
              margin={10}
              fontSize={13}
              placeholder={"Enter Address"}
              imageWidth={15}
              height={moderateVerticalScale(100)}
              //  source={imageConstants.mail}
              padding={1}
              width={"90%"}
            />


            <View style={[styles.nameContainer, { marginTop: moderateVerticalScale(20) }]}>


              <View style={{
                width: "47%",
              }}>
                <CustomText
                  width={"100%"}
                  Title={"Postal Code"}
                  marginTop={1}
                />


                <CustomInput
                  onChangeText={(text) => updateState({ ...iState, postalCode: text })}
                  value={postalCode}
                  textWidth={"100%"}
                  margin={10}
                  fontSize={13}
                  placeholder={"Enter Postal Code"}
                  // imageWidth={15}
                  // source={imageConstants.mail}
                  padding={5}
                  width={"100%"}
                />

              </View>

              <View style={{
                width: "47%",
              }}>
                <CustomText
                  width={"100%"}
                  Title={"Country"}
                  marginTop={1}
                />

                <CustomDropdown
                  // source={imageConstants.mail}
                  data={countryArr}
                  value={country.value}
                  onChange={item => {
                    updateState({ ...iState, country: item, state: "", city: "", phoneNumber: iState.phoneNumber, email: iState.email, firstName: iState.firstName, lastName: iState.lastName, postalCode: iState.postalCode, billingAddress: iState.billingAddress, designation: iState.designation, organization: iState.organization });
                    // setCountryCode(item.value);
                    // console.log(item.value, "huihui")
                    setStateIsoCode(item.value)
                    stateData(item.value)
                    setStateArr([])
                    setCityArr([])
                  }}
                  margin={10}
                  placeholder={country}
                  fontSize={13}
                  // imageWidth={15}
                  // marginLeft={"2%"}
                  width={"100%"}
                  padding={5}
                />

              </View>
            </View>

            <View style={[styles.nameContainer, { marginTop: moderateVerticalScale(20) }]}>


              <View style={{
                width: "47%",
              }}>
                <CustomText
                  width={"100%"}
                  Title={"State"}
                  marginTop={1}
                />


                <CustomDropdown
                  data={stateArr}
                  value={state.value}
                  onChange={item => {
                    updateState({ ...iState, state: item, city: "", phoneNumber: iState.phoneNumber, email: iState.email, firstName: iState.firstName, lastName: iState.lastName, postalCode: iState.postalCode, billingAddress: iState.billingAddress, designation: iState.designation, organization: iState.organization });
                    cityData(stateIsoCode + '/' + item.value)
                    setCityArr([])
                  }}
                  // source={imageConstants.mail}
                  margin={10}
                  placeholder={state}
                  fontSize={13}
                  // imageWidth={15}
                  // marginLeft={"2%"}
                  width={"100%"}
                  padding={5}
                />

              </View>

              <View style={{
                width: "47%",
              }}>
                <CustomText
                  width={"100%"}
                  Title={"City"}
                  marginTop={1}
                />

                <CustomDropdown
                  data={cityArr}
                  value={city.value}
                  onChange={item => {
                    updateState({ ...iState, city: item, phoneNumber: iState.phoneNumber, email: iState.email, firstName: iState.firstName, lastName: iState.lastName, postalCode: iState.postalCode, billingAddress: iState.billingAddress, designation: iState.designation, organization: iState.organization });
                    setCityCode(item.value)
                  }}
                  margin={10}
                  padding={5}
                  placeholder={city}
                  fontSize={13}
                  width={"100%"}
                />

              </View>
            </View>

            <CustomText
              Title={"Designation (optional)"}
              marginTop={20}
              width={"90%"}
            />

            <CustomInput
              onChangeText={(text) => updateState({ ...iState, designation: text })}
              value={designation}
              textWidth={"90%"}
              margin={10}
              fontSize={13}
              placeholder={"Enter Designation "}
              // imageWidth={15}
              // source={imageConstants.mail}
              padding={5}
              width={"90%"}
            />

            <CustomText
              Title={"Organization (optional)"}
              marginTop={20}
              width={"90%"}
            />

            <CustomInput
              onChangeText={(text) => updateState({ ...iState, organization: text })}
              value={organization}
              textWidth={"90%"}
              margin={10}
              fontSize={13}
              placeholder={"Enter Organization "}
              // imageWidth={15}
              // source={imageConstants.mail}
              padding={5}
              width={"90%"}
            />
          </KeyboardAwareScrollView>
          <View style={{
            width: "100%",
            backgroundColor: colorConstant.backgroundColor,
            position: "absolute",
            bottom: 0,
            // paddingVertical: moderateVerticalScale(20)
          }}>
            <CustomButton
              disable={loading ? true : false}
              isLoading={loading}
              bottom={10}
              OnButtonPress={UpdateProfileApiFunction}
              buttonText={"Save"}
            />
          </View>
        </View>

      </View>
    </AppSafeAreaView>
  )
}

export default ProfileEdit

const styles = StyleSheet.create({
  main: {
    flex: 1,
    // backgroundColor: "#EDF7FF"
  },
  container: {
    flex: 1,
    backgroundColor: colorConstant.backgroundColor,
    width: Width,
    marginTop: moderateVerticalScale(60),
    alignSelf: "center",
  },
  TextContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "90%",
    alignSelf: "center",
    marginTop: moderateVerticalScale(15)
  },

  headTitle: {
    fontFamily: fontConstant.medium,
    fontSize: 14,
    color: colorConstant.black
  },
  subTitle: {
    fontFamily: fontConstant.bold,
    fontSize: 14,
    color: colorConstant.black
  },
  DocContainer: {
    backgroundColor: colorConstant.white,
    width: "90%",
    padding: moderateVerticalScale(10),
    marginTop: moderateVerticalScale(20),
    alignSelf: "center",
    borderRadius: 20,
    borderColor: colorConstant.borderColor,
    borderWidth: 0.5,
    paddingVertical: moderateVerticalScale(8),
    paddingHorizontal: moderateScale(12),
    flexDirection: "row",
    alignItems: "center"
  },
  DocText: {
    fontFamily: fontConstant.bold,
    fontSize: 14,
    lineHeight: 25,
    color: colorConstant.black
  },
  CardText: {
    fontFamily: fontConstant.medium,
    fontSize: 14,
    lineHeight: 28,
    color: colorConstant.black
  },

  UploadText: {
    marginTop: "15%",
    textAlign: "center",
    fontFamily: fontConstant.semiBold,
    color: colorConstant.blue,
    fontSize: 16
  },

  billText: {
    paddingHorizontal: moderateVerticalScale(20),
    marginTop: moderateVerticalScale(20),
    fontFamily: fontConstant.bold,
    color: colorConstant.black,
    fontSize: 15
  },

  addrText: {
    paddingHorizontal: moderateVerticalScale(20),
    marginTop: moderateVerticalScale(10),
    fontFamily: fontConstant.regular,
    color: colorConstant.black,
    lineHeight: 22,
    fontSize: 15
  },

  nameContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: moderateVerticalScale(30),
    width: Width * 0.90,
    alignSelf: "center",
    justifyContent: "space-between"
  },

  codeView: {
    backgroundColor: colorConstant.white,
    width: Width * 0.15,
    borderRadius: 5,
    padding: "4.3%",
    // alignItems:"center",
    //  height:moderateVerticalScale(46),

  },
  codeText: {
    fontFamily: fontConstant.regular,
    fontSize: 13,

    //  backgroundColor:"pink",
    textAlign: "center",
    color: colorConstant.secondaryText
  },

  numberView: {
    justifyContent: "space-between",
    // backgroundColor:"pink",
    // marginLeft:"2%",
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: moderateScale(20),
    marginTop: moderateVerticalScale(14)
  },
})