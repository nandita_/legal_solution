import React, { useState } from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Switch,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants';
import { moderateVerticalScale } from 'react-native-size-matters';

const Setting = (props) => {
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);




  return (
    <AppSafeAreaView title="Settings" bgColor="#D7F1FF">
      <View style={styles.pushVw}>
        <Text style={styles.pushTxt}>Push Notification</Text>
        <Switch
          trackColor={{ false: '#767577', true: 'white' }}
          thumbColor={'#14B082'}
          ios_backgroundColor="white"
          onValueChange={toggleSwitch}
          value={isEnabled}
          style={
            {
              // position: 'absolute',
              // right: 10,
            }
          }
        />
      </View>
      <TouchableOpacity style={styles.changeVw} onPress={() => props.navigation.navigate("ChangePasswordOther")} activeOpacity={0.8}>
        <Text style={styles.changeTxt}>Change Password</Text>

        <Image source={imageConstants.rightBlackIcon} />

      </TouchableOpacity>
      <TouchableOpacity style={styles.deleteVw} onPress={() => props.navigation.navigate("DeleteUser")}>
        <Image source={imageConstants.deleteIcon} />
        <Text style={styles.deleteTxt}>Delete Account</Text>
        <Image source={imageConstants.rightBlackIcon} style={{ position: "absolute", right: 15 }} />

      </TouchableOpacity>
    </AppSafeAreaView>
  );
};

const styles = StyleSheet.create({
  pushVw: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 60,
  },

  pushTxt: {
    fontWeight: '700',
    fontSize: 14,
    color: 'black',
    marginLeft: 4,
  },

  changeVw: {
    flexDirection: 'row',
    padding: "3%",
    // width:"100%",
    backgroundColor: 'white',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 16,
    borderRadius: 6,
  },

  changeTxt: {
    fontWeight: '700',
    fontSize: 14,
    color: 'black',
  },

  deleteVw: {
    flexDirection: 'row',
    padding: "4%",
    backgroundColor: 'white',
    // justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 16,
    borderRadius: 6,
    position: 'absolute',
    bottom: 60,
    width: '100%',
    alignSelf: 'center',
  },

  deleteTxt: {
    fontFamily: fontConstant.medium,
    fontSize: 14,
    color: '#D55F5A',
    paddingHorizontal: moderateVerticalScale(10)
  },
});

export default Setting;
