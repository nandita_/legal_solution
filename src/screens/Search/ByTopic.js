import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import AppSafeAreaView from '../../Components/AppSafeAreaView'
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import { Width } from '../../dimensions/dimensions'
import CustomText from '../../Custom/CustomText'
import CustomInput from '../../Custom/CustomInput'
import CustomDropdown from '../../Custom/CustomDropdown'
import CustomButton from '../../Custom/CustomButton'
import CustomDropdownSearch from '../../Custom/CustomDropdownSearch'
import { store } from '../../utils/store'
import { CategoryApi } from '../../redux/Slices/APISlices'
import { useIsFocused } from '@react-navigation/native'
import toastShow from '../../utils/ToastMessage'

const ByTopic = (props) => {
    const IsFocused = useIsFocused()
    const [activeArea, setActiveArea] = useState(0)
    const [activeCountry, setActiveCountry] = useState([])
    const [yearToArr, setYearToArr] = useState([]);
    const [yearToData, setYearToData] = useState('');
    const [yearFromArr, setYearFromArr] = useState([]);
    const [yearFromData, setYearFromData] = useState('');
    const [categoryData, setCategoryData] = useState('');
    const [areaData, setAreaData] = useState('Entire Document');
    const [categoryArr, setCategoryArr] = useState([])
    const [topicText, setTopicText] = useState('')
    const [subtopicText, setSubTopicText] = useState('')

    const selectArea = [
        {
            id: 1,
            name: "Entire Document",
            image: imageConstants.circle,
            image1: imageConstants.circleFill

        },
        {
            id: 2,
            name: "Digest Notes",
            image: imageConstants.circle,
            image1: imageConstants.circleFill

        },
        {
            id: 3,
            name: "Both",
            image: imageConstants.circle,
            image1: imageConstants.circleFill

        }
    ]

    const SelectCountry = [
        {
            id: 1,
            name: "India",
            image: imageConstants.tickEmpty,
            image1: imageConstants.tickfill
        },
        {
            id: 2,
            name: "Bangladesh",
            image: imageConstants.tickEmpty,
            image1: imageConstants.tickfill
        },
        {
            id: 3,
            name: "Pakistan",
            image: imageConstants.tickEmpty,
            image1: imageConstants.tickfill
        },
        {
            id: 4,
            name: "Sri Lanka",
            image: imageConstants.tickEmpty,
            image1: imageConstants.tickfill
        },
        {
            id: 5,
            name: "Nepal",
            image: imageConstants.tickEmpty,
            image1: imageConstants.tickfill
        },
        {
            id: 6,
            name: "Bhutan",
            image: imageConstants.tickEmpty,
            image1: imageConstants.tickfill
        },
    ]

    const CategoryData = async () => {
        try {
            let response = await store.dispatch(CategoryApi())
            if (response.payload.status === 200) {
                let tempCategory = response.payload.data
                let resp = tempCategory.length > 0 ? tempCategory?.map(catName => { return { label: catName.name, value: catName.name } }).flat() : []
                setCategoryArr(resp)
                // console.log(response.payload.data , "Category response--=-==-=-=-===-=-=-=-=");
            }
            else {
                console.log(response?.payload?.message, "Else category response =---=-=-=-==-=-=-=-=-");
            }
        } catch (error) {
            console.log(error.response.message && error.message, "Catch category reponse =-=-=-=-=-=-=-=-=");
        }
    }

    function generateYearsBetween(startYear = 1947, flag = "") {
        const endDate = new Date().getFullYear();
        let years = [];

        for (var i = startYear; i <= endDate; i++) {
            years.push({ label: i.toString(), value: i.toString() });
            startYear++;
        }
        if (flag === "yearToData") {
            setYearToArr(years)
        } else {
            setYearFromArr(years)
        }
        return
    }


    useEffect(() => {
        generateYearsBetween()
        CategoryData()
    }, [])

    const toggleCountry = (id) => {
        const updatedCountries = [...activeCountry];

        if (updatedCountries.includes(id)) {
            // Remove the country if already selected
            const index = updatedCountries.indexOf(id);
            updatedCountries.splice(index, 1);
        } else {
            // Add the country if not already selected
            updatedCountries.push(id);
        }
        setActiveCountry(updatedCountries);
        console.log(updatedCountries, "updatedCountries------------------>>>>>>>");
    };

    const setUpCountryData = () => {
        let countryStr = ""
        activeCountry.forEach(element => {
            countryStr += element + ","
        });
        countryStr = countryStr.slice(0, -1);
        // countryStr === ""
        //     ? toastShow("Please Select Country", colorConstant.red)
        Validator?.isEmpty(topicText)
            ? toastShow("Please Enter Topic", colorConstant.red)
            : Validator?.isEmpty(subtopicText)
                ? toastShow("Please Enter Subject", colorConstant.red)
                : props.navigation.navigate('SearchList', {
                    apiData: {
                        url: 'user/search_by_topic',
                        method: 'get',
                        params: { category: categoryData, startDate: yearFromData, endDate: yearToData, keyWords: "", countryName: countryStr, courtorderPdf: "", digestNotes: "", limit: "", page: "", act: "", courtName: "", popularName: "", partysName: "", caseNumber: "", topicName: topicText?.trim(), subTopicName: subtopicText?.trim() },
                    },
                })
    }

    useEffect(() => {
        if (IsFocused) {
            setYearFromData("")
            setYearToData("")
            setCategoryData("")
            setActiveCountry([])
            setTopicText("")
            setSubTopicText("")
        }
    }, [IsFocused])

    const validationCheck = () => {
        // yearFromData === ""
        //     ? toastShow("Please Select Starting Year", colorConstant.red)
        //     : yearToData === ""
        //         ? toastShow("Please Select Ending Year", colorConstant.red)
        //         : categoryData === ""
        //             ? toastShow("Please Select Category", colorConstant.red)
        //             : areaData === ""
        //                 ? toastShow("Please Select Area", colorConstant.red)
        setUpCountryData()
    }
    return (
        <AppSafeAreaView title="Find by Topic" bgColor={colorConstant.themecolor} noPadding="false">
            <View style={styles.main}>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        // paddingBottom: moderateVerticalScale(8)
                    }}>
                    <Text style={styles.headerText}>Search for Environmental Judgments in India, Bangladesh, Pakistan, Sri Lanka, Nepal, Bhutan.”</Text>

                    <Text style={styles.SubText}>Utilize this function to search the environmental court orders, if you want to search court orders for a specific topic.</Text>


                    <View style={styles.Container}>
                        <View style={styles.nameContainer}>


                            <View style={{
                                width: "47%",
                            }}>
                                <CustomText
                                    width={"100%"}
                                    Title={"Year From"}
                                    marginTop={1}
                                />


                                <CustomDropdownSearch
                                    data={yearFromArr}
                                    search={true}
                                    value={yearFromData}
                                    onChange={(item) => {
                                        console.log(item.value, "=-=-=-=-=-=-=-==-=");
                                        setYearFromData(item.value)
                                        setYearToArr([])
                                        generateYearsBetween(item.value, "yearToData")
                                    }}
                                    source={imageConstants.mail}
                                    margin={10}
                                    placeholder={"Year From"}
                                    fontSize={13}
                                    imageWidth={15}
                                    // marginLeft={"2%"}
                                    width={"100%"}
                                    padding={5}
                                />

                            </View>

                            <View style={{
                                width: "47%",
                            }}>
                                <CustomText
                                    width={"100%"}
                                    Title={"Year To"}
                                    marginTop={1}
                                />

                                <CustomDropdownSearch
                                    data={yearToArr}
                                    search={yearFromData ? true : false}
                                    onChange={(item) => {
                                        setYearToData(item.value)
                                        console.log(item.value, "==-=-=-=-=-=-=-=-=-=--=-=-");
                                    }}
                                    value={yearToData}
                                    source={imageConstants.mail}
                                    margin={10}
                                    placeholder={"Year To"}
                                    fontSize={13}
                                    imageWidth={15}
                                    // marginLeft={"2%"}
                                    width={"100%"}
                                    padding={5}
                                />

                            </View>
                        </View>
                        <CustomText
                            width={"100%"}
                            Title={"Category"}
                            marginTop={20}
                        />

                        <CustomDropdownSearch
                            search={true}
                            data={categoryArr}
                            value={categoryData}
                            onChange={(item) => {
                                setCategoryData(item.value)
                                console.log(item.value, "=-=[][]][][]]][][][][][");
                            }}
                            source={imageConstants.mail}
                            margin={10}
                            placeholder={"Category"}
                            fontSize={13}
                            imageWidth={15}
                            // marginLeft={"2%"}
                            width={"100%"}
                            padding={5}
                        />

                        <CustomText
                            width={"100%"}
                            Title={"Select Area"}
                            marginTop={20}
                        />

                        <View style={styles.selectView} >
                            {
                                selectArea.map((item, index) => {
                                    return (
                                        <View style={{ flexDirection: "row", alignItems: "center", }}>
                                            <TouchableOpacity onPress={() => {
                                                setActiveArea(index)
                                                setAreaData(item.name)
                                            }}>
                                                <Image source={activeArea === index ? item.image1 : item.image} resizeMode='contain' style={{ width: 15, height: 15 }} />
                                            </TouchableOpacity>
                                            <Text style={styles.itemText}>{item.name}</Text>
                                        </View>
                                    )
                                })
                            }
                        </View>

                        <CustomText
                            width={"100%"}
                            Title={"Select  Country"}
                            marginTop={20}
                        />

                        <View style={styles.selectCountryView} >
                            {
                                SelectCountry.map((item, index) => {
                                    return (
                                        <View style={{ flexDirection: "row", width: Width * 0.30, alignItems: "center", marginTop: moderateVerticalScale(8) }}>
                                            <TouchableOpacity onPress={() => {
                                                toggleCountry(item.name)
                                            }}>
                                                <Image source={activeCountry?.indexOf(item.name) !== -1 ? item.image1 : item.image} resizeMode='contain' style={{ width: 15, height: 15 }} />
                                            </TouchableOpacity>
                                            <Text style={styles.itemText}>{item.name}</Text>
                                        </View>
                                    )
                                })
                            }
                        </View>

                        <CustomText
                            width={"100%"}
                            Title={"Subject/Topic to search court orders"}
                            marginTop={20}
                        />

                        <CustomInput
                            value={topicText}
                            onChangeText={(text) => setTopicText(text)}
                            fontSize={12}
                            textWidth={"90%"}
                            width={"100%"}
                            placeholder={"Enter the subject/topic to search court orders"}
                            paddingHorizontal={1}
                            // padding={1}
                            margin={15}
                        />

                        <CustomText
                            width={"100%"}
                            Title={"sub-topic/subject to search court orders"}
                            marginTop={20}
                        />

                        <CustomInput
                            value={subtopicText}
                            onChangeText={(text) => setSubTopicText(text)}
                            fontSize={12}
                            textWidth={"100%"}
                            width={"100%"}
                            placeholder={"Enter sub-topic/subject to search court orders"}
                            paddingHorizontal={1}
                            // padding={1}
                            margin={15}
                        />

                        <CustomButton
                            width={"100%"}
                            OnButtonPress={validationCheck}
                            buttonText={"Search"}
                        />

                        <Text style={styles.bottomText}>Note: Use AND, NOT, OR and NEAR [all capitals] between words for boolean search.</Text>


                    </View>
                </ScrollView>
            </View>
        </AppSafeAreaView>
    )
}

export default ByTopic

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: colorConstant.themecolor,

    },

    headerText: {
        paddingHorizontal: moderateScale(25),
        width: Width,
        // backgroundColor:"pink",
        fontFamily: fontConstant.medium,
        fontSize: 14.5,
        color: colorConstant.blue
    },
    SubText: {
        paddingHorizontal: moderateScale(25),
        fontFamily: fontConstant.regular,
        fontSize: 13.5,
        marginTop: moderateVerticalScale(10),
        color: colorConstant.black
    },

    Container: {
        flex: 1,
        backgroundColor: colorConstant.backgroundColor,
        padding: "5%",
        marginTop: moderateVerticalScale(20)
    },

    nameContainer: {
        flexDirection: "row",
        alignItems: "center",
        // marginTop:moderateVerticalScale(2),
        width: Width * 0.90,
        alignSelf: "center",
        justifyContent: "space-between"
    },

    selectView: {
        flexDirection: "row",
        alignItems: "center",
        width: Width * 0.95,
        marginTop: moderateVerticalScale(10)
        // backgroundColor:"pink"
    },
    itemText: {
        marginLeft: "8%",
        fontFamily: fontConstant.regular,
        fontSize: 14,
        color: colorConstant.black
    },

    selectCountryView: {
        flexDirection: "row",
        alignItems: "center",
        width: Width,
        flexWrap: "wrap",
        marginTop: moderateVerticalScale(8)
    },

    bottomText: {
        marginTop: moderateVerticalScale(15),
        paddingBottom: moderateVerticalScale(20),
        fontFamily: fontConstant.regular,
        fontSize: 14,
        color: colorConstant.black
    }
})