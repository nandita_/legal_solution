import { FlatList, Image, PermissionsAndroid, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import { Width } from '../../dimensions/dimensions';
import { store } from '../../utils/store';
import { NotificationApi } from '../../redux/Slices/APISlices';
import toastShow from '../../utils/ToastMessage';
import RNFetchBlob from 'rn-fetch-blob';


const NotificationList = (props) => {
  const { data } = props ?? "";
  console.log(data, "fdxzsfxghjk");

  // ******Download funciton****
  const StartDownload = (courtorderPdf) => {
    console.log("*****downnloading*****");
    let date = new Date();
    let FILE_URL = courtorderPdf;
    let file_name = FILE_URL.toString().split("/")[FILE_URL.toString().split("/").length - 1];
    let file_ext = file_name.split(".")[file_name.split(".").length - 1];
    const { config, fs } = RNFetchBlob;
    let RootDir = fs.dirs.DownloadDir;
    console.log("RootDir", RootDir);
    try {
      let options = {
        fileCache: true,
        addAndroidDownloads: {
          path: RootDir + "/" + Math.floor(date.getDate() + date.getSeconds() / 2) + file_name,
          description: 'Downloading your file',
          notification: true,
          // useDownloadManager works with Android only
          useDownloadManager: true,
        },
      };
      config(options)
        .fetch('GET', FILE_URL)
        .then(res => {
          console.log('res -> ', JSON.stringify(res));
          alert('File Downloaded Successfully.');
          props.navigation.goBack()
          // setModalVisible(!isModalVisible);
        })

        .catch(error => console.log(error));
    }
    catch (error) {
      console.log("error", error)
    }
  }


  const takePermissionForDownload = async (link, courtorderPdf) => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Downloading the Form ',
          message: 'Needs permissions for downloading',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (Platform.Version > 29) {
        StartDownload(link, courtorderPdf);
      }
      else {
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log(granted, "{{{}[][][][][][][][[][][[++++");
          StartDownload(link, courtorderPdf);
        } else {
          // Linking.openSettings();
          toastShow("Permission denied", colorConstant.red)
          console.log("{{{}[][][][][][][][[][][[>>>>>>>>>>>>>>>>>>");
        }
      }
    } catch (err) {
      console.warn(err);
    }
  }

  const searchCell = ({ item, index }) => (
    <View style={stylesSearchCell.bottomVw}>
      <View style={{
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: moderateVerticalScale(5)
        // width:
      }}>
        <View>
          <Text style={{ fontFamily: fontConstant.medium, color: colorConstant.black, fontSize: 14 }}>Enactment Date</Text>
          <Text style={{ fontFamily: fontConstant.bold, color: colorConstant.black, fontSize: 14 }}>04/01/2024</Text>
        </View>
        <TouchableOpacity
          onPress={() => takePermissionForDownload(item?.courtorderPdf)}
          style={{
            backgroundColor: colorConstant.blue,
            width: "40%",
            alignItems: "center",
            flexDirection: "row",
            // padding: "2.5%",
            borderRadius: 3,
            justifyContent: "center"
          }}>
          <Image
            source={imageConstants.downloadWhite}
            resizeMode='contain'
            style={{
              width: 20,
              height: 20
            }}
          />
          <Text style={{
            fontFamily: fontConstant.semiBold,
            color: colorConstant.white,
            fontSize: 14,
            paddingHorizontal: moderateScale(8)
          }}>Download</Text>
        </TouchableOpacity>
      </View>

      <Text style={{ fontFamily: fontConstant.medium, color: colorConstant.black, fontSize: 14, marginTop: moderateVerticalScale(10) }}>Description</Text>
      <Text style={{ fontFamily: fontConstant.bold, color: colorConstant.black, fontSize: 14, marginTop: moderateVerticalScale(5), lineHeight: 22, paddingBottom: moderateVerticalScale(10) }}>Notification Pollution (Regulation and Control) Amendment Rules, 2002</Text>
    </View>
  );
  return (
    <View style={styles.bottomVw}>
      <FlatList
        data={data ?? []}
        renderItem={searchCell}
        keyExtractor={item => item.id}
        contentContainerStyle={{
          paddingBottom: moderateVerticalScale(40)
        }}
        ListEmptyComponent={
          <View style={{
            marginTop: "70%",
            flex: 1,
            alignSelf: "center",
            // backgroundColor:"pink",
            // position:"absolute",
            // height: Height
          }}>
            <Text style={{
              fontFamily: fontConstant.bold,
              color: colorConstant.black,
              fontSize: 20
            }}>Data Not Found</Text>
          </View>
        }
      />
    </View>
  )
}

export default NotificationList

const stylesSearchCell = StyleSheet.create({
  bottomVw: {
    backgroundColor: colorConstant.white,
    marginTop: moderateVerticalScale(20),
    borderColor: '#0000001A',
    borderWidth: 0.5,
    borderRadius: 5,
    width: Width * 0.90,
    alignSelf: "center",
    padding: "2%"
  },
});

const styles = StyleSheet.create({

  bottomVw: {
    backgroundColor: colorConstant.backgroundColor,
    flex: 1
    // backgroundColor: 'pink',
  },


})