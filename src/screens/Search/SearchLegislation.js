import { Image, Platform, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import AppSafeAreaView from '../../Components/AppSafeAreaView'
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import { Height, Width } from '../../dimensions/dimensions'
import CustomText from '../../Custom/CustomText'
import CustomInput from '../../Custom/CustomInput'
import CustomDropdown from '../../Custom/CustomDropdown'
import CustomButton from '../../Custom/CustomButton'
import CustomDropdownSearch from '../../Custom/CustomDropdownSearch'
import Validator from '../../utils/Validator'
import toastShow from '../../utils/ToastMessage'
import { BASE_URL } from '../../utils/Uri'
import { KeyboardAvoidingView } from 'react-native'
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view'
import { store } from '../../utils/store'
import { CategoryApi, CountryApi } from '../../redux/Slices/APISlices'
import { useIsFocused } from '@react-navigation/native'

const SearchLegislation = (props) => {
    const IsFocused = useIsFocused()
    const [categoryData, setCategoryData] = useState('');
    const [countryArr, setCountryArr] = useState([])
    const [keyWord, setKeyWord] = useState('')
    const [country, setCountry] = useState('')
    const [categoryArr, setCategoryArr] = useState([])


    const countryData = async () => {
        try {
            let response = await store.dispatch(CountryApi())
            if (response.payload.status === 200) {
                // console.log(response.payload.data[0].name, "Country Data ========>>>>>>>>");
                let temCountry = response.payload.data
                let resp = temCountry?.length > 0 ? temCountry?.map(country => { return { label: country.name, value: country.isoCode } }).flat() : []
                setCountryArr(resp)
            }
            else {
                console.log(response.payload.message, "Else response ==========>>");
            }
        } catch (error) {
            console.log(error?.response?.data ?? error?.message, "CATCH country ERROR ========>>>");
        }
    };
    console.log(country, ">>>>>>>>>>>>>>>>>>>>>>>>>");

    const CategoryData = async () => {
        try {
            let response = await store.dispatch(CategoryApi())
            if (response.payload.status === 200) {
                let tempCategory = response.payload.data
                let resp = tempCategory.length > 0 ? tempCategory?.map(catName => { return { label: catName.name, value: catName.name } }).flat() : []

                setCategoryArr(resp)
                // console.log(response.payload.data , "Category response--=-==-=-=-===-=-=-=-=");
            }
            else {
                console.log(response?.payload?.message, "Else category response =---=-=-=-==-=-=-=-=-");
            }
        } catch (error) {
            console.log(error.response.message && error.message, "Catch category reponse =-=-=-=-=-=-=-=-=");
        }
    }

    useEffect(() => {
        if (IsFocused) {
            CategoryData()
            countryData()
            setCategoryData("")
            setKeyWord("")
        }
    }, [IsFocused])

    const validationCheck = () => {
        // country.label === ""
        //     ? toastShow("Please Select Starting Year", colorConstant.red)
        //         : categoryData === ""
        //             ? toastShow("Please Select Category", colorConstant.red)
        //             : Validator?.isEmpty(keyWord)
        //         ? toastShow("Please Enter Term", colorConstant.red)
        props.navigation.navigate('SearchResult', {
            apiData: {
                url: 'user/legislation/search-list',
                method: 'get',
                params: { category: categoryData, keyWords: keyWord?.trim(), countryName: country },
            },
        })
    }


    return (
        <AppSafeAreaView title="Find Acts, Rules & Notifications" bgColor={colorConstant.themecolor} noPadding="false">
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{
                    backgroundColor: colorConstant.backgroundColor,
                }}
            >
                <View style={{
                    // height:Height*0.17,
                    backgroundColor: colorConstant.themecolor
                }}>
                    <Text style={styles.headerText}>Search for Environmental Judgments in India, Bangladesh, Pakistan, Sri Lanka, Nepal, Bhutan.”</Text>

                    <Text style={styles.SubText}>Select the country name, specify the topic/category, and apply filters to search for acts, rules, or notifications/office memoranda.</Text>
                </View>



                <View style={{
                    backgroundColor: colorConstant.backgroundColor,
                    paddingHorizontal: "5%",
                }}>
                    <CustomText
                        width={"100%"}
                        Title={"Search Legislations"}
                        marginTop={moderateVerticalScale(20)}
                    />

                    <CustomText
                        width={"100%"}
                        Title={"Select Country"}
                        marginTop={20}
                    />

                    <CustomDropdownSearch
                        data={countryArr}
                        value={country}
                        search={true}
                        onChange={item => {
                            setCountry(item.label)
                            console.log(item, "{{}{}{}{}{}{}{}{}{}{}}{}{}{}{}{}{");
                        }}
                        source={imageConstants.mail}
                        margin={10}
                        placeholder={"Country"}
                        fontSize={13}
                        imageWidth={15}
                        // marginLeft={"2%"}
                        width={"100%"}
                        padding={5}
                    />

                    <CustomText
                        width={"100%"}
                        Title={"Select Topic/Category (optional)"}
                        marginTop={20}
                    />

                    <CustomDropdownSearch
                        search={true}
                        source={imageConstants.mail}
                        margin={10}
                        placeholder={"Topic/Category"}
                        fontSize={13}
                        imageWidth={15}
                        // marginLeft={"2%"}
                        width={"100%"}
                        padding={5}
                        data={categoryArr}
                        value={categoryData}
                        onChange={(item) => setCategoryData(item.value)}
                    />

                    <CustomText
                        width={"100%"}
                        Title={"Search Keyword"}
                        marginTop={20}
                    />

                    <CustomInput
                        value={keyWord}
                        onChangeText={(text) => setKeyWord(text)}
                        fontSize={12}
                        textWidth={"100%"}
                        width={"100%"}
                        placeholder={"Type your Keyword"}
                        margin={15}
                        marginBottom={moderateVerticalScale(30)}
                    />


                </View>

            </KeyboardAwareScrollView>



            <View style={{
                backgroundColor: colorConstant.backgroundColor
            }}>
                <CustomButton
                    OnButtonPress={validationCheck}
                    width={"90%"}
                    buttonText={"Search"}
                    marginVertical={moderateVerticalScale(15)}
                />
            </View>
        </AppSafeAreaView>
    )
}

export default SearchLegislation

const styles = StyleSheet.create({
    main: {
        marginTop: moderateVerticalScale(10),


    },

    headerText: {
        paddingHorizontal: moderateScale(25),
        width: Width,
        // backgroundColor:"pink",
        fontFamily: fontConstant.medium,
        fontSize: 14.5,
        color: colorConstant.blue,
        // marginTop: moderateVerticalScale(10)
    },
    SubText: {
        paddingHorizontal: moderateScale(25),
        fontFamily: fontConstant.regular,
        fontSize: 13.5,
        marginTop: moderateVerticalScale(10),
        marginVertical: moderateVerticalScale(5),
        color: colorConstant.black
    },

    Container: {
        flex: 1,
        padding: "5%",
    },

    nameContainer: {
        flexDirection: "row",
        alignItems: "center",
        // marginTop:moderateVerticalScale(2),
        width: Width * 0.90,
        alignSelf: "center",
        justifyContent: "space-between"
    },

    selectView: {
        flexDirection: "row",
        alignItems: "center",
        width: Width,
        marginTop: moderateVerticalScale(10)
        // backgroundColor:"pink"
    },
    itemText: {
        marginLeft: "8%",
        fontFamily: fontConstant.regular,
        fontSize: 14,
        color: colorConstant.black
    },

    selectCountryView: {
        flexDirection: "row",
        alignItems: "center",
        width: Width,
        flexWrap: "wrap",
        marginTop: moderateVerticalScale(8)
    },

    bottomText: {
        marginTop: moderateVerticalScale(15),
        // paddingBottom: moderateVerticalScale(20),
        fontFamily: fontConstant.regular,
        fontSize: 14,
        color: colorConstant.black
    }
})