import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import AppSafeAreaView from '../../Components/AppSafeAreaView'
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'
import { Width } from '../../dimensions/dimensions'
import CustomText from '../../Custom/CustomText'
import CustomInput from '../../Custom/CustomInput'
import CustomDropdown from '../../Custom/CustomDropdown'
import CustomButton from '../../Custom/CustomButton'
import CustomDropdownSearch from '../../Custom/CustomDropdownSearch'
import { store } from '../../utils/store'
import { CategoryApi, CourtNameApi } from '../../redux/Slices/APISlices'
import toastShow from '../../utils/ToastMessage'
import { useIsFocused } from '@react-navigation/native'

const SearchPartyName = (props) => {
    const IsFocused = useIsFocused()
    const [activeArea, setActiveArea] = useState(0)
    const [activeCountry, setActiveCountry] = useState([])
    const [yearToArr, setYearToArr] = useState([]);
    const [yearToData, setYearToData] = useState('');
    const [yearFromArr, setYearFromArr] = useState([]);
    const [yearFromData, setYearFromData] = useState('');
    const [courtArr, setCourtArr] = useState([]);
    const [countryData, setCountryData] = useState('')
    const [courtData, setCourtData] = useState('')
    const [partyName, setPartyName] = useState('')
    const [categoryData, setCategoryData] = useState('');
    const [areaData, setAreaData] = useState('Entire Document');
    const [categoryArr, setCategoryArr] = useState([])
    const [popularName, setPopularName] = useState('')

    const selectArea = [
        {
            id: 1,
            name: "Entire Document",
            image: imageConstants.circle,
            image1: imageConstants.circleFill

        },
        {
            id: 2,
            name: "Digest Notes",
            image: imageConstants.circle,
            image1: imageConstants.circleFill

        },
        {
            id: 3,
            name: "Both",
            image: imageConstants.circle,
            image1: imageConstants.circleFill

        }
    ]

    const SelectCountry = [
        {
            id: 1,
            name: "India",
            image: imageConstants.tickEmpty,
            image1: imageConstants.tickfill
        },
        {
            id: 2,
            name: "Bangladesh",
            image: imageConstants.tickEmpty,
            image1: imageConstants.tickfill
        },
        {
            id: 3,
            name: "Pakistan",
            image: imageConstants.tickEmpty,
            image1: imageConstants.tickfill
        },
        {
            id: 4,
            name: "Sri Lanka",
            image: imageConstants.tickEmpty,
            image1: imageConstants.tickfill
        },
        {
            id: 5,
            name: "Nepal",
            image: imageConstants.tickEmpty,
            image1: imageConstants.tickfill
        },
        {
            id: 6,
            name: "Bhutan",
            image: imageConstants.tickEmpty,
            image1: imageConstants.tickfill
        },
    ]

    const CategoryData = async () => {
        try {
            let response = await store.dispatch(CategoryApi())
            if (response.payload.status === 200) {
                let tempCategory = response.payload.data
                let resp = tempCategory.length > 0 ? tempCategory?.map(catName => { return { label: catName.name, value: catName.name } }).flat() : []

                setCategoryArr(resp)
                // console.log(response.payload.data , "Category response--=-==-=-=-===-=-=-=-=");
            }
            else {
                console.log(response?.payload?.message, "Else category response =---=-=-=-==-=-=-=-=-");
            }
        } catch (error) {
            console.log(error.response.message && error.message, "Catch category reponse =-=-=-=-=-=-=-=-=");
        }
    }


    function generateYearsBetween(startYear = 1947, flag = "") {
        const endDate = new Date().getFullYear();
        let years = [];

        for (var i = startYear; i <= endDate; i++) {
            years.push({ label: i.toString(), value: i.toString() });
            startYear++;
        }
        if (flag === "yearToData") {
            setYearToArr(years)
        } else {
            setYearFromArr(years)
        }
        return
    }


    useEffect(() => {
        generateYearsBetween()
        CategoryData()
    }, [])

    const toggleCountry = (id) => {
        const updatedCountries = [...activeCountry];

        if (updatedCountries.includes(id)) {
            // Remove the country if already selected
            const index = updatedCountries.indexOf(id);
            updatedCountries.splice(index, 1);
        } else {
            // Add the country if not already selected
            updatedCountries.push(id);
        }
        setActiveCountry(updatedCountries);
        console.log(updatedCountries, "updatedCountries------------------>>>>>>>");
    };

    const setUpCountryData = () => {
        let countryStr = ""
        activeCountry.forEach(element => {
            countryStr += element + ","
        });
        countryStr = countryStr.slice(0, -1);
        // countryStr === ""
        //     ? toastShow("Please Select Country", colorConstant.red)
        //     : courtData === ""
        //         ? toastShow("Please Select Court Name", colorConstant.red)
        Validator?.isEmpty(partyName)
            ? toastShow("Please Enter Party Name", colorConstant.red)
            : props.navigation.navigate('SearchList', {
                apiData: {
                    url: 'user/search_by_party',
                    method: 'get',
                    params: { category: categoryData, startDate: yearFromData, endDate: yearToData, partysName: partyName?.trim(), keyWords: "", countryName: countryStr, courtorderPdf: "", digestNotes: "", limit: "", page: "", courtName: courtData, popularName: "", partysName: "", caseNumber: "", topicName: "", subTopicName: "" },
                },
            })
    }

    useEffect(() => {
        if (IsFocused) {
            setYearFromData("")
            setYearToData("")
            setCategoryData("")
            setActiveCountry([])
            setCourtData("")
            setPartyName("")
            setPopularName("")
        }
    }, [IsFocused])

    const CourtNameFunction = async (yearFromData, yearToData, countryData) => {
        let data = {
            yearFromData: yearFromData,
            yearToData: yearToData,
            countryData: countryData
        }
        try {
            let response = await store.dispatch(CourtNameApi(data))
            console.log(response.payload.data.result, "Court Response ====-=-=-=-=-=-=-=-=-=-=-=-");
            let tempData = response.payload.data.result
            let resp = tempData?.length > 0 ? tempData?.map(data => { return { label: data.courtName?.replace("-"," "), value:  data.courtName?.replace("-"," ") } }).flat() : []
            console.log(resp,"+++++");
            setCourtArr(resp)
        } catch (error) {
            console.log(error.response.message && error.message, "Catch reponse");
        }
    }

    console.log(courtData , "{{}{}{}{}{}{}{}{}");

    const validationCheck = () => {
        // yearFromData === ""
        //     ? toastShow("Please Select Starting Year", colorConstant.red)
        //     : yearToData === ""
        //         ? toastShow("Please Select Ending Year", colorConstant.red)
        //         : categoryData === ""
        //             ? toastShow("Please Select Category", colorConstant.red)
        //             : areaData === ""
        //                 ? toastShow("Please Select Area", colorConstant.red)
        setUpCountryData()
    }


    return (
        <AppSafeAreaView title="Find by Party(s) Name" bgColor={colorConstant.themecolor} noPadding="false">
            <View style={styles.main}>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        // paddingBottom: moderateVerticalScale(8)
                    }}>
                    <Text style={styles.headerText}>Search for Environmental Judgments in India, Bangladesh, Pakistan, Sri Lanka, Nepal, Bhutan.”</Text>

                    <Text style={styles.SubText}>Use this feature if you know the party name of the case you are looking for. It can be in first party name, second party name, in the middle of the name or even a partial name. Just type a party name in the ‘Search Text’ box, click on the ‘’ button, and find the result you’re looking for. Don't remember in party names? Use the Famous Case name search to find the case. e.g. Narmada Dam Case etc.</Text>


                    <View style={styles.Container}>
                        <View style={styles.nameContainer}>


                            <View style={{
                                width: "47%",
                            }}>
                                <CustomText
                                    width={"100%"}
                                    Title={"Year From"}
                                    marginTop={1}
                                />


                                <CustomDropdownSearch
                                    data={yearFromArr}
                                    search={true}
                                    value={yearFromData}
                                    onChange={(item) => {
                                        setYearFromData(item.value)
                                        setYearToArr([])
                                        generateYearsBetween(item.value, "yearToData")
                                    }}
                                    source={imageConstants.mail}
                                    margin={10}
                                    placeholder={"Year From"}
                                    fontSize={13}
                                    imageWidth={15}
                                    // marginLeft={"2%"}
                                    width={"100%"}
                                    padding={5}
                                />

                            </View>

                            <View style={{
                                width: "47%",
                            }}>
                                <CustomText
                                    width={"100%"}
                                    Title={"Year To"}
                                    marginTop={1}
                                />

                                <CustomDropdownSearch
                                    data={yearToArr}
                                    search={yearFromData ? true : false}
                                    onChange={(item) => setYearToData(item.value)}
                                    value={yearToData}
                                    source={imageConstants.mail}
                                    margin={10}
                                    placeholder={"Year To"}
                                    fontSize={13}
                                    imageWidth={15}
                                    // marginLeft={"2%"}
                                    width={"100%"}
                                    padding={5}
                                />

                            </View>
                        </View>
                        <CustomText
                            width={"100%"}
                            Title={"Category"}
                            marginTop={20}
                        />

                        <CustomDropdownSearch
                            search={true}
                            data={categoryArr}
                            value={categoryData}
                            onChange={(item) => setCategoryData(item.value)}
                            source={imageConstants.mail}
                            margin={10}
                            placeholder={"Category"}
                            fontSize={13}
                            imageWidth={15}
                            // marginLeft={"2%"}
                            width={"100%"}
                            padding={5}
                        />

                        <CustomText
                            width={"100%"}
                            Title={"Select Area"}
                            marginTop={20}
                        />

                        <View style={styles.selectView} >
                            {
                                selectArea.map((item, index) => {
                                    return (
                                        <View
                                            key={index}
                                            style={{ flexDirection: "row", alignItems: "center", }}>
                                            <TouchableOpacity onPress={() => {
                                                setActiveArea(index)
                                                setAreaData(item.name)
                                            }}>
                                                <Image source={activeArea === index ? item.image1 : item.image} resizeMode='contain' style={{ width: 15, height: 15 }} />
                                            </TouchableOpacity>
                                            <Text style={styles.itemText}>{item.name}</Text>
                                        </View>
                                    )
                                })
                            }
                        </View>

                        <CustomText
                            width={"100%"}
                            Title={"Select  Country"}
                            marginTop={20}
                        />

                        <View style={styles.selectCountryView} >
                            {
                                SelectCountry.map((item, index) => {
                                    return (
                                        <View
                                            key={index}
                                            style={{ flexDirection: "row", width: Width * 0.30, alignItems: "center", marginTop: moderateVerticalScale(8) }}>
                                            <TouchableOpacity onPress={() => {
                                                toggleCountry(item.name)
                                                CourtNameFunction(yearFromData, yearToData, item.name)
                                            }}>
                                                <Image source={activeCountry?.indexOf(item.name) !== -1 ? item.image1 : item.image} resizeMode='contain' style={{ width: 15, height: 15 }} />
                                            </TouchableOpacity>
                                            <Text style={styles.itemText}>{item.name}</Text>
                                        </View>
                                    )
                                })
                            }
                        </View>

                        <CustomText
                            width={"100%"}
                            Title={"Court Name"}
                            marginTop={20}
                        />

                        <CustomDropdownSearch
                            paddingHorizontal={moderateScale(15)}
                            source={imageConstants.mail}
                            margin={10}
                            placeholder={"Select Court Name"}
                            fontSize={13}
                            imageWidth={15}
                            // marginLeft={"2%"}
                            width={"100%"}
                            padding={5}
                            data={courtArr}
                            value={courtData}
                            onChange={(item) => setCourtData(item.value)}
                        />

                        <CustomText
                            width={"100%"}
                            Title={"Enter Party(s) Name"}
                            marginTop={20}
                        />

                        <CustomInput
                            value={partyName}
                            onChangeText={(text) => setPartyName(text)}
                            fontSize={12}
                            textWidth={"90%"}
                            width={"100%"}
                            placeholder={"Enter Party(s) Name"}
                            paddingHorizontal={1}
                            // padding={1}
                            margin={15}
                        />

                        <CustomButton
                            OnButtonPress={validationCheck}
                            width={"100%"}
                            buttonText={"Search"}
                        />

                        <Text style={styles.bottomText}>Note: Use AND, NOT, OR and NEAR [all capitals] between words for boolean search.</Text>

                        <CustomText
                            width={"100%"}
                            Title={"Popular Case Name"}
                            marginTop={20}
                        />

                        <CustomInput
                            value={popularName}
                            onChangeText={(text) => setPopularName(text)}
                            fontSize={12}
                            textWidth={"100%"}
                            width={"100%"}
                            placeholder={"Enter Popular Case Name"}
                            paddingHorizontal={1}
                            // padding={1}
                            margin={15}
                        />

                        <CustomButton
                            OnButtonPress={() => {
                                if (popularName?.trim() !== "") {
                                    props.navigation.navigate('SearchList', {
                                        apiData: {
                                            url: 'user/search_by_party',
                                            method: 'get',
                                            params: { category: categoryData, startDate: yearFromData, endDate: yearToData, partysName: partyName?.trim(), keyWords: "", countryName: "", courtorderPdf: "", digestNotes: "", limit: "", page: "", courtName: courtData, popularName: "", partysName: "", caseNumber: "" },
                                        },
                                    })
                                }
                                else {
                                    toastShow("Please Enter Popular Name", colorConstant.red)
                                }
                            }
                            }
                            width={"100%"}
                            buttonText={"Search"}
                        />

                    </View>
                </ScrollView>
            </View>
        </AppSafeAreaView>
    )
}

export default SearchPartyName

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: colorConstant.themecolor,

    },

    headerText: {
        paddingHorizontal: moderateScale(25),
        width: Width,
        // backgroundColor:"pink",
        fontFamily: fontConstant.medium,
        fontSize: 14.5,
        color: colorConstant.blue
    },
    SubText: {
        paddingHorizontal: moderateScale(25),
        fontFamily: fontConstant.regular,
        fontSize: 13.5,
        marginTop: moderateVerticalScale(10),
        color: colorConstant.black
    },

    Container: {
        flex: 1,
        backgroundColor: colorConstant.backgroundColor,
        padding: "5%",
        marginTop: moderateVerticalScale(20)
    },

    nameContainer: {
        flexDirection: "row",
        alignItems: "center",
        // marginTop:moderateVerticalScale(2),
        width: Width * 0.90,
        alignSelf: "center",
        justifyContent: "space-between"
    },

    selectView: {
        flexDirection: "row",
        alignItems: "center",
        width: Width * 0.95,
        marginTop: moderateVerticalScale(10)
        // backgroundColor:"pink"
    },
    itemText: {
        marginLeft: "8%",
        fontFamily: fontConstant.regular,
        fontSize: 14,
        color: colorConstant.black
    },

    selectCountryView: {
        flexDirection: "row",
        alignItems: "center",
        width: Width,
        flexWrap: "wrap",
        marginTop: moderateVerticalScale(8)
    },

    bottomText: {
        marginTop: moderateVerticalScale(15),
        // paddingBottom: moderateVerticalScale(20),
        fontFamily: fontConstant.regular,
        fontSize: 14,
        color: colorConstant.black
    }
})