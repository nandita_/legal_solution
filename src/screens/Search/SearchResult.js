import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  ActivityIndicator,
  PermissionsAndroid
} from 'react-native';
import React, { useEffect, useState } from 'react';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import {
  colorConstant,
  fontConstant,
  imageConstants,
} from '../../utils/constants';
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters';
import CustomText from '../../Custom/CustomText';
import SegmentedControl from '../../Custom/SegmentComponent';
import { Width } from '../../dimensions/dimensions';
import RulesList from './RulesList';
import axios from '../../utils/Interceptor'
import NotificationList from './NotificationList';
import moment from 'moment';
import RNFetchBlob from 'rn-fetch-blob';

const SearchResult = props => {
  const { apiData } = props?.route.params ?? "";
  const [isLoading, setLoading] = useState(false);
  const [tabIndex, setTabIndex] = React.useState(0);
  const [searchData, setSearchData] = useState([])
  console.log(tabIndex, "TBABBBBA");

  // ******Download funciton****
  const StartDownload = (courtorderPdf) => {
    console.log("*****downnloading*****");
    let date = new Date();
    let FILE_URL = courtorderPdf;
    let file_name = FILE_URL.toString().split("/")[FILE_URL.toString().split("/").length - 1];
    let file_ext = file_name.split(".")[file_name.split(".").length - 1];
    const { config, fs } = RNFetchBlob;
    let RootDir = fs.dirs.DCIMDir;
    console.log("RootDir", RootDir);
    try {
      let options = {
        fileCache: true,
        addAndroidDownloads: {
          path: RootDir + "/Legal" + Math.floor(date.getDate() + date.getSeconds() / 2) + file_name,
          description: 'Downloading your file',
          notification: true,
          // useDownloadManager works with Android only
          useDownloadManager: true,
        },
      };
      config(options)
        .fetch('GET', FILE_URL)
        .then(res => {
          console.log('res -> ', JSON.stringify(res));
          alert('File Downloaded Successfully.');
          props.navigation.goBack()
          // setModalVisible(!isModalVisible);
        })

        .catch(error => console.log(error));
    }
    catch (error) {
      console.log("error", error)
    }
  }


  const takePermissionForDownload = async (link, courtorderPdf) => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Downloading the Form ',
          message: 'Needs permissions for downloading',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (Platform.Version > 29) {
        StartDownload(link, courtorderPdf);
      }
      else {
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          StartDownload(link, courtorderPdf);
        } else {
          // Linking.openSettings();
          toastShow("Permission denied", colorConstant.red)
        }
      }
    } catch (err) {
      console.warn(err);
    }
  }

  const handleTabsChange = index => {
    setTabIndex(index);
    getSearchData(apiData, index)
  };

  const getSearchData = async (data, indexx) => {
    const { url, method } = data;
    var statusSearch = 'acts'
    if (indexx === 0) {
      statusSearch = 'acts'
    } else if (indexx === 1) {
      statusSearch = 'rules'
    } else {
      statusSearch = 'notifications/office memoranda'
    }
    try {
      let resp = await axios({
        url,
        method,
        params: { category: data.params.category, keyWords: data.params.keyWords, countryName: data.params.countryName, legislationType: statusSearch },
      });
      console.log(resp, "CHECK API DATA============>>>>>>>>");
      if (resp && resp?.data?.status === 200) {
        console.log("absdasdjas", resp?.data?.data?.result);
        setSearchData(resp?.data?.data?.result);
        // setTotal(resp?.data?.data?.total)

      } else {
        // toastShow(resp.data.message, colorConstant.red);

      }
    } catch (error) {
      console.log(error.message, "catch error");

    }
  };

  // console.log(searchData , "szdxfghjkl;kjhgvfcdxcghjk");

  useEffect(() => {
    getSearchData(apiData, 0)
  }, [])


  const searchCell = ({ item, index }) => (
    <View style={stylesSearchCell.bottomVw}>
      <View style={{
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: moderateVerticalScale(5)
        // width:
      }}>
        <View>
          <Text style={{ fontFamily: fontConstant.medium, color: colorConstant.black, fontSize: 14 }}>Enactment Date</Text>
          <Text style={{ fontFamily: fontConstant.bold, color: colorConstant.black, fontSize: 14 }}>{item?.enactmentDate}</Text>
        </View>
        <TouchableOpacity
          onPress={() => takePermissionForDownload(item?.courtorderPdf)}
          style={{
            backgroundColor: colorConstant.blue,
            width: "40%",
            alignItems: "center",
            flexDirection: "row",
            // padding: "2.5%",
            borderRadius: 3,
            justifyContent: "center"
          }}>
          <Image
            source={imageConstants.downloadWhite}
            resizeMode='contain'
            style={{
              width: 20,
              height: 20
            }}
          />
          <Text style={{
            fontFamily: fontConstant.semiBold,
            color: colorConstant.white,
            fontSize: 14,
            paddingHorizontal: moderateScale(8)
          }}>Download</Text>
        </TouchableOpacity>
      </View>

      <Text style={{ fontFamily: fontConstant.medium, color: colorConstant.black, fontSize: 14, marginTop: moderateVerticalScale(10) }}>Act No.</Text>
      <Text style={{ fontFamily: fontConstant.bold, color: colorConstant.black, fontSize: 14, marginTop: moderateVerticalScale(5) }}>{item?.actNo}</Text>
      <Text style={{ fontFamily: fontConstant.medium, color: colorConstant.black, fontSize: 14, marginTop: moderateVerticalScale(10) }}>Title of Act</Text>
      <Text style={{ fontFamily: fontConstant.bold, color: colorConstant.black, fontSize: 14, marginTop: moderateVerticalScale(5), paddingBottom: moderateVerticalScale(20) }}>{item?.titleOfAct}</Text>
    </View>
  );

  return (
    <AppSafeAreaView
      title="Search Results"
      bgColor={colorConstant.themecolor}
      noPadding="false">
      <View style={styles.bottomVw}>

        <View style={styles.SegmentContainer}>
          <SegmentedControl
            tabs={['Acts', 'Rules', 'Notifications/\nOffice Memoranda']}
            currentIndex={tabIndex}
            onChange={handleTabsChange}
            segmentedControlBackgroundColor={"#C2EAFF"}
            activeSegmentBackgroundColor={colorConstant.blue}
            activeTextColor={colorConstant.white}
            textColor="black"
            paddingVertical={18}
            borderTopLeftRadius={tabIndex == 0 ? 10 : 0}
            borderBottomLeftRadius={tabIndex == 0 ? 10 : 0}
            borderTopRightRadius={tabIndex == 2 ? 10 : 0}
            borderBottomRightRadius={tabIndex == 2 ? 10 : 0}
          />
        </View>

        <View style={{ flexDirection: 'row', paddingHorizontal: moderateScale(20) }}>
          <Text style={{
            fontFamily: fontConstant.medium,
            color: colorConstant.black,
            fontSize: 14
          }}>You Searched For: </Text>
          <Text style={{
            fontFamily: fontConstant.semiBold,
            color: colorConstant.black,
            fontSize: 14
          }}>{" "}Keyword (No. of Results)</Text>
        </View>

        {
          tabIndex == 0 ?

            <FlatList
              data={searchData ?? []}
              renderItem={searchCell}
              keyExtractor={item => item.id}
              contentContainerStyle={{
                paddingBottom: moderateVerticalScale(40)
              }}
              ListEmptyComponent={
                <View style={{
                  marginTop: "70%",
                  flex: 1,
                  alignSelf: "center",
                  // backgroundColor:"pink",
                  // position:"absolute",
                  // height: Height
                }}>
                  <Text style={{
                    fontFamily: fontConstant.bold,
                    color: colorConstant.black,
                    fontSize: 20
                  }}>Data Not Found</Text>
                </View>
              }
            />

            :
            tabIndex == 1 ?
              <RulesList data={searchData} />
              :
              tabIndex == 2 ?
                <NotificationList data={searchData} />
                :
                ""
        }

      </View>
    </AppSafeAreaView>
  );
};

const stylesSearchCell = StyleSheet.create({
  bottomVw: {
    backgroundColor: colorConstant.white,
    marginTop: moderateVerticalScale(20),
    borderColor: '#0000001A',
    borderWidth: 0.5,
    borderRadius: 5,
    width: Width * 0.90,
    alignSelf: "center",
    padding: "2%"
  },
});

const styles = StyleSheet.create({
  bottomVw: {
    backgroundColor: colorConstant.backgroundColor,
    flex: 1
    // backgroundColor: 'pink',
  },

  SegmentContainer: {
    alignSelf: "center",
    // marginTop: moderateVerticalScale(0)
  },
});

export default SearchResult;
