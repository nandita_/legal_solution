import { FlatList, Image, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View, PermissionsAndroid, Platform } from 'react-native'
import React, { useEffect, useRef, useState } from 'react'
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants'
import { Width } from '../../dimensions/dimensions'
import AppSafeAreaView from '../../Components/AppSafeAreaView'
import { moderateVerticalScale } from 'react-native-size-matters'
import CustomButton from '../../Custom/CustomButton'
import Pdf from 'react-native-pdf'
import RNFetchBlob from "rn-fetch-blob";
import toastShow from '../../utils/ToastMessage'
import WebView from 'react-native-webview'
import RNPrint from 'react-native-print';
import { useIsFocused } from '@react-navigation/native'
import { useSelector } from 'react-redux'

const Judgment = (props) => {
  const isFocused = useIsFocused()
  const pdfRef = useRef()
  const { item } = props.route.params ?? "";
  const PlanType = useSelector((state) => state.counter.apiDataFlow.PlanType)
  console.log(PlanType.subIsExpired, "=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");

  const [selectedPrinter, setSelectedPrinter] = useState(null);

  const selectPrinter = async () => {
    const printer = await RNPrint.selectPrinter({ x: 100, y: 100 });
    setSelectedPrinter(printer);
  };

  const silentPrint = async () => {
    if (!selectedPrinter) {
      alert('Must Select Printer First');
      return;
    }
  }

  const printRemotePDF = async () => {
    await RNPrint.print({ filePath: item?.digestNotes });
  };

  const customOptions = () => {
    return (
      <View>
        {selectedPrinter &&
          <View>
            <Text>{`Selected Printer Name: ${selectedPrinter.name}`}</Text>
            <Text>{`Selected Printer URI: ${selectedPrinter.url}`}</Text>
          </View>
        }
        <Button onPress={selectPrinter} title="Select Printer" />
        <Button onPress={silentPrint} title="Silent Print" />
      </View>
    );
  };

  useEffect(() => {
    if (isFocused) {
      if (PlanType.freeTrialIsExpir) {
        if (PlanType.planType == "freeTrial") {
          props.navigation.navigate("UpgradePlan")   // Free trail
        } else {
          if (PlanType.subIsExpired) {
            alert("Sub Expire") // Sub
          }
        }
      }
    }

  }, [])



  // ******Download funciton****
  const StartDownload = () => {
    console.log("*****downnloading*****");
    let date = new Date();
    let FILE_URL = item.digestNotes;
    let file_name = FILE_URL.toString().split("/")[FILE_URL.toString().split("/").length - 1];
    let file_ext = file_name.split(".")[file_name.split(".").length - 1];
    const { config, fs } = RNFetchBlob;
    let RootDir = fs.dirs.DCIMDir;
    console.log("RootDir", RootDir);
    try {
      let options = {
        fileCache: true,
        addAndroidDownloads: {
          path: RootDir + "/Legal" + Math.floor(date.getDate() + date.getSeconds() / 2) + file_name,
          description: 'Downloading your file',
          notification: true,
          // useDownloadManager works with Android only
          useDownloadManager: true,
        },
      };
      config(options)
        .fetch('GET', FILE_URL)
        .then(res => {
          console.log('res -> ', JSON.stringify(res));
          alert('File Downloaded Successfully.');
          props.navigation.goBack()
          // setModalVisible(!isModalVisible);
        })

        .catch(error => console.log(error));
    }
    catch (error) {
      console.log("error", error)
    }
  }


  const takePermissionForDownload = async (link) => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Downloading the Form ',
          message: 'Needs permissions for downloading',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (Platform.Version > 29) {
        StartDownload(link);
      }
      else {
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          StartDownload(link);
        } else {
          // Linking.openSettings();
          toastShow("Permission denied", colorConstant.red)
        }
      }
    } catch (err) {
      console.warn(err);
    }
  }


  const pdfUrl = item.digestNotes;

  const htmlContent = `
    <html>
      <head>
        <title>PDF Viewer</title>
        <style>
          body, html {
            margin: 0;
            padding: 0;
            height: 100%;
          }
          #pdf {
            width: 100%;
            height: 100%;
          }
        </style>
      </head>
      <body>
      <iframe id="pdf" src="${pdfUrl}" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>
      </body>
    </html>
  `;

  const INJECTED_JAVASCRIPT = `(function() {
    window.ReactNativeWebView.postMessage(JSON.stringify(window.location));
})();`;


  return (
    <AppSafeAreaView title="Judgement" bgColor={colorConstant.themecolor} noPadding="false">
      <View style={styles.main}>



        <View style={styles.Container}>
          {/* <Pdf
          trustAllCerts={false}
            ref={pdfRef}
            source={{ uri: item.digestNotes }}
            // source={{ uri: 'https://inter-com.s3.amazonaws.com/Invoice/1234_1710929372929_item.pdf' }}
            onLoadComplete={(numberOfPages, filePath) => {
              console.log(`Number of pages: ${numberOfPages}`);
            }}
            onPageChanged={(page, numberOfPages) => {
              console.log(`Current page: ${page}`);
            }}
            onError={(error) => {
              console.log(error);
            }}
            onPressLink={(uri) => {
              console.log(`Link pressed: ${uri}`);
            }}
            style={styles.pdf} /> */}

          <WebView
            ref={pdfRef}
            // javaScriptEnabled={true}
            // injectedJavaScript={INJECTED_JAVASCRIPT}
            source={{ uri: `https://docs.google.com/viewer?url=${item.digestNotes}` }}
            style={{ flex: 1 }}
          />

        </View>


        <View style={{
          position: "absolute",
          backgroundColor: colorConstant.backgroundColor,
          bottom: 0,
          flexDirection: "row",
          alignItems: "center",
          width: "100%",
          alignSelf: "center",
          justifyContent: "space-evenly"
        }}>

          {Platform.OS === 'ios' && customOptions()}
          <CustomButton
            OnButtonPress={() => printRemotePDF()}
            imageTrue={true}
            bottom={10}
            buttonText={"Print"}
            width={"45%"}
            source={imageConstants.print}
          />
          <CustomButton
            OnButtonPress={() => takePermissionForDownload()}
            imageTrue={true}
            color={colorConstant.black}
            backgroundColor={colorConstant.white}
            buttonText={"Download"}
            width={"45%"}
            bottom={10}
            source={imageConstants.download}
          />


        </View>


      </View>
    </AppSafeAreaView>
  )
}


export default Judgment

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.themecolor,
  },

  Container: {
    flex: 1,
    // paddingBottom: moderateVerticalScale(150),
    backgroundColor: colorConstant.white,
    padding: "5%",
    marginTop: moderateVerticalScale(20)
  },

  divContainer: {
    // width:Width*0.90,
    // alignSelf:"center",
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "#0000001A",
    backgroundColor: colorConstant.white,
    marginTop: moderateVerticalScale(15)
  },

  innerView: {
    paddingHorizontal: moderateVerticalScale(15),
    flexDirection: "row",
    marginTop: moderateVerticalScale(10),
    alignItems: "center",
    justifyContent: "space-between",

  },

  titleText: {
    color: colorConstant.black,
    fontFamily: fontConstant.regular,
    fontSize: 13
  },

  descText: {
    paddingBottom: moderateVerticalScale(10),
    marginTop: moderateVerticalScale(8),
    fontFamily: fontConstant.bold,
    fontSize: 14.5,
    lineHeight: 20,
    color: colorConstant.black,
    paddingHorizontal: moderateVerticalScale(15)
  },

  pdf: {
    flex: 1,
    // width:Dimensions.get('window').width,
    // height:Dimensions.get('window').height,
  },

  text1: {
    fontFamily: fontConstant.semiBold,
    fontSize: 16,
    color: colorConstant.black,
    marginTop: moderateVerticalScale(20),

  },

  text2: {
    fontFamily: fontConstant.semiBold,
    fontSize: 16,
    color: colorConstant.black,
    // lineHeight:20
  },

  text3: {
    fontFamily: fontConstant.semiBold,
    fontSize: 14,
    color: colorConstant.black,
    marginTop: moderateVerticalScale(20)
  },

  text4: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: colorConstant.black,
    marginTop: moderateVerticalScale(15)
  }

})