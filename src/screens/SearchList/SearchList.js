import { FlatList, Image, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { colorConstant, fontConstant, imageConstants } from '../../utils/constants'
import { Height, Width } from '../../dimensions/dimensions'
import axios from '../../utils/Interceptor'
import AppSafeAreaView from '../../Components/AppSafeAreaView'
import { moderateVerticalScale } from 'react-native-size-matters'
import toastShow from '../../utils/ToastMessage'
import moment from 'moment'

const SearchList = props => {
  const { apiData } = props?.route.params ?? "";
  // console.log(apiData.areaName, "CHECK API DATA============>>>>>>>>");
  const [searchData, setSearchData] = useState([])
  const [total, setTotal] = useState("");


  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity style={styles.divContainer} activeOpacity={0.8} onPress={() => props.navigation.navigate("Judgment", { item })}>
        <View style={styles.innerView}>
          <Text style={styles.titleText}>{item?.courtName}</Text>
          <Text style={styles.titleText}>{moment(item?.createdAt).format('DD/MM/YYYY')}</Text>
        </View>

        <Text style={styles.descText}>{item?.lawDetails?.actName} :{" "}<Text style={{ fontFamily: fontConstant.regular, color: colorConstant.black }}>{item?.digestNotes}</Text></Text>
      </TouchableOpacity>
    )
  }

  const getSearchData = async data => {
    const { url, method } = data;
    console.log(url, method, data?.params, "huihui");
    try {
      let resp = await axios({
        url,
        method,
        params: data?.params,
      });
      if (resp && resp?.data?.status === 200) {
        console.log("absdasdjas", resp?.data?.status, resp?.data);
        setSearchData(resp?.data?.data?.result);
        setTotal(resp?.data?.data?.total)

      } else {
        // toastShow(resp.data.message, colorConstant.red);

      }
    } catch (error) {
      console.log(error.message, "catch error");

    }
  };

  console.log(searchData, "szdxfghjkl;kjhgvfcdxcghjk");

  useEffect(() => {
    getSearchData(apiData)
  }, [])

  return (
    <AppSafeAreaView title="Search List" bgColor={colorConstant.themecolor} noPadding="false">
      <View style={styles.main}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            // paddingBottom: moderateVerticalScale(8)
          }}>


          <View style={styles.Container}>
            <FlatList
              data={searchData ?? []}
              renderItem={renderItem}
              contentContainerStyle={{
                // backgroundColor:"pink",
                // flex:1,
                // height:Height
              }}
              ListHeaderComponent={<>
                <Text style={{ fontFamily: fontConstant.regular, color: colorConstant.black, fontSize: 16 }}>You Searched For:{" "}{searchData && (<Text style={{ fontFamily: fontConstant.semiBold, color: colorConstant.black, fontSize: 16 }}>{total ? total : '0'}</Text>)}</Text>
              </>}
              ListEmptyComponent={<>
                <View style={{
                  marginTop: "80%",
                  flex: 1,
                  alignSelf: "center",
                  // backgroundColor:"pink",
                  // position:"absolute",
                  // height: Height
                }}>
                  <Text style={{
                    fontFamily: fontConstant.bold,
                    color: colorConstant.black,
                    fontSize: 20
                  }}>Data Not Found</Text>
                </View>
              </>}
              keyExtractor={item => item.id}
            />
          </View>
        </ScrollView>
      </View>
    </AppSafeAreaView>
  )
}

export default SearchList

const styles = StyleSheet.create({
  main: {
    flex: 1,
    marginTop: moderateVerticalScale(20),
    // backgroundColor: colorConstant.themecolor,
    backgroundColor: colorConstant.backgroundColor,
    // backgroundColor:"red"
  },

  Container: {
    // flex: 1,
    // backgroundColor: colorConstant.backgroundColor,
    padding: "5%",
    // marginTop: moderateVerticalScale(20)
  },

  divContainer: {
    // width:Width*0.90,
    // alignSelf:"center",
    borderRadius: 5,
    borderWidth: 0.8,
    borderColor: "#0000001A",
    backgroundColor: colorConstant.white,
    marginTop: moderateVerticalScale(15)
  },

  innerView: {
    paddingHorizontal: moderateVerticalScale(15),
    flexDirection: "row",
    marginTop: moderateVerticalScale(10),
    alignItems: "center",
    justifyContent: "space-between",

  },

  titleText: {
    color: colorConstant.black,
    fontFamily: fontConstant.regular,
    fontSize: 13
  },

  descText: {
    paddingBottom: moderateVerticalScale(10),
    marginTop: moderateVerticalScale(8),
    fontFamily: fontConstant.bold,
    fontSize: 14.5,
    lineHeight: 20,
    color: colorConstant.black,
    paddingHorizontal: moderateVerticalScale(15)
  }

})