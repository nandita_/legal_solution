import React, { useState } from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import AppSafeAreaView from '../../Components/AppSafeAreaView';
import {colorConstant, fontConstant, imageConstants} from '../../utils/constants';
import { Height, Width } from '../../dimensions/dimensions';
import CustomButton from '../../Custom/CustomButton';
import { moderateVerticalScale } from 'react-native-size-matters';

const UpgradePlan = props => {
    const [contentChange , setContentChange] = useState(false);
  return (
    <View style={{backgroundColor:"rgba(0,0,0,0.7)", flex: 1}}>
      <View style={styles.bottomVw}>
        <Image source={imageConstants.upgrade} resizeMode='contain' style={{width:160, height:160}}/>
        <Text style={styles.TitleTxt}>{contentChange ? "Your Monthly Payment Overdue" : "Your Free Trial has Expired"}</Text>
        {
            contentChange ? 
            <Text style={styles.SubTitleTxtChange}>Your <Text style={{fontFamily: fontConstant.semiBold, color: colorConstant.red}}>Monthly{" "}</Text>Payment is overdue since{" "}<Text style={{fontFamily: fontConstant.semiBold}}>1st Jan 2024</Text>.</Text>
            :
            <Text style={styles.SubTitleTxt}>Your <Text style={{fontFamily: fontConstant.semiBold}}>1 Month Free</Text> Trial has now expired.</Text>
        }
        
        <Text style={styles.descText}>{contentChange ? "Kindly pay your monthly payment to continue your subscription services." : "You no longer have access to the SAREL Judgments. Let’s change that, To continue using SAREL Judgments, you will need to upgrade your plan."}</Text>
        <CustomButton 
        OnButtonPress={() => setContentChange(true)}
        buttonText={contentChange ? "Pay Now (₹100)" : "Upgrade Plan"}
        marginTop={contentChange && moderateVerticalScale(40)}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  bottomVw: {
    height: Height*0.60,
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: 'white',
    margin: 17,
    top: "25%",
  },

  TitleTxt: {
   fontFamily: fontConstant.bold,
    fontSize: 20,
    color: 'black',
    marginTop: moderateVerticalScale(10)
    // padding: 16,
  },

  SubTitleTxt: {
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: 'black',
    padding: 16,
  },

  SubTitleTxtChange:{
    fontFamily: fontConstant.regular,
    fontSize: 14,
    color: 'black',
    padding: 16,
    width:Width*0.80,
    textAlign:"center"
  },

  descText:{
    fontFamily: fontConstant.regular,
    fontSize:14,
    color: colorConstant.black,
    textAlign:"center",
    width:Width*0.80,
    // backgroundColor:"pink",
    lineHeight:24
  },

  NoBtn: {
    margin: 14,
    height: 44,
    backgroundColor: '#2B59FF',
    width: '90%',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    marginBottom: 0,
  },

  YesBtn: {
    margin: 16,
    height: 44,
    width: '90%',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    borderColor: 'lightgray',
    borderWidth: 2,
  },

  NoTxt: {
    fontWeight: '600',
    fontSize: 16,
    color: 'white',
  },

  YesTxt: {
    fontWeight: '600',
    fontSize: 16,
    color: 'black',
  },
});

export default UpgradePlan;
