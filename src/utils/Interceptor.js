import axios from 'axios';
import { BASE_URL } from './Uri';
import { store } from './store';
import { colorConstant } from './constants';
import toastShow from './ToastMessage';
import { clearLogout, setIntroStatus, setLogoutValue, setUserProfile, setjwtToken } from '../redux/Slices/APISlices';
import { useDispatch } from 'react-redux';


// const dispatch = useDispatch()

// Create an Axios instance
const instance = axios.create({
  baseURL: BASE_URL,
  timeout: 15000, // 15 seconds timeout
  headers: {
    'Content-Type': 'application/json',
  },
});

// Request interceptor
instance.interceptors.request.use(
  async function (config) {
    // console.log(config, "confih++++++++++++++++++++++");
    let { counter } = store.getState();
    // console.log(counter.apiDataFlow, "STORE DATAAAAAAA=========<<<<<<");
    let token = '';
    let userID = '';
    if (counter) {
      token = counter?.apiDataFlow?.jwtToken;
      // userID = counter.apiDataFlow.userProfile._id;
    } else {
      token = '';
      userID = '';
    }
    // console.log("counter===token",counter);

    if (token) {
      config.headers['Authorization'] = token;
    }
    console.log("}{}{}{}{}{}{}{}{}{", config);
    return config;

  },
  function (error) {
    console.log('interceptor request-->', error.message);
    return Promise.reject(error);
  },
);

// Response interceptor
instance.interceptors.response.use(
  function (response) {
    // console.log("request at interceptor------>",response);
    return response;
  },
  function (error) {
    console.log("Response  ERROR at interceptor------>", error?.response);
    if (error?.response?.status == 404) {
      console.log(error?.response, "User Doesn't exists-------------------------->>>>>");
      // if (error?.response?.message === "User Doesn't exists"){
      toastShow("User Doesn't exists", colorConstant.red);
      // }
      // else{
      //   toastShow("Data not Found", colorConstant.red);
      // }
      return Promise.reject(error);
    }
    if (error?.response?.status == 409) {
      toastShow("User already exists", colorConstant.red);
      return Promise.reject(error);
    }
    if (error?.response?.status == 400) {
      toastShow(error?.response?.data?.message, colorConstant.red)
      console.log("400 response", error.response.data.message);
      return Promise.reject(error);
    }
    if (error?.response?.status == 401) {
      // store.dispatch(setLogoutValue({
      //   introStatus: 'auth',
      //   jwtToken:'',
      //   userProfile: null
      // })
      setTimeout(() => {
        store.dispatch(setLogoutValue())
        // store.dispatch(setIntroStatus({introStatus:'auth'}))
      }, 100)

      // );


      setTimeout(() => {
        toastShow(
          'Your account has been accessed from another device. This session has been logged out.',
          colorConstant.red,
        );
      }, 10);
      return Promise.reject(error);
    } else {
      console.error(
        'Response interceptor Axios error:',
        error?.response?.data?.message ?? error?.message,
      ); // Handle the error accordingly
      return Promise.reject(error);
    }
  },
);

export default instance;
