import { Platform, ToastAndroid } from 'react-native';
import Toast from 'react-native-root-toast';
import { STATUS_BAR_HEIGHT } from '../dimensions/dimensions';
// import { colorConstant } from './constant';
// import { heightPercentageToDP } from './responsiveFIle';

const toastShow = (message, type, color) => {
    if (Platform.OS == "ios") {
        Toast.show(message, {
            position: 50,
            shadow: true,
            animation: true,
            hideOnPress: true,
            textColor: color == "" ? "#FFFFFF" : color,
            delay: 0,
            backgroundColor: type,
            onShow: () => {
                // calls on toast\`s appear animation start
            },
            onShown: () => {
                // calls on toast\`s appear animation end.
            },
            onHide: () => {
                // calls on toast\`s hide animation start.
            },
            onHidden: () => {
                // calls on toast\`s hide animation end.
            }
        });
    }
    else {
        ToastAndroid.showWithGravityAndOffset(
            message,
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
            25,
            50,
        );
    }

}

export default toastShow