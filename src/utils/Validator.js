import { Alert } from "react-native/types";

export default Validator = {
    // isEmail: (email) => {
    //     const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //     return re.test(String(email).toLowerCase());
    // },

    isEmail: (email) => {
       const re = /^[a-zA-Z0-9.]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
       return re.test(String(email).toLowerCase());
    },

     isPassword : (password) => {
        const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]{8,}$/;
        return re.test(password);
      },
      
    isEmpty: (string) => {
        return string?.trim() === "" 
    },
    isName: (string) => {
        const re = /^[a-zA-Z ]+$/
        return re.test(string)
    },
    // isphoneNumber: (string) => {
    //     console.log(string.length);
    //     let regex = new RegExp(/[0-9]{10}/);
 
    //     // if mobile_number
    //     // is empty return false
    //     if (string == null) {
    //         return "false";
    //     }
     
    //     // Return true if the mobile_number
    //     // matched the ReGex
    //     if (regex.test(string) == true) {
    //         return true;
    //     }
    //     else {
    //         return false;
    //     }
    // },

    isphoneNumber :(string) => {
        const regex = /^\d{8,13}$/; // This regex matches a string of 9 to 12 digits.
        return regex.test(string);
      },

    isAlphabet: (string) => {
        const re = /^(?:[A-Za-z]+|\d+)$/
        return re.test(string)
    },
    isFormattedNumberEmpty:(string) =>
    {
        console.log(string.length)
        return string.length < 2 ? false : true
    },
    isFormattedNumber:(string) =>
    {
        return string.length == 13 ? false : true
    }
}