import Intro1 from '../assets/images/Intro1.png';
import Intro2 from '../assets/images/Intro2.png';
import Intro3 from '../assets/images/Intro3.png';
import BackIcon from '../assets/icons/BackIcon.png';
import Logo from '../assets/icons/Logo.png';
import phone from '../assets/icons/phone.png';
import searchIcon from '../assets/icons/searchIcon.png';
import FilterIcon from '../assets/icons/FilterIcon.png';
import bellIcon from '../assets/icons/bellIcon.png';
import companyFullImage from '../assets/images/companyFullImage.png';
import CompanyImage from '../assets/images/CompanyImage.png';
import google from '../assets/images/google.png';
import facebook from '../assets/images/facebook.png';
import Success from '../assets/images/Success.png';
import lock from '../assets/icons/lock.png';
import mail from '../assets/icons/mail.png';
import userIcon from '../assets/icons/userIcon.png';
import closeEye from '../assets/icons/closeEye.png';
import ArrowRight from '../assets/icons/ArrowRight.png';
import home from '../assets/icons/home.png';
import homeColor from '../assets/icons/homeColor.png';
import profile from '../assets/icons/profile.png';
import profileColor from '../assets/icons/profileColor.png';
import blog from '../assets/icons/blog.png';
import blogColor from '../assets/icons/blogColor.png';
import premium from '../assets/icons/premium.png';
import premiumColor from '../assets/icons/premiumColor.png';
import circle from '../assets/icons/circle.png';
import circleFill from '../assets/icons/circleFill.png';
import tickfill from '../assets/icons/tickfill.png';
import tickEmpty from '../assets/icons/tickEmpty.png';
import print from '../assets/icons/print.png';
import download from '../assets/icons/download.png';
import emailBigIcon from '../assets/icons/emailBigIcon.png';
import callBigIcon from '../assets/icons/callBigIcon.png';
import LogoutImage from '../assets/images/LogoutImage.png';
import upgrade from '../assets/images/upgrade.png';
import SuccessImage from '../assets/images/SuccessImage.png';
import closeEyeIcon from '../assets/icons/closeEyeIcon.png';
import deleteIcon from '../assets/icons/deleteIcon.png';
import tickIcon from '../assets/icons/tickIcon.png';
import rightBlackIcon from '../assets/icons/rightBlackIcon.png';
import userProfile from '../assets/images/userProfile.png';
import DrawerUser from '../assets/icons/DrawerUser.png';
import Calendar from '../assets/icons/Calendar.png';
import Page from '../assets/icons/Page.png';
import Settings from '../assets/icons/Settings.png';
import cross from '../assets/icons/cross.png';
import LogoutDrawer from '../assets/icons/LogoutDrawer.png';
import remindMe from '../assets/icons/remindMe.png';
import openEye from '../assets/icons/openEye.png';
import payCircle from '../assets/icons/payCircle.png';
import downloadWhite from '../assets/icons/downloadWhite.png';
import DummyProfile from '../assets/icons/DummyProfile.png';
import privacy from '../assets/icons/privacy.png';
import SuccessFree from '../assets/images/SuccessFree.png';
import { moderateVerticalScale } from 'react-native-size-matters';
import { Height } from '../dimensions/dimensions';


export const imageConstants = {
  Intro1,
  Intro2,
  Intro3,
  BackIcon,
  Logo,
  searchIcon,
  FilterIcon,
  bellIcon,
  companyFullImage,
  CompanyImage,
  phone,
  google,
  facebook,
  mail,
  lock,
  closeEye,
  Success,
  userIcon,
  ArrowRight,
  home,
  homeColor,
  blog,
  blogColor,
  premium,
  premiumColor,
  profile,
  profileColor,
  tickEmpty,
  tickfill,
  circle,
  circleFill,
  print,
  download,
  callBigIcon,
  emailBigIcon,
  LogoutImage,
  SuccessImage,
  closeEyeIcon,
  deleteIcon,
  rightBlackIcon,
  tickIcon,
  upgrade,
  userProfile,
  cross,
  DrawerUser,
  Page,
  Calendar,
  Settings,
  LogoutDrawer,
  remindMe,
  openEye,
  payCircle,
  downloadWhite,
  DummyProfile,
  SuccessFree,
  privacy
}


export const fontConstant = {
  bold: 'Inter-Bold',
  extraBold: 'Inter-ExtraBold',
  light: 'Inter-Light',
  medium: 'Inter-Medium',
  regular: 'Inter-Regular',
  semiBold: 'Inter-SemiBold',
};

export const colorConstant = {
  themecolor: "#C2EAFF",
  white: '#FFFFFF',
  green: '#00A56A',
  red: '#ED1B24',
  orange: "#EC6602",
  black: '#111111',
  blue: "#077CA1",
  buttonColor: '#2B59FF',
  secondaryText: "#4F5D77",
  inputBackground: "#F3F3F3",
  borderColor: "#0000004D",
  placeholderTextColor: "#8F8F8F",
  backgroundColor: '#D7F1FF',
  tabcolor: "#676D75",
};

export const shadowObject = {
  shadowColor: '#0000004D',
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowOpacity: 0.22,
  shadowRadius: 2.22,

  elevation: 3,
};

export const HEADER_MARGIN_IOS =
  Height > 670 ? moderateVerticalScale(50) : moderateVerticalScale(40);

export const iphone8 = Height < 670 ? true : false;
